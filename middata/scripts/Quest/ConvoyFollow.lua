local FSMWalk = 1
local FSMLost = 100

local t = {timers={},fsm=FSMWalk}

function main(pPlayer, pQuestLog, args, status)
    t.pPlayer, t.pQuestLog = pPlayer, pQuestLog
    t.instance = pPlayer:GetMapInstance()
    t.cfg = LoadConfigFile(string.format(
        'scripts/Quest/Convoy/%d.lua', pQuestLog:GetQuestTypeID()))
    t4player.handler = t.pPlayer:AttachObjectHookInfo(t4player)
    if not status then
        t.pPlayer:SaveConvoyStatus(pQuestLog:GetQuestTypeID())
    end
    t.RestoreConvoyStatus(status)
end

function t.RestoreConvoyStatus(status)
    local hp, pos = 1, nil
    if status and #status > 0 then
        status = status:splitnumber(',')
        hp = status[2]
        pos = table.slice(status, 3)
    else
        pos = table.slice(t.cfg.actor, 2, 5)
    end
    t.actor = t.instance:DeployCreature(t.cfg.actor[1],
        pos[1], pos[2], pos[3], pos[4], table.unpack(t.cfg.actor, 6))
    t.actor:AttachObjectHookInfo(t4actor)
    t.actor:SetS64Value(UNIT64_FIELD_HP,
        math.max(t.actor:GetS64Value(UNIT64_FIELD_HP_MAX) * hp, 1))
    table.insert(t.timers, CreateHandlerTimer(t.instance,
        function() t.Update() end, 1000))
end

function t.CleanConvoyStatus()
    for i, timer in ipairs(t.timers) do
        RemoveTimer(t.instance, timer)
    end
    if not t.actor:IstobeDisappear() then
        t.actor:FastDisappear()
    end
end

function t.Update()
    if t.fsm == FSMWalk then
        if not t.actor:IsInCombat() then
            if t.actor:GetDistanceSq(t.cfg.tgtpos) <= 3^2 then
                t.pPlayer:GetQuestStorage():OnConvoy(pQuestLog:GetQuestTypeID(), true)
                return
            else
                if t.actor:GetDistanceSq2obj(t.pPlayer) <= 50^2 then
                    if not t.actor:IsMoving() then
                        MoveToPosition(t.actor, t.pPlayer:GetPosition(), 1, 1, 1)
                    end
                else
                    t.fsm = FSMLost
                end
            end
        end
    elseif t.fsm == FSMLost then
        if not t.actor:IsInCombat() then
            if not t.lostime or t.lostime + 5000 < systime() then
                t.islost = systime()
                t.Player:SendPlayTips(11, 3000)
            end
            if t.actor:GetDistanceSq2obj(t.pPlayer) <= 50^2 then
                t.fsm = FSMWalk
            else
                if t.actor:IsMoving() then
                    t.actor:ForceMotionless()
                end
            end
        end
    end
end

local t4actor = {}
t4actor.events = {
    ObjectHookEvent.OnUnitDead,
}

function t4actor.OnUnitDead()
    t.pQuestLog:Failed()
    t.Player:SendPlayTips(11, 3000)
end

local t4player = {}
t4player.events = {
    ObjectHookEvent.OnPlayerChangeQuestStatus,
    ObjectHookEvent.OnPlayerDelete,
    ObjectHookEvent.OnSendMessage,
}

function t4player.OnPlayerChangeQuestStatus(pQuestLog, type)
    if pQuestLog == t.pQuestLog then
        if type == QuestWhenType.Finish || type == QuestWhenType.Failed ||
           type == QuestWhenType.Cancel then
            t.pPlayer:DetachObjectHookInfo(t4player.handler)
            t.CleanConvoyStatus()
        end
        if type == QuestWhenType.Failed then
            t.Player:SendPlayTips(11, 3000)
        end
    end
end

function t4player.OnPlayerDelete()
    t.CleanConvoyStatus()
end

function t4player.SaveConvoyStatus()
    local pos, o =  t.actor:GetPosition(), t.actor:GetOrientation()
    local status = {0, t.actor:GetHPRate(), pos.x, pos.y, pos.z, o}
    t.pPlayer:SaveConvoyStatus(pQuestLog:GetQuestTypeID(), table.concat(status))
end