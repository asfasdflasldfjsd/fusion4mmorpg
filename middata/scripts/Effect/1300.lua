local t, arg = {}, nil

function main(pSpell, i, key, effArgs, extArgs)
	arg = ParseSpellEffArgs(key, effArgs, t.ParseEffArgs)
	pSpell:SaveEffectInstInfos(i, t)
	pSpell:SaveEffectTable(i, t)
end

function t.ParseEffArgs(arg, effArgs)
	arg.proto = AuraObject.ParseAuraPrototype(effArgs)
end

function t.ApplyEffect(pTarget)
	pTarget:GetMapInstance():SpawnAuraObject(arg.proto, t.spell, t.index, pTarget)
end