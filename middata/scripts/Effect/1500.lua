local t, arg = {}, nil

function main(pSpell, i, key, effArgs, extArgs)
	arg = ParseSpellEffArgs(key, effArgs, t.ParseEffArgs)
	pSpell:SaveEffectInstInfos(i, t)
	pSpell:SaveEffectTable(i, t)
end

function t.ParseEffArgs(arg, effArgs)
	arg.tpId = table.unpack(effArgs:splitnumber(','))
end

function t.ApplyEffect(pTarget)
	if pTarget:IsType(TYPE_PLAYER) then
		pTarget:TeleportBy(arg.tpId)
	end
end