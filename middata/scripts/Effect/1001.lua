local t, arg = {}, nil

function main(pSpell, i, key, effArgs, extArgs)
	arg = ParseSpellEffArgs(key, effArgs, t.ParseEffArgs)
	pSpell:SaveEffectInstInfos(i, t)
	pSpell:SaveEffectTable(i, t)
end

function t.ParseEffArgs(arg, effArgs)
	arg.attackRate, arg.attackValue = table.unpack(effArgs:splitnumber(','))
end

function t.ApplyEffect(pTarget)
	t.obj:Strike(pTarget, t)
end

function t.CalcDamage(t, pVictim)
	return t.obj:GetAttribute():CalcAttackDamage(pVictim, arg.attackRate, arg.attackValue)
end