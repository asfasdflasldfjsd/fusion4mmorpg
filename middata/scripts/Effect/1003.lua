local t, t1, arg = {}, Class(), nil

function main(pSpell, i, key, effArgs, extArgs)
	arg = ParseSpellEffArgs(key, effArgs, t.ParseEffArgs)
	pSpell:SaveEffectBuffInfos(i, t1)
	pSpell:SaveEffectTable(i, t, t1)
end

function t.ParseEffArgs(arg, effArgs)
	local argsList = effArgs:splitnumber(',')
	arg.duration, arg.attrs = argsList[1], table.slice(argsList, 2)
end

function t.ApplyEffect(pTarget)
	local tx = t1:new({obj=pTarget, duration=t1.duration or arg.duration})
	pTarget:AttachSpellBuffInfo(tx)
end

t1.resumable = true

function t1.OnAttach(tx, key)
	local restTime = CalcSpellBuffRestTime(tx.duration, t1.elapse, t1.skip)
	tx.handler = CreateHandlerTimer(tx.obj, function()
		DetachSpellBuffWhenTimerExpired(tx, key)
	end, restTime, 1)
	tx:ApplyEffect(true)
end

function t1.OnDetach(tx)
	TryRemoveTimerWhenSpellBuffDetached(tx)
	tx:ApplyEffect(false)
end

function t1.ApplyEffect(tx, isApply)
	local pAttrib = tx.obj:GetAttribute()
	for i = 1, #arg.attrs, 3 do
		pAttrib:ModAttr(arg.attrs[i], ATTRPARTTYPE.BUFF,
			arg.attrs[i+1], isApply and arg.attrs[i+2] or -arg.attrs[i+2])
	end
end