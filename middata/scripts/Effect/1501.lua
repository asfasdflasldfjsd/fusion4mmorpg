local t, arg = {}, nil

function main(pSpell, i, key, effArgs, extArgs)
	arg = ParseSpellEffArgs(key, effArgs, t.ParseEffArgs)
	pSpell:SaveEffectInstInfos(i, t)
	pSpell:SaveEffectTable(i, t)
end

function t.ParseEffArgs(arg, effArgs)
end

function t.ApplyEffect(pTarget)
	if pTarget:IsType(TYPE_PLAYER) then
		SpellEffects.Apply4Mine(t.spell, pTarget, t.index)
	end
end