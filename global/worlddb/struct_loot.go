package worlddb

type LootSet struct {
	LsID   uint32 `json:"lsID,omitempty" rule:"required"`
	LsName string `json:"lsName,omitempty" rule:"required"`
}

func (*LootSet) GetTableName() string {
	return "loot_set"
}
func (*LootSet) GetTableKeyName() string {
	return "lsID"
}
func (obj *LootSet) GetTableKeyValue() uint {
	return uint(obj.LsID)
}

type LootSetGroup struct {
	LsgID             uint32  `json:"lsgID,omitempty" rule:"required"`
	LsID              uint32  `json:"lsID,omitempty" rule:"required"`
	LsgName           string  `json:"lsgName,omitempty" rule:"required"`
	LsgOdds           float32 `json:"lsgOdds,omitempty" rule:"required"`
	LsgMinItemTimes   uint32  `json:"lsgMinItemTimes,omitempty" rule:"required"`
	LsgMaxItemTimes   uint32  `json:"lsgMaxItemTimes,omitempty" rule:"required"`
	LsgMinChequeTimes uint32  `json:"lsgMinChequeTimes,omitempty" rule:"required"`
	LsgMaxChequeTimes uint32  `json:"lsgMaxChequeTimes,omitempty" rule:"required"`
}

func (*LootSetGroup) GetTableName() string {
	return "loot_set_group"
}
func (*LootSetGroup) GetTableKeyName() string {
	return "lsgID"
}
func (obj *LootSetGroup) GetTableKeyValue() uint {
	return uint(obj.LsgID)
}

type LootSetGroupItem_Flags struct {
	BindToPicker bool `json:"bindToPicker,omitempty"`
}

type LootSetGroupItem struct {
	LsgiID          uint32                 `json:"lsgiID,omitempty" rule:"required"`
	LsID            uint32                 `json:"lsID,omitempty" rule:"required"`
	LsgID           uint32                 `json:"lsgID,omitempty" rule:"required"`
	LsgiFlags       LootSetGroupItem_Flags `json:"lsgiFlags,omitempty" rule:"required"`
	LsgiWeight      uint32                 `json:"lsgiWeight,omitempty" rule:"required"`
	LsgiItemTypeID  uint32                 `json:"lsgiItemTypeID,omitempty" rule:"required"`
	LsgiMinCount    uint32                 `json:"lsgiMinCount,omitempty" rule:"required"`
	LsgiMaxCount    uint32                 `json:"lsgiMaxCount,omitempty" rule:"required"`
	LsgiMaxTimes    uint32                 `json:"lsgiMaxTimes,omitempty" rule:"required"`
	LsgiLimitQuest  uint32                 `json:"lsgiLimitQuest,omitempty" rule:"required"`
	LsgiLimitCareer uint32                 `json:"lsgiLimitCareer,omitempty" rule:"required"`
	LsgiLimitGender uint32                 `json:"lsgiLimitGender,omitempty" rule:"required"`
}

func (*LootSetGroupItem) GetTableName() string {
	return "loot_set_group_item"
}
func (*LootSetGroupItem) GetTableKeyName() string {
	return "lsgiID"
}
func (obj *LootSetGroupItem) GetTableKeyValue() uint {
	return uint(obj.LsgiID)
}

type LootSetGroupCheque struct {
	LsgcID         uint32 `json:"lsgcID,omitempty" rule:"required"`
	LsID           uint32 `json:"lsID,omitempty" rule:"required"`
	LsgID          uint32 `json:"lsgID,omitempty" rule:"required"`
	LsgcWeight     uint32 `json:"lsgcWeight,omitempty" rule:"required"`
	LsgcChequeType uint32 `json:"lsgcChequeType,omitempty" rule:"required"`
	LsgcMinValue   uint32 `json:"lsgcMinValue,omitempty" rule:"required"`
	LsgcMaxValue   uint32 `json:"lsgcMaxValue,omitempty" rule:"required"`
	LsgcMaxTimes   uint32 `json:"lsgcMaxTimes,omitempty" rule:"required"`
}

func (*LootSetGroupCheque) GetTableName() string {
	return "loot_set_group_cheque"
}
func (*LootSetGroupCheque) GetTableKeyName() string {
	return "lsgcID"
}
func (obj *LootSetGroupCheque) GetTableKeyValue() uint {
	return uint(obj.LsgcID)
}
