package worlddb

// ATTRARITHTYPE
const (
	ATTRARITHTYPE_BASE  = 0
	ATTRARITHTYPE_SCALE = 1
	ATTRARITHTYPE_FINAL = 2
	ATTRARITHTYPE_COUNT = 3
)

// ATTRTYPE
const (
	ATTRTYPE_NONE = 0

	ATTRTYPE_HIT_POINT   = 1
	ATTRTYPE_MAGIC_POINT = 2

	ATTRTYPE_ATTACK_VALUE  = 3
	ATTRTYPE_DEFENSE_VALUE = 4

	ATTRTYPE_HIT_CHANCE                = 5
	ATTRTYPE_DODGE_CHANCE              = 6
	ATTRTYPE_CRITIHIT_CHANCE           = 7
	ATTRTYPE_CRITIHIT_CHANCE_RESIST    = 8
	ATTRTYPE_CRITIHIT_INTENSITY        = 9
	ATTRTYPE_CRITIHIT_INTENSITY_RESIST = 10

	ATTRTYPE_COUNT = 11
)

type PlayerBase struct {
	Level           uint32  `json:"level,omitempty" rule:"required"`
	LvUpExp         uint64  `json:"lvUpExp,omitempty" rule:"required"`
	DamageFactor    float64 `json:"damageFactor,omitempty" rule:"required"`
	RecoveryHPRate  float64 `json:"recoveryHPRate,omitempty" rule:"required"`
	RecoveryHPValue float64 `json:"recoveryHPValue,omitempty" rule:"required"`
	RecoveryMPRate  float64 `json:"recoveryMPRate,omitempty" rule:"required"`
	RecoveryMPValue float64 `json:"recoveryMPValue,omitempty" rule:"required"`
}

func (*PlayerBase) GetTableName() string {
	return "player_base"
}
func (*PlayerBase) GetTableKeyName() string {
	return "level"
}
func (obj *PlayerBase) GetTableKeyValue() uint {
	return uint(obj.Level)
}

type PlayerAttribute struct {
	Id     uint32    `json:"Id,omitempty" rule:"required"`
	Career uint32    `json:"career,omitempty" rule:"required"`
	Level  uint32    `json:"level,omitempty" rule:"required"`
	Attrs  []float64 `json:"attrs,omitempty" rule:"required"`
}

func (*PlayerAttribute) GetTableName() string {
	return "player_attribute"
}
func (*PlayerAttribute) GetTableKeyName() string {
	return "Id"
}
func (obj *PlayerAttribute) GetTableKeyValue() uint {
	return uint(obj.Id)
}

type CreatureAttribute struct {
	Id           uint32    `json:"Id,omitempty" rule:"required"`
	Elite        uint32    `json:"elite,omitempty" rule:"required"`
	Level        uint32    `json:"level,omitempty" rule:"required"`
	Attrs        []float64 `json:"attrs,omitempty" rule:"required"`
	DamageFactor float64   `json:"damageFactor,omitempty" rule:"required"`
}

func (*CreatureAttribute) GetTableName() string {
	return "creature_attribute"
}
func (*CreatureAttribute) GetTableKeyName() string {
	return "Id"
}
func (obj *CreatureAttribute) GetTableKeyValue() uint {
	return uint(obj.Id)
}

type CreatureCustomAttribute struct {
	CharTypeId   uint32    `json:"charTypeId,omitempty" rule:"required"`
	Attrs        []float64 `json:"attrs,omitempty" rule:"required"`
	DamageFactor float64   `json:"damageFactor,omitempty" rule:"required"`
}

func (*CreatureCustomAttribute) GetTableName() string {
	return "creature_custom_attribute"
}
func (*CreatureCustomAttribute) GetTableKeyName() string {
	return "charTypeId"
}
func (obj *CreatureCustomAttribute) GetTableKeyValue() uint {
	return uint(obj.CharTypeId)
}
