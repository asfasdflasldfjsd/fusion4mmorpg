function If_undefined(value, defValue) {
    return value !== undefined ? value : defValue
}

function ArrayRemove(array, value) {
  for (let i in array) {
      if (array[i] == value) {
          array.splice(i, 1)
          return i
      }
  }
}

function ArrayRemoveIf(array, predicate) {
    for (let i in array) {
        if (predicate(array[i])) {
            array.splice(i, 1)
            return i
        }
    }
}

function ArrayNearestIndex(array, index) {
    if (index < array.length) {
        return index
    } else if (array.length > 0) {
        return array.length - 1
    }
}

function JoinBits2Number(bits) {
    let num = 0
    for (let v of bits) {
        num |= 1 << v
    }
    return num
}

function SplitNumber2Bits(num) {
    let bits = []
    for (let i = 0; i < 53; ++i) {
        if ((num & (1 << i)) != 0) {
            bits.push(i)
        }
    }
    return bits
}

function SplitString2Number(str) {
    let nums = []
    for (let s of str.split(',')) {
        let num = parseFloat(s)
        nums.push(!isNaN(num) ? num : 0)
    }
    return nums
}

function FixEditorTree(treeNodes) {
  let FixTreeNodes = function(treeNodes) {
    for (let treeNode of treeNodes) {
      if (treeNode.id === undefined) {
        if (treeNode.children !== undefined) {
          FixTreeNodes(treeNode.children)
        } else {
          treeNode.children = []
        }
        if (treeNode.cls === undefined) {
          treeNode.cls = 0
        }
      } else {
        treeNode.brothers = treeNodes
      }
    }
  }
  FixTreeNodes(treeNodes)
}

function FindEditorTreeNodeBrothers(treeNodes, itemClass) {
  for (let cls of itemClass) {
    let treeNode = _.find(treeNodes, treeNode =>
      treeNode.id === undefined && treeNode.cls == cls)
    if (treeNode !== undefined) {
      treeNodes = treeNode.children
    } else {
      break
    }
  }
  return treeNodes
}

function InsertEditorTreeNode(treeNodes, itemId, itemName, itemClass) {
  let brothers = FindEditorTreeNodeBrothers(treeNodes, itemClass)
  let treeNode = {'id': itemId, 'label': itemName, 'brothers': brothers}
  brothers.push(treeNode)
  return treeNode
}

function RelocateEditorTreeNode(treeNodes, treeNode, itemClass) {
  let brothers = FindEditorTreeNodeBrothers(treeNodes, itemClass)
  if (brothers !== treeNode.brothers) {
    ArrayRemove(treeNode.brothers, treeNode)
    brothers.push(treeNode)
    treeNode.brothers = brothers
  }
}

function SaveEditorTree(treeNodes) {
  let itemIDs = []
  let GetTreeNodes = function(treeNodes) {
    for (let treeNode of treeNodes) {
      if (treeNode.id === undefined) {
        GetTreeNodes(treeNode.children)
      } else {
        itemIDs.push(treeNode.id)
      }
    }
  }
  GetTreeNodes(treeNodes)
  return itemIDs
}

function FixItemClassCascade(options, itemClass) {
  for (let i in itemClass) {
    let option = _.find(options,
      option => option.value == itemClass[i])
    if (option !== undefined) {
      options = option.children
    } else {
      itemClass.splice(i)
    }
  }
}

function GetWebSelectOptionLabel(options, value) {
  for (let option of options) {
    if (option.value == value) {
      return option.label
    }
  }
}

Vue.prototype.$GetMapWebSelectOptionLabel = function(allMapNames) {
  let options = [{value: 0, label: '无'}]
  for (let id in allMapNames) {
    options.push({value: id, label: `(${id})${allMapNames[id]}`})
  }
  return options.sort((a, b) => a.value - b.value)
}

Vue.prototype.$GetTreeNodeLabel = function(node) {
  if (node.id === undefined) return node.label
  return '('+node.id+')'+node.label
}

Vue.prototype.$GetSpellLabel = function(spellRows, spellID) {
  if (spellID == 0) return ''
  let v = _.find(spellRows, v => v.spellID == spellID)
  return '('+spellID+')'+(v!==undefined?v.spellName:'')
}

Vue.prototype.$GetQuestLabel = function(questRows, questTypeID) {
  if (questTypeID == 0) return ''
  let v = _.find(questRows, v => v.questTypeID == questTypeID)
  return '('+questTypeID+')'+(v!==undefined?v.questName:'')
}

Vue.prototype.$GetQuestionLabel = function(questionRows, questionID) {
  if (questionID == 0) return ''
  let v = _.find(questionRows, v => v.questionID == questionID)
  return '('+questionID+')'+(v!==undefined?v.question:'')
}

Vue.prototype.$GetItemLabel = function(itemRows, itemTypeID) {
  if (itemTypeID == 0) return ''
  let v = _.find(itemRows, v => v.itemTypeID == itemTypeID)
  return '('+itemTypeID+')'+(v!==undefined?v.itemName:'')
}

Vue.prototype.$GetTeleportLabel = function(tpRows, tpID) {
  if (tpID == 0) return ''
  let v = _.find(tpRows, v => v.tpID == tpID)
  return '('+tpID+')'+(v!==undefined?v.tpName:'')
}

Vue.prototype.$GetLootLabel = function(lsRows, lsID) {
  if (lsID == 0) return ''
  let v = _.find(lsRows, v => v.lsID == lsID)
  return '('+lsID+')'+(v!==undefined?v.lsName:'')
}

Vue.prototype.$GetScriptableLabel = function(scriptFiles, scriptId) {
  if (scriptId == 0) return ''
  let v = _.find(scriptFiles, v => v.scriptId == scriptId)
  return '('+scriptId+')'+(v!==undefined?v.scriptFile:'')
}

Vue.prototype.$GetObjInstLabel = function(objInstType, objInstID, env) {
  if (objInstType != 0 && objInstID != 0) {
    let name = undefined, v = undefined
    switch (objInstType) {
    case env.enumType.QuestObjType.Guide:
      v = _.find(env.dialogArgs.lmRows, v => v.lmID == objInstID)
      name = v !== undefined ? v.lmName : undefined
      break
    case env.enumType.QuestObjType.NPC:
      v = _.find(env.dialogArgs.charSpawnRows, v => v.spawnId == objInstID)
      name = v !== undefined ? env.dialogArgs.charNames[v.entry] : undefined
      break
    case env.enumType.QuestObjType.NPCPt:
      name = env.dialogArgs.charNames[objInstID]
      break
    case env.enumType.QuestObjType.SObj:
      v = _.find(env.dialogArgs.sobjSpawnRows, v => v.spawnId == objInstID)
      name = v !== undefined ? env.dialogArgs.sobjNames[v.entry] : undefined
      break
    case env.enumType.QuestObjType.SObjPt:
      name = env.dialogArgs.sobjNames[objInstID]
      break
    }
    return `(${objInstID})${name !== undefined ? name : ''}`
  } else {
    return ''
  }
}

Vue.prototype.$FilterObjUIType = function(filter, env) {
  switch (filter) {
    case 'all':
      return env.questUi.QuestObjType
    case 'npc':
      return [env.questUi.QuestObjType[env.enumType.QuestObjType.None],
              env.questUi.QuestObjType[env.enumType.QuestObjType.NPC],
              env.questUi.QuestObjType[env.enumType.QuestObjType.NPCPt],]
    case 'sobj':
      return [env.questUi.QuestObjType[env.enumType.QuestObjType.None],
              env.questUi.QuestObjType[env.enumType.QuestObjType.SObj],
              env.questUi.QuestObjType[env.enumType.QuestObjType.SObjPt],]
    case 'guide':
      return [env.questUi.QuestObjType[env.enumType.QuestObjType.None],
              env.questUi.QuestObjType[env.enumType.QuestObjType.Guide],]
    case 'lobj':
      return [env.questUi.QuestObjType[env.enumType.QuestObjType.None],
              env.questUi.QuestObjType[env.enumType.QuestObjType.NPC],
              env.questUi.QuestObjType[env.enumType.QuestObjType.NPCPt],
              env.questUi.QuestObjType[env.enumType.QuestObjType.SObj],
              env.questUi.QuestObjType[env.enumType.QuestObjType.SObjPt],]
  }
}

Vue.prototype.$GetQuestionOptions = function(questionRows, questionID, extOptions = undefined) {
  let v = _.find(questionRows, v => v.questionID == questionID)
  if (v !== undefined) {
    return extOptions !== undefined ? extOptions.concat(v.options) : v.options
  } else {
    return extOptions !== undefined ? extOptions : []
  }
}

Vue.component('my-spell-dialog', {
  model: {
    prop: 'spellID',
    event: 'change',
  },
  props: {
    spellData: {type: Array, required: true},
    spellID: {type: Number, required: true},
    visible: {type: Boolean, required: true},
  },
  data: function() {
    return {
      spellInternalID: this.spellID,
      spellFilter: '',
    }
  },
  computed: {
    spellInternalName: function() {
      let v = _.find(this.spellData, v => v.spellID == this.spellInternalID)
      return v !== undefined ? v.spellName : ''
    },
  },
  methods: {
    handleSpellFilter(data) {
      if (this.spellFilter == '') {
        return true
      }
      if (data.spellName.includes(this.spellFilter)) {
        return true
      }
      if (data.spellID == this.spellFilter) {
        return true
      }
    },
    handleCurrentSpellChange(row) {
      this.spellInternalID = row.spellID
    },
    handleDone() {
      this.$emit('change', this.spellInternalID)
      this.$emit('update:visible', false)
    },
    handleCancel() {
      this.$emit('update:visible', false)
    },
  },
  template: `
    <el-dialog title="选择技能" :visible="visible" @update:visible="handleCancel">
      <el-table :data="spellData.filter(handleSpellFilter)" :height="450"
        highlight-current-row row-key="spellID" :current-row-key="spellInternalID"
        @current-change="handleCurrentSpellChange">
        <el-table-column property="spellID" label="ID" width="100"></el-table-column>
        <el-table-column property="spellName" label="名称" width="250"></el-table-column>
        <el-table-column property="spellDesc" label="描述"></el-table-column>
      </el-table>
      <el-form :inline="true" style="margin-top: 6px">
        <el-form-item label="技能ID">
          <el-input :value="spellInternalID" readonly style="width: 100px"></el-input>
        </el-form-item>
        <el-form-item label="技能名称">
          <el-input :value="spellInternalName" readonly></el-input>
        </el-form-item>
        <el-form-item label="过滤">
          <el-input v-model="spellFilter" suffix-icon="el-icon-search" placeholder="输入关键字搜索">
          </el-input>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="handleCancel">取消</el-button>
        <el-button type="primary" @click="handleDone">确定</el-button>
      </div>
    </el-dialog>
  `
})

Vue.component('my-quest-dialog', {
  model: {
    prop: 'questTypeID',
    event: 'change',
  },
  props: {
    questData: {type: Array, required: true},
    questTypeID: {type: Number, required: true},
    visible: {type: Boolean, required: true},
  },
  data: function() {
    return {
      questInternalTypeID: this.questTypeID,
      questFilter: '',
    }
  },
  computed: {
    questInternalName: function() {
      let v = _.find(this.questData, v => v.questTypeID == this.questInternalTypeID)
      return v !== undefined ? v.questName : ''
    },
  },
  methods: {
    handleQuestFilter(data) {
      if (this.questFilter == '') {
        return true
      }
      if (data.questName.includes(this.questFilter)) {
        return true
      }
      if (data.questTypeID == this.questFilter) {
        return true
      }
    },
    handleCurrentQuestChange(row) {
      this.questInternalTypeID = row.questTypeID
    },
    handleDone() {
      this.$emit('change', this.questInternalTypeID)
      this.$emit('update:visible', false)
    },
    handleCancel() {
      this.$emit('update:visible', false)
    },
  },
  template: `
    <el-dialog title="选择任务" :visible="visible" @update:visible="handleCancel">
      <el-table :data="questData.filter(handleQuestFilter)" :height="450"
        highlight-current-row row-key="questTypeID" :current-row-key="questInternalTypeID"
        @current-change="handleCurrentQuestChange">
        <el-table-column property="questTypeID" label="ID" width="100"></el-table-column>
        <el-table-column property="questName" label="名称" width="250"></el-table-column>
        <el-table-column property="questDesc" label="描述"></el-table-column>
      </el-table>
      <el-form :inline="true" style="margin-top: 6px">
        <el-form-item label="任务ID">
          <el-input :value="questInternalTypeID" readonly style="width: 100px"></el-input>
        </el-form-item>
        <el-form-item label="任务名称">
          <el-input :value="questInternalName" readonly></el-input>
        </el-form-item>
        <el-form-item label="过滤">
          <el-input v-model="questFilter" suffix-icon="el-icon-search" placeholder="输入关键字搜索">
          </el-input>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="handleCancel">取消</el-button>
        <el-button type="primary" @click="handleDone">确定</el-button>
      </div>
    </el-dialog>
  `
})

Vue.component('my-question-dialog', {
  model: {
    prop: 'questionID',
    event: 'change',
  },
  props: {
    questionData: {type: Array, required: true},
    questionID: {type: Number, required: true},
    visible: {type: Boolean, required: true},
  },
  data: function() {
    return {
      questionInternalID: this.questionID,
      questionFilter: '',
    }
  },
  computed: {
    questionInternal: function() {
      let v = _.find(this.questionData, v => v.questionID == this.questionInternalID)
      return v !== undefined ? v.question : ''
    },
  },
  methods: {
    handleQuestionFilter(data) {
      if (this.questionFilter == '') {
        return true
      }
      if (data.question.includes(this.questionFilter)) {
        return true
      }
      if (data.questionID == this.questionFilter) {
        return true
      }
    },
    handleCurrentQuestionChange(row) {
      this.questionInternalID = row.questionID
    },
    handleDone() {
      this.$emit('change', this.questionInternalID)
      this.$emit('update:visible', false)
    },
    handleCancel() {
      this.$emit('update:visible', false)
    },
  },
  template: `
    <el-dialog title="选择问题" :visible="visible" @update:visible="handleCancel">
      <el-table :data="questionData.filter(handleQuestionFilter)" :height="450"
        highlight-current-row row-key="questionID" :current-row-key="questionInternalID"
        @current-change="handleCurrentQuestionChange">
        <el-table-column property="questionID" label="ID" width="100"></el-table-column>
        <el-table-column property="question" label="问题"></el-table-column>
      </el-table>
      <el-form :inline="true" style="margin-top: 6px">
        <el-form-item label="ID">
          <el-input :value="questionInternalID" readonly style="width: 100px"></el-input>
        </el-form-item>
        <el-form-item label="问题">
          <el-input :value="questionInternal" type="textarea" readonly style="width: 432px"></el-input>
        </el-form-item>
        <el-form-item label="过滤">
          <el-input v-model="questionFilter" suffix-icon="el-icon-search" placeholder="输入关键字搜索">
          </el-input>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="handleCancel">取消</el-button>
        <el-button type="primary" @click="handleDone">确定</el-button>
      </div>
    </el-dialog>
  `
})

Vue.component('my-item-dialog', {
  model: {
    prop: 'itemTypeID',
    event: 'change',
  },
  props: {
    itemData: {type: Array, required: true},
    itemTypeID: {type: Number, required: true},
    visible: {type: Boolean, required: true},
  },
  data: function() {
    return {
      itemInternalTypeID: this.itemTypeID,
      itemFilter: '',
    }
  },
  computed: {
    itemInternalName: function() {
      let v = _.find(this.itemData, v => v.itemTypeID == this.itemInternalTypeID)
      return v !== undefined ? v.itemName : ''
    },
  },
  methods: {
    handleItemFilter(data) {
      if (this.itemFilter == '') {
        return true
      }
      if (data.itemName.includes(this.itemFilter)) {
        return true
      }
      if (data.itemTypeID == this.itemFilter) {
        return true
      }
    },
    handleCurrentItemChange(row) {
      this.itemInternalTypeID = row.itemTypeID
    },
    handleDone() {
      this.$emit('change', this.itemInternalTypeID)
      this.$emit('update:visible', false)
    },
    handleCancel() {
      this.$emit('update:visible', false)
    },
  },
  template: `
    <el-dialog title="选择物品" :visible="visible" @update:visible="handleCancel">
      <el-table :data="itemData.filter(handleItemFilter)" :height="450"
        highlight-current-row row-key="itemTypeID" :current-row-key="itemInternalTypeID"
        @current-change="handleCurrentItemChange">
        <el-table-column property="itemTypeID" label="ID" width="100"></el-table-column>
        <el-table-column property="itemName" label="名称" width="250"></el-table-column>
        <el-table-column property="itemDesc" label="描述"></el-table-column>
      </el-table>
      <el-form :inline="true" style="margin-top: 6px">
        <el-form-item label="物品ID">
          <el-input :value="itemInternalTypeID" readonly style="width: 100px"></el-input>
        </el-form-item>
        <el-form-item label="物品名称">
          <el-input :value="itemInternalName" readonly></el-input>
        </el-form-item>
        <el-form-item label="过滤">
          <el-input v-model="itemFilter" suffix-icon="el-icon-search" placeholder="输入关键字搜索">
          </el-input>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="handleCancel">取消</el-button>
        <el-button type="primary" @click="handleDone">确定</el-button>
      </div>
    </el-dialog>
  `
})

Vue.component('my-loot-dialog', {
  model: {
    prop: 'lsID',
    event: 'change',
  },
  props: {
    lsData: {type: Array, required: true},
    lsID: {type: Number, required: true},
    visible: {type: Boolean, required: true},
  },
  data: function() {
    return {
      lsInternalID: this.lsID,
      lsFilter: '',
    }
  },
  computed: {
    lsInternalName: function() {
      let v = _.find(this.lsData, v => v.lsID == this.lsInternalID)
      return v !== undefined ? v.lsName : ''
    },
  },
  methods: {
    handleLootFilter(data) {
      if (this.lsFilter == '') {
        return true
      }
      if (data.lsName.includes(this.lsFilter)) {
        return true
      }
      if (data.lsID == this.lsFilter) {
        return true
      }
    },
    handleCurrentLootChange(row) {
      this.lsInternalID = row.lsID
    },
    handleDone() {
      this.$emit('change', this.lsInternalID)
      this.$emit('update:visible', false)
    },
    handleCancel() {
      this.$emit('update:visible', false)
    },
  },
  template: `
    <el-dialog title="选择掉落" :visible="visible" @update:visible="handleCancel">
      <el-table :data="lsData.filter(handleLootFilter)" :height="450"
        highlight-current-row row-key="lsID" :current-row-key="lsInternalID"
        @current-change="handleCurrentLootChange">
        <el-table-column property="lsID" label="ID" width="100"></el-table-column>
        <el-table-column property="lsName" label="名称"></el-table-column>
      </el-table>
      <el-form :inline="true" style="margin-top: 6px">
        <el-form-item label="ID">
          <el-input :value="lsInternalID" readonly style="width: 100px"></el-input>
        </el-form-item>
        <el-form-item label="名称">
          <el-input :value="lsInternalName" readonly></el-input>
        </el-form-item>
        <el-form-item label="过滤">
          <el-input v-model="lsFilter" suffix-icon="el-icon-search" placeholder="输入关键字搜索">
          </el-input>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="handleCancel">取消</el-button>
        <el-button type="primary" @click="handleDone">确定</el-button>
      </div>
    </el-dialog>
  `
})

Vue.component('my-teleport-dialog', {
  model: {
    prop: 'tpID',
    event: 'change',
  },
  props: {
    tpData: {type: Array, required: true},
    tpID: {type: Number, required: true},
    visible: {type: Boolean, required: true},
  },
  data: function() {
    return {
      tpInternalID: this.tpID,
      tpFilter: '',
    }
  },
  computed: {
    tpInternalName: function() {
      let v = _.find(this.tpData, v => v.tpID == this.tpInternalID)
      return v !== undefined ? v.tpName : ''
    },
  },
  methods: {
    handleTeleportFilter(data) {
      if (this.tpFilter == '') {
        return true
      }
      if (data.tpName.includes(this.tpFilter)) {
        return true
      }
      if (data.tpID == this.tpFilter) {
        return true
      }
    },
    handleCurrentTeleportChange(row) {
      this.tpInternalID = row.tpID
    },
    handleDone() {
      this.$emit('change', this.tpInternalID)
      this.$emit('update:visible', false)
    },
    handleCancel() {
      this.$emit('update:visible', false)
    },
  },
  template: `
    <el-dialog title="选择传送点" :visible="visible" @update:visible="handleCancel">
      <el-table :data="tpData.filter(handleTeleportFilter)" :height="450"
        highlight-current-row row-key="tpID" :current-row-key="tpInternalID"
        @current-change="handleCurrentTeleportChange">
        <el-table-column property="tpID" label="ID" width="100"></el-table-column>
        <el-table-column property="tpName" label="名称"></el-table-column>
      </el-table>
      <el-form :inline="true" style="margin-top: 6px">
        <el-form-item label="传送点ID">
          <el-input :value="tpInternalID" readonly style="width: 100px"></el-input>
        </el-form-item>
        <el-form-item label="传送点名称">
          <el-input :value="tpInternalName" readonly></el-input>
        </el-form-item>
        <el-form-item label="过滤">
          <el-input v-model="tpFilter" suffix-icon="el-icon-search" placeholder="输入关键字搜索">
          </el-input>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="handleCancel">取消</el-button>
        <el-button type="primary" @click="handleDone">确定</el-button>
      </div>
    </el-dialog>
  `
})

Vue.component('my-script-dialog', {
  model: {
    prop: 'scriptID',
    event: 'change',
  },
  props: {
    scriptData: {type: Array, required: true},
    scriptType: {type: Number, required: true},
    scriptID: {type: Number, required: true},
    visible: {type: Boolean, required: true},
  },
  data: function() {
    return {
      scriptInternalID: this.scriptID,
      scriptFilter: '',
    }
  },
  computed: {
    scriptInternalName: function() {
      let v = _.find(this.scriptData, v => v.scriptId == this.scriptInternalID)
      return v !== undefined ? v.scriptFile : ''
    },
  },
  methods: {
    handleScriptableFilter(data) {
      if (this.scriptType != data.scriptType) {
        return false
      }
      if (this.scriptFilter == '') {
        return true
      }
      if (data.scriptFile.includes(this.scriptFilter)) {
        return true
      }
      if (data.scriptId == this.scriptFilter) {
        return true
      }
    },
    handleCurrentScriptableChange(row) {
      this.scriptInternalID = row.scriptId
    },
    handleDone() {
      this.$emit('change', this.scriptInternalID)
      this.$emit('update:visible', false)
    },
    handleCancel() {
      this.$emit('update:visible', false)
    },
  },
  template: `
    <el-dialog title="选择脚本" :visible="visible" @update:visible="handleCancel">
      <el-table :data="scriptData.filter(handleScriptableFilter)" :height="450"
        highlight-current-row row-key="scriptId" :current-row-key="scriptInternalID"
        @current-change="handleCurrentScriptableChange">
        <el-table-column property="scriptId" label="ID" width="100"></el-table-column>
        <el-table-column property="scriptFile" label="脚本文件"></el-table-column>
      </el-table>
      <el-form :inline="true" style="margin-top: 6px">
        <el-form-item label="脚本ID">
          <el-input :value="scriptInternalID" readonly style="width: 100px"></el-input>
        </el-form-item>
        <el-form-item label="脚本文件">
          <el-input :value="scriptInternalName" readonly style="width: 350px"></el-input>
        </el-form-item>
        <el-form-item label="过滤">
          <el-input v-model="scriptFilter" suffix-icon="el-icon-search" placeholder="输入关键字搜索">
          </el-input>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="handleCancel">取消</el-button>
        <el-button type="primary" @click="handleDone">确定</el-button>
      </div>
    </el-dialog>
  `
})

Vue.component('my-landmark-dialog', {
  model: {
    prop: 'lmID',
    event: 'change',
  },
  props: {
    lmData: {type: Array, required: true},
    lmID: {type: Number, required: true},
    visible: {type: Boolean, required: true},
  },
  data: function() {
    return {
      lmInternalID: this.lmID,
      lmFilter: '',
    }
  },
  computed: {
    lmInternalName: function() {
      let v = _.find(this.lmData, v => v.lmID == this.lmInternalID)
      return v !== undefined ? v.lmName : ''
    },
  },
  methods: {
    handleLandmarkFilter(data) {
      if (this.lmFilter == '') {
        return true
      }
      if (data.lmName.includes(this.lmFilter)) {
        return true
      }
      if (data.lmID == this.lmFilter) {
        return true
      }
    },
    handleCurrentLandmarkChange(row) {
      this.lmInternalID = row.lmID
    },
    handleDone() {
      this.$emit('change', this.lmInternalID)
      this.$emit('update:visible', false)
    },
    handleCancel() {
      this.$emit('update:visible', false)
    },
  },
  template: `
    <el-dialog title="选择向导点" :visible="visible" @update:visible="handleCancel">
      <el-table :data="lmData.filter(handleLandmarkFilter)" :height="450"
        highlight-current-row row-key="lmID" :current-row-key="lmInternalID"
        @current-change="handleCurrentLandmarkChange">
        <el-table-column property="lmID" label="ID" width="100"></el-table-column>
        <el-table-column property="lmName" label="名称"></el-table-column>
      </el-table>
      <el-form :inline="true" style="margin-top: 6px">
        <el-form-item label="ID">
          <el-input :value="lmInternalID" readonly style="width: 100px"></el-input>
        </el-form-item>
        <el-form-item label="名称">
          <el-input :value="lmInternalName" readonly></el-input>
        </el-form-item>
        <el-form-item label="过滤">
          <el-input v-model="lmFilter" suffix-icon="el-icon-search" placeholder="输入关键字搜索">
          </el-input>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="handleCancel">取消</el-button>
        <el-button type="primary" @click="handleDone">确定</el-button>
      </div>
    </el-dialog>
  `
})

Vue.component('my-char-spawn-dialog', {
  model: {
    prop: 'spawnId',
    event: 'change',
  },
  props: {
    MapType: {type: Array, required: true},
    mapNames: {type: Object, required: true},
    charNames: {type: Object, required: true},
    charSpawnData: {type: Array, required: true},
    spawnId: {type: Number, required: true},
    visible: {type: Boolean, required: true},
  },
  data: function() {
    return {
      spawnInternalId: this.spawnId,
      charFilter: '',
    }
  },
  computed: {
    spawnInternalName: function() {
      let v = _.find(this.charSpawnInternalData, v => v.spawnId == this.spawnInternalId)
      return v !== undefined ? v.name : ''
    },
    charSpawnInternalData: function() {
      let tableRows = []
      for (let v of this.charSpawnData) {
        let tableRow = Object.create(v)
        tableRow.name = this.charNames[v.entry]
        tableRow.position = `${v.x},${v.y},${v.z}`
        tableRow.mapName = `(${v.map_id})${this.mapNames[v.map_id]}`
        tableRow.mapType = GetWebSelectOptionLabel(this.MapType, v.map_type)
        tableRows.push(tableRow)
      }
      return tableRows
    },
  },
  methods: {
    handleCharSpawnFilter(data) {
      if (this.charFilter == '') {
        return true
      }
      if (data.name.includes(this.charFilter)) {
        return true
      }
      if (data.spawnId == this.charFilter) {
        return true
      }
      if (data.entry == this.charFilter) {
        return true
      }
    },
    handleCurrentCharSpawnChange(row) {
      this.spawnInternalId = row.spawnId
    },
    handleDone() {
      this.$emit('change', this.spawnInternalId)
      this.$emit('update:visible', false)
    },
    handleCancel() {
      this.$emit('update:visible', false)
    },
  },
  template: `
    <el-dialog title="选择NPC实例" :visible="visible" @update:visible="handleCancel">
      <el-table :data="charSpawnInternalData.filter(handleCharSpawnFilter)" :height="450"
        highlight-current-row row-key="spawnId" :current-row-key="spawnInternalId"
        @current-change="handleCurrentCharSpawnChange">
        <el-table-column property="spawnId" label="ID" width="100"></el-table-column>
        <el-table-column property="entry" label="NPC原型" width="100"></el-table-column>
        <el-table-column property="name" label="NPC名称"></el-table-column>
        <el-table-column property="mapName" label="地图名称"></el-table-column>
        <el-table-column property="mapType" label="地图类型"></el-table-column>
        <el-table-column property="position" label="所在位置"></el-table-column>
      </el-table>
      <el-form :inline="true" style="margin-top: 6px">
        <el-form-item label="ID">
          <el-input :value="spawnInternalId" readonly style="width: 100px"></el-input>
        </el-form-item>
        <el-form-item label="名称">
          <el-input :value="spawnInternalName" readonly></el-input>
        </el-form-item>
        <el-form-item label="过滤">
          <el-input v-model="charFilter" suffix-icon="el-icon-search" placeholder="输入关键字搜索">
          </el-input>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="handleCancel">取消</el-button>
        <el-button type="primary" @click="handleDone">确定</el-button>
      </div>
    </el-dialog>
  `
})

Vue.component('my-char-proto-dialog', {
  model: {
    prop: 'charTypeId',
    event: 'change',
  },
  props: {
    charNames: {type: Object, required: true},
    charProtoData: {type: Array, required: true},
    charTypeId: {type: Number, required: true},
    visible: {type: Boolean, required: true},
  },
  data: function() {
    return {
      charInternalTypeId: this.charTypeId,
      charFilter: '',
    }
  },
  computed: {
    charInternalName: function() {
      return this.charNames[this.charInternalTypeId]
    },
    charProtoInternalData: function() {
      let tableRows = []
      for (let v of this.charProtoData) {
        let tableRow = Object.create(v)
        tableRow.name = this.charNames[v.charTypeId]
        tableRows.push(tableRow)
      }
      return tableRows
    },
  },
  methods: {
    handleCharProtoFilter(data) {
      if (this.charFilter == '') {
        return true
      }
      if (data.name.includes(this.charFilter)) {
        return true
      }
      if (data.charTypeId == this.charFilter) {
        return true
      }
    },
    handleCurrentCharProtoChange(row) {
      this.charInternalTypeId = row.charTypeId
    },
    handleDone() {
      this.$emit('change', this.charInternalTypeId)
      this.$emit('update:visible', false)
    },
    handleCancel() {
      this.$emit('update:visible', false)
    },
  },
  template: `
    <el-dialog title="选择NPC原型" :visible="visible" @update:visible="handleCancel">
      <el-table :data="charProtoInternalData.filter(handleCharProtoFilter)" :height="450"
        highlight-current-row row-key="charTypeId" :current-row-key="charInternalTypeId"
        @current-change="handleCurrentCharProtoChange">
        <el-table-column property="charTypeId" label="ID" width="100"></el-table-column>
        <el-table-column property="name" label="名称"></el-table-column>
        <el-table-column property="level" label="等级"></el-table-column>
      </el-table>
      <el-form :inline="true" style="margin-top: 6px">
        <el-form-item label="ID">
          <el-input :value="charInternalTypeId" readonly style="width: 100px"></el-input>
        </el-form-item>
        <el-form-item label="名称">
          <el-input :value="charInternalName" readonly></el-input>
        </el-form-item>
        <el-form-item label="过滤">
          <el-input v-model="charFilter" suffix-icon="el-icon-search" placeholder="输入关键字搜索">
          </el-input>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="handleCancel">取消</el-button>
        <el-button type="primary" @click="handleDone">确定</el-button>
      </div>
    </el-dialog>
  `
})

Vue.component('my-sobj-spawn-dialog', {
  model: {
    prop: 'spawnId',
    event: 'change',
  },
  props: {
    MapType: {type: Array, required: true},
    mapNames: {type: Object, required: true},
    sobjNames: {type: Object, required: true},
    sobjSpawnData: {type: Array, required: true},
    spawnId: {type: Number, required: true},
    visible: {type: Boolean, required: true},
  },
  data: function() {
    return {
      spawnInternalId: this.spawnId,
      sobjFilter: '',
    }
  },
  computed: {
    spawnInternalName: function() {
      let v = _.find(this.sobjSpawnInternalData, v => v.spawnId == this.spawnInternalId)
      return v !== undefined ? v.name : ''
    },
    sobjSpawnInternalData: function() {
      let tableRows = []
      for (let v of this.sobjSpawnData) {
        let tableRow = Object.create(v)
        tableRow.name = this.sobjNames[v.entry]
        tableRow.position = `${v.x},${v.y},${v.z}`
        tableRow.mapName = `(${v.map_id})${this.mapNames[v.map_id]}`
        tableRow.mapType = GetWebSelectOptionLabel(this.MapType, v.map_type)
        tableRows.push(tableRow)
      }
      return tableRows
    },
  },
  methods: {
    handleSObjSpawnFilter(data) {
      if (this.sobjFilter == '') {
        return true
      }
      if (data.name.includes(this.sobjFilter)) {
        return true
      }
      if (data.spawnId == this.sobjFilter) {
        return true
      }
      if (data.entry == this.sobjFilter) {
        return true
      }
    },
    handleCurrentSObjSpawnChange(row) {
      this.spawnInternalId = row.spawnId
    },
    handleDone() {
      this.$emit('change', this.spawnInternalId)
      this.$emit('update:visible', false)
    },
    handleCancel() {
      this.$emit('update:visible', false)
    },
  },
  template: `
    <el-dialog title="选择静物实例" :visible="visible" @update:visible="handleCancel">
      <el-table :data="sobjSpawnInternalData.filter(handleSObjSpawnFilter)" :height="450"
        highlight-current-row row-key="spawnId" :current-row-key="spawnInternalId"
        @current-change="handleCurrentSObjSpawnChange">
        <el-table-column property="spawnId" label="ID" width="100"></el-table-column>
        <el-table-column property="entry" label="静物原型" width="100"></el-table-column>
        <el-table-column property="name" label="静物名称"></el-table-column>
        <el-table-column property="mapName" label="地图名称"></el-table-column>
        <el-table-column property="mapType" label="地图类型"></el-table-column>
        <el-table-column property="position" label="所在位置"></el-table-column>
      </el-table>
      <el-form :inline="true" style="margin-top: 6px">
        <el-form-item label="ID">
          <el-input :value="spawnInternalId" readonly style="width: 100px"></el-input>
        </el-form-item>
        <el-form-item label="名称">
          <el-input :value="spawnInternalName" readonly></el-input>
        </el-form-item>
        <el-form-item label="过滤">
          <el-input v-model="sobjFilter" suffix-icon="el-icon-search" placeholder="输入关键字搜索">
          </el-input>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="handleCancel">取消</el-button>
        <el-button type="primary" @click="handleDone">确定</el-button>
      </div>
    </el-dialog>
  `
})

Vue.component('my-sobj-proto-dialog', {
  model: {
    prop: 'sobjTypeId',
    event: 'change',
  },
  props: {
    sobjNames: {type: Object, required: true},
    sobjProtoData: {type: Array, required: true},
    sobjTypeId: {type: Number, required: true},
    visible: {type: Boolean, required: true},
  },
  data: function() {
    return {
      sobjInternalTypeId: this.sobjTypeId,
      sobjFilter: '',
    }
  },
  computed: {
    sobjInternalName: function() {
      return this.sobjNames[this.sobjInternalTypeId]
    },
    sobjProtoInternalData: function() {
      let tableRows = []
      for (let v of this.sobjProtoData) {
        let tableRow = Object.create(v)
        tableRow.name = this.sobjNames[v.sobjTypeId]
        tableRows.push(tableRow)
      }
      return tableRows
    },
  },
  methods: {
    handleSObjProtoFilter(data) {
      if (this.sobjFilter == '') {
        return true
      }
      if (data.name.includes(this.sobjFilter)) {
        return true
      }
      if (data.sobjTypeId == this.sobjFilter) {
        return true
      }
    },
    handleCurrentSObjProtoChange(row) {
      this.sobjInternalTypeId = row.sobjTypeId
    },
    handleDone() {
      this.$emit('change', this.sobjInternalTypeId)
      this.$emit('update:visible', false)
    },
    handleCancel() {
      this.$emit('update:visible', false)
    },
  },
  template: `
    <el-dialog title="选择静物原型" :visible="visible" @update:visible="handleCancel">
      <el-table :data="sobjProtoInternalData.filter(handleSObjProtoFilter)" :height="450"
        highlight-current-row row-key="sobjTypeId" :current-row-key="sobjInternalTypeId"
        @current-change="handleCurrentSObjProtoChange">
        <el-table-column property="sobjTypeId" label="ID" width="100"></el-table-column>
        <el-table-column property="name" label="名称"></el-table-column>
        <el-table-column property="lootSetID" label="掉落"></el-table-column>
      </el-table>
      <el-form :inline="true" style="margin-top: 6px">
        <el-form-item label="ID">
          <el-input :value="sobjInternalTypeId" readonly style="width: 100px"></el-input>
        </el-form-item>
        <el-form-item label="名称">
          <el-input :value="sobjInternalName" readonly></el-input>
        </el-form-item>
        <el-form-item label="过滤">
          <el-input v-model="sobjFilter" suffix-icon="el-icon-search" placeholder="输入关键字搜索">
          </el-input>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="handleCancel">取消</el-button>
        <el-button type="primary" @click="handleDone">确定</el-button>
      </div>
    </el-dialog>
  `
})

Vue.component('my-cheque-type-value', {
  props: {
    uiChequeType: {type: Array, required: true},
    chequeInst: {type: Object, required: true},
    minimum: {type: Number, default: 0},
    props: {type: Object, default: () => ({type: 'chequeType', value: 'chequeValue'})},
  },
  template: `
    <el-row>
      <el-col style="width: 161px">
        <el-input>
          <el-select v-model="chequeInst[props.type]" slot="prepend" style="width: 160px">
            <el-option v-for="item in uiChequeType"
              :label="item.label" :value="item.value" :key="item.value"></el-option>
          </el-select>
        </el-input>
      </el-col>
      <el-col style="width: 180px">
        <el-input-number v-model="chequeInst[props.value]"
          controls-position="right" :precision="0" :min="minimum"></el-input-number>
      </el-col>
    </el-row>
  `
})

Vue.component('my-item-proto-number', {
  props: {
    itemData: {type: Array, required: true},
    itemInst: {type: Object, required: true},
    clearable: {type: Boolean, default: false},
    minimum: {type: Number, default: 0},
    props: {type: Object, default: () => ({id: 'itemId', num: 'itemNum'})},
  },
  data: function() {
    return {
      dialogVisible: false,
    }
  },
  computed: {
    btnColStyle: function() {
      return `width: ${30+(this.clearable?5+10+30:0)}px; margin-left: 15px;`
    },
  },
  template: `
    <el-row>
      <el-col style="width: 201px">
        <el-input>
          <div slot="prepend" style="width: 160px">
            {{$GetItemLabel(itemData, itemInst[props.id])}}</div>
        </el-input>
      </el-col>
      <el-col style="width: 180px">
        <el-input-number v-model="itemInst[props.num]"
          controls-position="right" :precision="0" :min="minimum"></el-input-number>
      </el-col>
      <el-col :style=btnColStyle>
        <my-item-dialog :visible.sync="dialogVisible" :item-data="itemData"
          v-model="itemInst[props.id]">
        </my-item-dialog>
        <el-button type="text"
          @click="dialogVisible=true">选择</el-button>
        <el-button type="text" v-if="clearable"
          @click="itemInst[props.id]=0">清除</el-button>
      </el-col>
    </el-row>
  `
})

Vue.component('my-spell-proto-level', {
  props: {
    spellData: {type: Array, required: true},
    spellInst: {type: Object, required: true},
    clearable: {type: Boolean, default: false},
    props: {type: Object, default: () => ({id: 'spellId', lv: 'spellLv'})},
  },
  data: function() {
    return {
      dialogVisible: false,
    }
  },
  computed: {
    btnColStyle: function() {
      return `width: ${30+(this.clearable?5+10+30:0)}px; margin-left: 15px;`
    },
  },
  template: `
    <el-row>
      <el-col style="width: 201px">
        <el-input>
          <div slot="prepend" style="width: 160px">
            {{$GetSpellLabel(spellData, spellInst[props.id])}}</div>
        </el-input>
      </el-col>
      <el-col style="width: 180px">
        <el-input-number v-model="spellInst[props.lv]"
          controls-position="right" :precision="0" :min="1"></el-input-number>
      </el-col>
      <el-col :style=btnColStyle>
        <my-spell-dialog :visible.sync="dialogVisible" :spell-data="spellData"
          v-model="spellInst[props.id]">
        </my-spell-dialog>
        <el-button type="text"
          @click="dialogVisible=true">选择</el-button>
        <el-button type="text" v-if="clearable"
          @click="spellInst[props.id]=0">清除</el-button>
      </el-col>
    </el-row>
  `
})

Vue.component('my-script-file-args', {
  props: {
    title: {type: String, required: true},
    spans: {type: Array, required: true},
    scriptData: {type: Array, required: true},
    scriptType: {type: Number, required: true},
    scriptInst: {type: Object, required: true},
    gutter: {type: Number, default: 0},
    clearable: {type: Boolean, default: false},
    props: {type: Object, default: () => ({id: 'scriptId', args: 'scriptArgs'})},
  },
  data: function() {
    return {
      dialogVisible: false,
    }
  },
  computed: {
    btnColStyle: function() {
      return `width: ${this.gutter+30+(this.clearable?5+10+30:0)}px;
              margin-left: ${15-this.gutter}px;`
    },
  },
  template: `
    <el-row :gutter="gutter">
      <el-col :span="spans[0]">
        <el-form-item :label="title">
          <el-input :value="$GetScriptableLabel(scriptData, scriptInst[props.id])"
            readonly></el-input>
        </el-form-item>
      </el-col>
      <el-col :style=btnColStyle>
        <my-script-dialog :visible.sync="dialogVisible" :script-data="scriptData"
          :script-type="scriptType" v-model="scriptInst[props.id]">
        </my-script-dialog>
        <el-button type="text"
          @click="dialogVisible=true">选择</el-button>
        <el-button type="text" v-if="clearable"
          @click="scriptInst[props.id]=0">清除</el-button>
      </el-col>
      <el-col :span="spans[1]" v-if="props.args">
        <el-form-item label="脚本参数">
          <el-input v-model="scriptInst[props.args]"></el-input>
        </el-form-item>
      </el-col>
      <slot></slot>
    </el-row>
  `
})

Vue.component('my-quest-proto', {
  props: {
    questData: {type: Array, required: true},
    questTypeId: {type: Number, required: true},
    clearable: {type: Boolean, default: false},
  },
  data: function() {
    return {
      dialogVisible: false,
      questInternalTypeId: this.questTypeId,
    }
  },
  computed: {
    btnColStyle: function() {
      return `width: ${30+(this.clearable?5+10+30:0)}px; margin-left: 15px;`
    },
  },
  watch: {
    questTypeId: function(value) {
      this.questInternalTypeId = value
    },
    questInternalTypeId: function(value) {
      this.$emit('update:questTypeId', value)
    },
  },
  template: `
    <el-row>
      <el-col style="width: 220px">
        <el-input :value="$GetQuestLabel(questData, questInternalTypeId)" readonly>
        </el-input>
      </el-col>
      <el-col :style=btnColStyle>
        <my-quest-dialog :visible.sync="dialogVisible" :quest-data="questData"
          v-model="questInternalTypeId">
        </my-quest-dialog>
        <el-button type="text"
          @click="dialogVisible=true">选择</el-button>
        <el-button type="text" v-if="clearable"
          @click="questInternalTypeId=0">清除</el-button>
      </el-col>
    </el-row>
  `
})

Vue.component('my-question-proto', {
  props: {
    questionData: {type: Array, required: true},
    questionId: {type: Number, required: true},
    clearable: {type: Boolean, default: false},
  },
  data: function() {
    return {
      dialogVisible: false,
      questionInternalId: this.questionId,
    }
  },
  computed: {
    btnColStyle: function() {
      return `width: ${30+(this.clearable?5+10+30:0)}px; margin-left: 15px;`
    },
  },
  watch: {
    questionId: function(value) {
      this.questionInternalId = value
    },
    questionInternalId: function(value) {
      this.$emit('update:questionId', value)
    },
  },
  template: `
    <el-row>
      <el-col style="width: 700px">
        <el-input :value="$GetQuestionLabel(questionData, questionInternalId)" type="textarea" readonly>
        </el-input>
      </el-col>
      <el-col :style=btnColStyle>
        <my-question-dialog :visible.sync="dialogVisible" :question-data="questionData"
          v-model="questionInternalId">
        </my-question-dialog>
        <el-button type="text"
          @click="dialogVisible=true">选择</el-button>
        <el-button type="text" v-if="clearable"
          @click="questionInternalId=0">清除</el-button>
      </el-col>
    </el-row>
  `
})

Vue.component('my-teleport-proto', {
  props: {
    tpData: {type: Array, required: true},
    tpId: {type: Number, required: true},
    clearable: {type: Boolean, default: false},
  },
  data: function() {
    return {
      dialogVisible: false,
      tpInternalId: this.tpId,
    }
  },
  computed: {
    btnColStyle: function() {
      return `width: ${30+(this.clearable?5+10+30:0)}px; margin-left: 15px;`
    },
  },
  watch: {
    tpId: function(value) {
      this.tpInternalId = value
    },
    tpInternalId: function(value) {
      this.$emit('update:tpId', value)
    },
  },
  template: `
    <el-row>
      <el-col style="width: 220px">
        <el-input :value="$GetTeleportLabel(tpData, tpInternalId)" readonly>
        </el-input>
      </el-col>
      <el-col :style=btnColStyle>
        <my-teleport-dialog :visible.sync="dialogVisible" :tp-data="tpData"
          v-model="tpInternalId">
        </my-teleport-dialog>
        <el-button type="text"
          @click="dialogVisible=true">选择</el-button>
        <el-button type="text" v-if="clearable"
          @click="tpInternalId=0">清除</el-button>
      </el-col>
    </el-row>
  `
})

Vue.component('my-loot-proto', {
  props: {
    lsData: {type: Array, required: true},
    lsId: {type: Number, required: true},
    clearable: {type: Boolean, default: false},
  },
  data: function() {
    return {
      dialogVisible: false,
      lsInternalId: this.lsId,
    }
  },
  computed: {
    btnColStyle: function() {
      return `width: ${30+(this.clearable?5+10+30:0)}px; margin-left: 15px;`
    },
  },
  watch: {
    lsId: function(value) {
      this.lsInternalId = value
    },
    lsInternalId: function(value) {
      this.$emit('update:lsId', value)
    },
  },
  template: `
    <el-row>
      <el-col style="width: 220px">
        <el-input :value="$GetLootLabel(lsData, lsInternalId)" readonly>
        </el-input>
      </el-col>
      <el-col :style=btnColStyle>
        <my-loot-dialog :visible.sync="dialogVisible" :ls-data="lsData"
          v-model="lsInternalId">
        </my-loot-dialog>
        <el-button type="text"
          @click="dialogVisible=true">选择</el-button>
        <el-button type="text" v-if="clearable"
          @click="lsInternalId=0">清除</el-button>
      </el-col>
    </el-row>
  `
})