package fusion

import (
	"io"
)

type RPCReqMetaInfo struct {
	sn uint64
}

type RPCRespMetaInfo struct {
	sn  uint64
	err int32
	eof bool
}

func ReadRPCReqMetaInfo(pck *NetPacket) *RPCReqMetaInfo {
	const infoLen = 8
	if pck.buffer.Len() < infoLen {
		panic(io.ErrUnexpectedEOF)
	}

	var buffer NetBuffer
	availLen := pck.buffer.Len() - infoLen
	buffer.AppendBytes(pck.buffer.Bytes()[availLen:])
	pck.buffer.Truncate(availLen)

	var info RPCReqMetaInfo
	buffer.ReadUInt64(&info.sn)
	return &info
}

func ReadRPCRespMetaInfo(pck *NetPacket) *RPCRespMetaInfo {
	const infoLen = 8 + 4 + 1
	if pck.buffer.Len() < infoLen {
		panic(io.ErrUnexpectedEOF)
	}

	var buffer NetBuffer
	availLen := pck.buffer.Len() - infoLen
	buffer.AppendBytes(pck.buffer.Bytes()[availLen:])
	pck.buffer.Truncate(availLen)

	var info RPCRespMetaInfo
	buffer.Read(&info.sn, &info.err, &info.eof)
	return &info
}

func (info *RPCRespMetaInfo) Err() int32 {
	return info.err
}

func (info *RPCRespMetaInfo) Eof() bool {
	return info.eof
}
