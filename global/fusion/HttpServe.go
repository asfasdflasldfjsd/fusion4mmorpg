package fusion

import (
	"net/http"

	"github.com/NYTimes/gziphandler"
)

func HttpHandleGzipFunc(mux *http.ServeMux, pattern string, handler func(http.ResponseWriter, *http.Request)) {
	mux.Handle(pattern, gziphandler.GzipHandler(http.HandlerFunc(handler)))
}
