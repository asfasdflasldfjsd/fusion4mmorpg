package main

import (
	"net/http"
	"os"
	"path"
)

func handleStaticFile(w http.ResponseWriter, r *http.Request) {
	header := w.Header()
	switch path.Ext(r.URL.Path) {
	case ".css":
		header.Set("content-type", "text/css")
	case ".js":
		header.Set("content-type", "application/x-javascript")
	}
	dir, _ := os.Getwd()
	http.ServeFile(w, r, path.Join(dir, r.URL.Path))
}
