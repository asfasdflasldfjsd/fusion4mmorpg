package main

import (
	"database/sql"
)

func getTreeView(db *sql.DB, Type int) ([]byte, error) {
	var treeView []byte
	err := db.QueryRow(
		"SELECT `etvTree` FROM `editor_tree_view` WHERE `etvType`=?",
		Type).Scan(&treeView)
	if err != nil {
		return nil, err
	}
	return treeView, nil
}

func setTreeView(db *sql.DB, Type int, treeView []byte) error {
	_, err := db.Exec(
		"REPLACE INTO `editor_tree_view` VALUES(?,?)", Type, treeView)
	return err
}
