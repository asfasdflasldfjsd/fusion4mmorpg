package main

import (
	"database/sql"

	"test.com/worlddb"
)

type teleportDialogTableRow struct {
	TpID   uint32 `json:"tpID"`
	TpName string `json:"tpName"`
}

func getAllTeleport4DialogTable(db *sql.DB) ([]*teleportDialogTableRow, error) {
	var tpRows []*teleportDialogTableRow
	rows, err := db.Query("SELECT `Id`,`name` FROM `teleport_point`")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var tpRow teleportDialogTableRow
		if err := rows.Scan(&tpRow.TpID, &tpRow.TpName); err == nil {
			tpRows = append(tpRows, &tpRow)
		} else {
			return nil, err
		}
	}
	return tpRows, nil
}

type landmarkDialogTableRow struct {
	LmID   uint32 `json:"lmID"`
	LmName string `json:"lmName"`
}

func getAllLandmark4DialogTable(db *sql.DB) ([]*landmarkDialogTableRow, error) {
	var lmRows []*landmarkDialogTableRow
	rows, err := db.Query("SELECT `Id`,`name` FROM `landmark_point`")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var lmRow landmarkDialogTableRow
		if err := rows.Scan(&lmRow.LmID, &lmRow.LmName); err == nil {
			lmRows = append(lmRows, &lmRow)
		} else {
			return nil, err
		}
	}
	return lmRows, nil
}

func getAllMapNames(db *sql.DB) (map[uint32]string, error) {
	return getAllKeyI18Ns(
		db, "map_info", "Id", worlddb.STRING_TEXT_TYPE_MAP_NAME)
}
