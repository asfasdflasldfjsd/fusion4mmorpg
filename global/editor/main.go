package main

import (
	"database/sql"

	"test.com/fusion"
)

var cfg userConfig
var worldDB *sql.DB

type userConfig struct {
	WebListen string
	WorldDB   fusion.MysqlConfig
}

func main() {
	fusion.LoadUserConfig("editor.json", &cfg)
	worldDB = fusion.OpenMysql(&cfg.WorldDB, 32, 1)
	runWebListener()
}
