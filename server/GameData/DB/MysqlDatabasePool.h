#pragma once

#include "Singleton.h"
#include "mysql/MysqlDatabase.h"

class MysqlDatabasePool : public Singleton<MysqlDatabasePool>
{
public:
	MysqlDatabasePool();
	virtual ~MysqlDatabasePool();

	bool InitWorldDB(const char* host, unsigned int port,
					 const char* user, const char* passwd, const char* db,
					 unsigned int connInit = 1, unsigned int connMax = 5);
	bool InitActvtDB(const char* host, unsigned int port,
					 const char* user, const char* passwd, const char* db,
					 unsigned int connInit = 1, unsigned int connMax = 5);
	bool InitCharDB(const char* host, unsigned int port,
					const char* user, const char* passwd, const char* db,
					unsigned int connInit = 1, unsigned int connMax = 5);
	bool InitLogDB(const char* host, unsigned int port,
				   const char* user, const char* passwd, const char* db,
				   unsigned int connInit = 1, unsigned int connMax = 5);

	MysqlDatabase* GetWorldDB() const { return m_pWorldDB; }
	MysqlDatabase* GetActvtDB() const { return m_pActvtDB; }
	MysqlDatabase* GetCharDB() const { return m_pCharDB; }
	MysqlDatabase* GetLogDB() const { return m_pLogDB; }

private:
	MysqlDatabase* m_pWorldDB;
	MysqlDatabase* m_pActvtDB;
	MysqlDatabase* m_pCharDB;
	MysqlDatabase* m_pLogDB;
};

#define sMysqlDatabasePool (*MysqlDatabasePool::instance())
