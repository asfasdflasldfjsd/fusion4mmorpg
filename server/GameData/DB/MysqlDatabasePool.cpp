#include "MysqlDatabasePool.h"

MysqlDatabasePool::MysqlDatabasePool()
{
	m_pWorldDB = new MysqlDatabase();
	m_pActvtDB = new MysqlDatabase();
	m_pCharDB = new MysqlDatabase();
	m_pLogDB = new MysqlDatabase();
}

MysqlDatabasePool::~MysqlDatabasePool()
{
	delete m_pWorldDB;
	delete m_pActvtDB;
	delete m_pCharDB;
	delete m_pLogDB;
}

bool MysqlDatabasePool::InitWorldDB(const char* host, unsigned int port,
	const char* user, const char* passwd, const char* db,
	unsigned int connInit, unsigned int connMax)
{
	return m_pWorldDB->Connect(host, port, user, passwd, db, connInit, connMax);
}

bool MysqlDatabasePool::InitActvtDB(const char* host, unsigned int port,
	const char* user, const char* passwd, const char* db,
	unsigned int connInit, unsigned int connMax)
{
	return m_pActvtDB->Connect(host, port, user, passwd, db, connInit, connMax);
}

bool MysqlDatabasePool::InitCharDB(const char* host, unsigned int port,
	const char* user, const char* passwd, const char* db,
	unsigned int connInit, unsigned int connMax)
{
	return m_pCharDB->Connect(host, port, user, passwd, db, connInit, connMax);
}

bool MysqlDatabasePool::InitLogDB(const char* host, unsigned int port,
	const char* user, const char* passwd, const char* db,
	unsigned int connInit, unsigned int connMax)
{
	return m_pLogDB->Connect(host, port, user, passwd, db, connInit, connMax);
}
