#pragma once

enum DBPError
{
	DBPErrorNone,
	DBPErrorBegin = 100,
	DBPErrorConnectMysqlFailed,
	DBPErrorQueryMysqlFailed,
	DBPErrorInsertMysqlFailed,
	DBPErrorUpdateMysqlFailed,
	DBPErrorDeleteMysqlFailed,
};
