#include "jsontable/table_helper.h"
#include "inst_guild.h"

GuildApply::GuildApply()
: playerId(0)
, guildId(0)
, applyTime(0)
{
}

GuildMember::GuildMember()
: playerId(0)
, guildId(0)
, guildTitle(0)
, joinTime(0)
{
}

GuildInformation::GuildInformation()
: Id(0)
, gsId(0)
, buildTime(0)
, level(0)
, levelTime(0)
{
}
