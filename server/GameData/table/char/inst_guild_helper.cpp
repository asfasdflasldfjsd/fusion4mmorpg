#include "jsontable/table_helper.h"
#include "inst_guild.h"

template<> void LoadFromStream(GUILD_TITLE &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const GUILD_TITLE &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(GUILD_TITLE &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const GUILD_TITLE &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(GUILD_TITLE &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const GUILD_TITLE &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> const char *GetTableName<GuildApply>()
{
	return "guild_apply";
}

template<> const char *GetTableKeyName<GuildApply>()
{
	return "";
}

template<> uint32 GetTableKeyValue(const GuildApply &entity)
{
	return 0;
}

template<> void SetTableKeyValue(GuildApply &entity, uint32 key)
{
}

template<> const char *GetTableFieldNameByIndex<GuildApply>(size_t index)
{
	switch (index)
	{
		case 0: return "playerId";
		case 1: return "guildId";
		case 2: return "applyTime";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<GuildApply>(const char *name)
{
	if (strcmp(name, "playerId") == 0) return 0;
	if (strcmp(name, "guildId") == 0) return 1;
	if (strcmp(name, "applyTime") == 0) return 2;
	return -1;
}

template<> size_t GetTableFieldNumber<GuildApply>()
{
	return 3;
}

template<> std::string GetTableFieldValue(const GuildApply &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.playerId);
		case 1: return StringHelper::ToString(entity.guildId);
		case 2: return StringHelper::ToString(entity.applyTime);
	}
	return "";
}

template<> void SetTableFieldValue(GuildApply &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.playerId, value);
		case 1: return StringHelper::FromString(entity.guildId, value);
		case 2: return StringHelper::FromString(entity.applyTime, value);
	}
}

template<> void LoadFromStream(GuildApply &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.playerId, stream);
	StreamHelper::FromStream(entity.guildId, stream);
	StreamHelper::FromStream(entity.applyTime, stream);
}

template<> void SaveToStream(const GuildApply &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.playerId, stream);
	StreamHelper::ToStream(entity.guildId, stream);
	StreamHelper::ToStream(entity.applyTime, stream);
}

template<> void LoadFromText(GuildApply &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const GuildApply &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(GuildApply &entity, const rapidjson::Value &value)
{
	FromJson(entity.playerId, value, "playerId");
	FromJson(entity.guildId, value, "guildId");
	FromJson(entity.applyTime, value, "applyTime");
}

template<> void JsonHelper::BlockToJson(const GuildApply &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.playerId, value, "playerId");
	ToJson(entity.guildId, value, "guildId");
	ToJson(entity.applyTime, value, "applyTime");
}

template<> const char *GetTableName<GuildMember>()
{
	return "guild_member";
}

template<> const char *GetTableKeyName<GuildMember>()
{
	return "playerId";
}

template<> uint32 GetTableKeyValue(const GuildMember &entity)
{
	return entity.playerId;
}

template<> void SetTableKeyValue(GuildMember &entity, uint32 key)
{
	entity.playerId = key;
}

template<> const char *GetTableFieldNameByIndex<GuildMember>(size_t index)
{
	switch (index)
	{
		case 0: return "playerId";
		case 1: return "guildId";
		case 2: return "guildTitle";
		case 3: return "joinTime";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<GuildMember>(const char *name)
{
	if (strcmp(name, "playerId") == 0) return 0;
	if (strcmp(name, "guildId") == 0) return 1;
	if (strcmp(name, "guildTitle") == 0) return 2;
	if (strcmp(name, "joinTime") == 0) return 3;
	return -1;
}

template<> size_t GetTableFieldNumber<GuildMember>()
{
	return 4;
}

template<> std::string GetTableFieldValue(const GuildMember &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.playerId);
		case 1: return StringHelper::ToString(entity.guildId);
		case 2: return StringHelper::ToString(entity.guildTitle);
		case 3: return StringHelper::ToString(entity.joinTime);
	}
	return "";
}

template<> void SetTableFieldValue(GuildMember &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.playerId, value);
		case 1: return StringHelper::FromString(entity.guildId, value);
		case 2: return StringHelper::FromString(entity.guildTitle, value);
		case 3: return StringHelper::FromString(entity.joinTime, value);
	}
}

template<> void LoadFromStream(GuildMember &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.playerId, stream);
	StreamHelper::FromStream(entity.guildId, stream);
	StreamHelper::FromStream(entity.guildTitle, stream);
	StreamHelper::FromStream(entity.joinTime, stream);
}

template<> void SaveToStream(const GuildMember &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.playerId, stream);
	StreamHelper::ToStream(entity.guildId, stream);
	StreamHelper::ToStream(entity.guildTitle, stream);
	StreamHelper::ToStream(entity.joinTime, stream);
}

template<> void LoadFromText(GuildMember &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const GuildMember &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(GuildMember &entity, const rapidjson::Value &value)
{
	FromJson(entity.playerId, value, "playerId");
	FromJson(entity.guildId, value, "guildId");
	FromJson(entity.guildTitle, value, "guildTitle");
	FromJson(entity.joinTime, value, "joinTime");
}

template<> void JsonHelper::BlockToJson(const GuildMember &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.playerId, value, "playerId");
	ToJson(entity.guildId, value, "guildId");
	ToJson(entity.guildTitle, value, "guildTitle");
	ToJson(entity.joinTime, value, "joinTime");
}

template<> const char *GetTableName<GuildInformation>()
{
	return "guild";
}

template<> const char *GetTableKeyName<GuildInformation>()
{
	return "Id";
}

template<> uint32 GetTableKeyValue(const GuildInformation &entity)
{
	return entity.Id;
}

template<> void SetTableKeyValue(GuildInformation &entity, uint32 key)
{
	entity.Id = key;
}

template<> const char *GetTableFieldNameByIndex<GuildInformation>(size_t index)
{
	switch (index)
	{
		case 0: return "Id";
		case 1: return "gsId";
		case 2: return "name";
		case 3: return "buildTime";
		case 4: return "level";
		case 5: return "levelTime";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<GuildInformation>(const char *name)
{
	if (strcmp(name, "Id") == 0) return 0;
	if (strcmp(name, "gsId") == 0) return 1;
	if (strcmp(name, "name") == 0) return 2;
	if (strcmp(name, "buildTime") == 0) return 3;
	if (strcmp(name, "level") == 0) return 4;
	if (strcmp(name, "levelTime") == 0) return 5;
	return -1;
}

template<> size_t GetTableFieldNumber<GuildInformation>()
{
	return 6;
}

template<> std::string GetTableFieldValue(const GuildInformation &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.Id);
		case 1: return StringHelper::ToString(entity.gsId);
		case 2: return StringHelper::ToString(entity.name);
		case 3: return StringHelper::ToString(entity.buildTime);
		case 4: return StringHelper::ToString(entity.level);
		case 5: return StringHelper::ToString(entity.levelTime);
	}
	return "";
}

template<> void SetTableFieldValue(GuildInformation &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.Id, value);
		case 1: return StringHelper::FromString(entity.gsId, value);
		case 2: return StringHelper::FromString(entity.name, value);
		case 3: return StringHelper::FromString(entity.buildTime, value);
		case 4: return StringHelper::FromString(entity.level, value);
		case 5: return StringHelper::FromString(entity.levelTime, value);
	}
}

template<> void LoadFromStream(GuildInformation &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.Id, stream);
	StreamHelper::FromStream(entity.gsId, stream);
	StreamHelper::FromStream(entity.name, stream);
	StreamHelper::FromStream(entity.buildTime, stream);
	StreamHelper::FromStream(entity.level, stream);
	StreamHelper::FromStream(entity.levelTime, stream);
}

template<> void SaveToStream(const GuildInformation &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.Id, stream);
	StreamHelper::ToStream(entity.gsId, stream);
	StreamHelper::ToStream(entity.name, stream);
	StreamHelper::ToStream(entity.buildTime, stream);
	StreamHelper::ToStream(entity.level, stream);
	StreamHelper::ToStream(entity.levelTime, stream);
}

template<> void LoadFromText(GuildInformation &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const GuildInformation &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(GuildInformation &entity, const rapidjson::Value &value)
{
	FromJson(entity.Id, value, "Id");
	FromJson(entity.gsId, value, "gsId");
	FromJson(entity.name, value, "name");
	FromJson(entity.buildTime, value, "buildTime");
	FromJson(entity.level, value, "level");
	FromJson(entity.levelTime, value, "levelTime");
}

template<> void JsonHelper::BlockToJson(const GuildInformation &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.Id, value, "Id");
	ToJson(entity.gsId, value, "gsId");
	ToJson(entity.name, value, "name");
	ToJson(entity.buildTime, value, "buildTime");
	ToJson(entity.level, value, "level");
	ToJson(entity.levelTime, value, "levelTime");
}
