#pragma once

struct IpcFlags
{
	IpcFlags();

	bool isInit;
};

struct IpcAppearance
{
	IpcAppearance();

	uint32 charTypeId;
	uint8 hairType;
	uint8 hairColor;
	uint8 face;
	uint8 skin;
};

struct IpcPropertyValue
{
	IpcPropertyValue();

	uint32 bagCapacity;
	uint32 bankCapacity;
};

struct IpcRankValue
{
	IpcRankValue();

	int64 lastLevelTime;
	int64 lastFightValueTime;
};

struct IpcGsReadValue
{
	IpcGsReadValue();

	uint64 lastFightValue;
};

struct IpcGsWriteValue
{
	IpcGsWriteValue();

	int64 guildScore;
};

struct InstPlayerPreviewInfo
{
	InstPlayerPreviewInfo();

	uint32 ipcInstID;
	std::string ipcNickName;
	IpcAppearance ipcAppearance;
	uint8 ipcCareer;
	uint8 ipcGender;
	uint32 ipcLevel;
	int64 ipcMoneyGold;
	int64 ipcMoneyDiamond;
	std::string ipcEffectItems;
};

struct InstPlayerOutlineInfo
{
	InstPlayerOutlineInfo();

	uint32 ipcInstID;
	uint32 ipcAcctID;
	std::string ipcNickName;
	IpcFlags ipcFlags;
	uint8 ipcCareer;
	uint8 ipcGender;
	uint16 ipcMapType;
	uint16 ipcMapID;
	float ipcPosX;
	float ipcPosY;
	float ipcPosZ;
	float ipcPosO;
	uint32 ipcLevel;
	uint32 ipcServerID;
	int64 ipcLastOnlineTime;
	IpcRankValue ipcRankValue;
	IpcGsReadValue ipcGsReadValue;
	IpcGsWriteValue ipcGsWriteValue;
};

struct inst_player_char
{
	inst_player_char();

	uint32 ipcInstID;
	uint32 ipcAcctID;
	std::string ipcNickName;
	IpcFlags ipcFlags;
	IpcAppearance ipcAppearance;
	uint8 ipcCareer;
	uint8 ipcGender;
	uint16 ipcMapType;
	uint16 ipcMapID;
	float ipcPosX;
	float ipcPosY;
	float ipcPosZ;
	float ipcPosO;
	uint32 ipcLevel;
	int64 ipcExp;
	int64 ipcCurHP;
	int64 ipcCurMP;
	int64 ipcMoneyGold;
	int64 ipcMoneyDiamond;
	uint32 ipcServerID;
	int64 ipcCreateTime;
	int64 ipcLastLoginTime;
	int64 ipcLastOnlineTime;
	int64 ipcDeleteTime;
	std::string ipcQuestsDone;
	std::string ipcQuests;
	std::string ipcEffectItems;
	std::string ipcOtherItems;
	std::string ipcItemStorage;
	std::string ipcShopStatus;
	std::string ipcSpells;
	std::string ipcBuffs;
	std::string ipcCooldowns;
	std::string ipcJsonValue;
	std::vector<float> ipcF32Values;
	std::vector<int32> ipcS32Values;
	std::vector<int64> ipcS64Values;
	IpcPropertyValue ipcPropertyValue;
	IpcRankValue ipcRankValue;
	IpcGsReadValue ipcGsReadValue;
	IpcGsWriteValue ipcGsWriteValue;
};
