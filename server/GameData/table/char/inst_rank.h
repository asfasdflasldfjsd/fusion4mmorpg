#pragma once

enum class RANK_TYPE
{
	PLAYER,
	GUILD,
};

enum class RANK_SUBTYPE
{
	// player
	PLAYER_LEVEL = 0,
	PLAYER_FIGHT_VALUE,
	PLAYER_COUNT,

	// guild
	GUILD_LEVEL = 0,
	GUILD_COUNT,
};
