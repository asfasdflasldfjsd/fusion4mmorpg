#pragma once

enum class GUILD_TITLE
{
	INVALID = -1,
	MASTER = 0,
	SUBMASTER,
	OFFICER,
	MEMBER,
	COUNT,
};

struct GuildApply
{
	GuildApply();

	uint32 playerId;
	uint32 guildId;
	int64 applyTime;
};

struct GuildMember
{
	GuildMember();

	uint32 playerId;
	uint32 guildId;
	int8 guildTitle;
	int64 joinTime;
};

struct GuildInformation
{
	GuildInformation();

	uint32 Id;
	uint32 gsId;
	std::string name;
	int64 buildTime;
	uint32 level;
	int64 levelTime;
};
