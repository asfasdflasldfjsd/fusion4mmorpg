#include "jsontable/table_helper.h"
#include "inst_player_char.h"

template<> void LoadFromStream(IpcFlags &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.isInit, stream);
}

template<> void SaveToStream(const IpcFlags &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.isInit, stream);
}

template<> void LoadFromText(IpcFlags &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const IpcFlags &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(IpcFlags &entity, const rapidjson::Value &value)
{
	FromJson(entity.isInit, value, "isInit");
}

template<> void JsonHelper::BlockToJson(const IpcFlags &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.isInit, value, "isInit");
}

template<> void LoadFromStream(IpcAppearance &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.charTypeId, stream);
	StreamHelper::FromStream(entity.hairType, stream);
	StreamHelper::FromStream(entity.hairColor, stream);
	StreamHelper::FromStream(entity.face, stream);
	StreamHelper::FromStream(entity.skin, stream);
}

template<> void SaveToStream(const IpcAppearance &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.charTypeId, stream);
	StreamHelper::ToStream(entity.hairType, stream);
	StreamHelper::ToStream(entity.hairColor, stream);
	StreamHelper::ToStream(entity.face, stream);
	StreamHelper::ToStream(entity.skin, stream);
}

template<> void LoadFromText(IpcAppearance &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const IpcAppearance &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(IpcAppearance &entity, const rapidjson::Value &value)
{
	FromJson(entity.charTypeId, value, "charTypeId");
	FromJson(entity.hairType, value, "hairType");
	FromJson(entity.hairColor, value, "hairColor");
	FromJson(entity.face, value, "face");
	FromJson(entity.skin, value, "skin");
}

template<> void JsonHelper::BlockToJson(const IpcAppearance &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.charTypeId, value, "charTypeId");
	ToJson(entity.hairType, value, "hairType");
	ToJson(entity.hairColor, value, "hairColor");
	ToJson(entity.face, value, "face");
	ToJson(entity.skin, value, "skin");
}

template<> void LoadFromStream(IpcPropertyValue &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.bagCapacity, stream);
	StreamHelper::FromStream(entity.bankCapacity, stream);
}

template<> void SaveToStream(const IpcPropertyValue &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.bagCapacity, stream);
	StreamHelper::ToStream(entity.bankCapacity, stream);
}

template<> void LoadFromText(IpcPropertyValue &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const IpcPropertyValue &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(IpcPropertyValue &entity, const rapidjson::Value &value)
{
	FromJson(entity.bagCapacity, value, "bagCapacity");
	FromJson(entity.bankCapacity, value, "bankCapacity");
}

template<> void JsonHelper::BlockToJson(const IpcPropertyValue &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.bagCapacity, value, "bagCapacity");
	ToJson(entity.bankCapacity, value, "bankCapacity");
}

template<> void LoadFromStream(IpcRankValue &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.lastLevelTime, stream);
	StreamHelper::FromStream(entity.lastFightValueTime, stream);
}

template<> void SaveToStream(const IpcRankValue &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.lastLevelTime, stream);
	StreamHelper::ToStream(entity.lastFightValueTime, stream);
}

template<> void LoadFromText(IpcRankValue &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const IpcRankValue &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(IpcRankValue &entity, const rapidjson::Value &value)
{
	FromJson(entity.lastLevelTime, value, "lastLevelTime");
	FromJson(entity.lastFightValueTime, value, "lastFightValueTime");
}

template<> void JsonHelper::BlockToJson(const IpcRankValue &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.lastLevelTime, value, "lastLevelTime");
	ToJson(entity.lastFightValueTime, value, "lastFightValueTime");
}

template<> void LoadFromStream(IpcGsReadValue &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.lastFightValue, stream);
}

template<> void SaveToStream(const IpcGsReadValue &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.lastFightValue, stream);
}

template<> void LoadFromText(IpcGsReadValue &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const IpcGsReadValue &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(IpcGsReadValue &entity, const rapidjson::Value &value)
{
	FromJson(entity.lastFightValue, value, "lastFightValue");
}

template<> void JsonHelper::BlockToJson(const IpcGsReadValue &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.lastFightValue, value, "lastFightValue");
}

template<> void LoadFromStream(IpcGsWriteValue &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.guildScore, stream);
}

template<> void SaveToStream(const IpcGsWriteValue &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.guildScore, stream);
}

template<> void LoadFromText(IpcGsWriteValue &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const IpcGsWriteValue &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(IpcGsWriteValue &entity, const rapidjson::Value &value)
{
	FromJson(entity.guildScore, value, "guildScore");
}

template<> void JsonHelper::BlockToJson(const IpcGsWriteValue &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.guildScore, value, "guildScore");
}

template<> const char *GetTableName<InstPlayerPreviewInfo>()
{
	return "inst_player_char";
}

template<> const char *GetTableKeyName<InstPlayerPreviewInfo>()
{
	return "ipcInstID";
}

template<> uint32 GetTableKeyValue(const InstPlayerPreviewInfo &entity)
{
	return entity.ipcInstID;
}

template<> void SetTableKeyValue(InstPlayerPreviewInfo &entity, uint32 key)
{
	entity.ipcInstID = key;
}

template<> const char *GetTableFieldNameByIndex<InstPlayerPreviewInfo>(size_t index)
{
	switch (index)
	{
		case 0: return "ipcInstID";
		case 1: return "ipcNickName";
		case 2: return "ipcAppearance";
		case 3: return "ipcCareer";
		case 4: return "ipcGender";
		case 5: return "ipcLevel";
		case 6: return "ipcMoneyGold";
		case 7: return "ipcMoneyDiamond";
		case 8: return "ipcEffectItems";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<InstPlayerPreviewInfo>(const char *name)
{
	if (strcmp(name, "ipcInstID") == 0) return 0;
	if (strcmp(name, "ipcNickName") == 0) return 1;
	if (strcmp(name, "ipcAppearance") == 0) return 2;
	if (strcmp(name, "ipcCareer") == 0) return 3;
	if (strcmp(name, "ipcGender") == 0) return 4;
	if (strcmp(name, "ipcLevel") == 0) return 5;
	if (strcmp(name, "ipcMoneyGold") == 0) return 6;
	if (strcmp(name, "ipcMoneyDiamond") == 0) return 7;
	if (strcmp(name, "ipcEffectItems") == 0) return 8;
	return -1;
}

template<> size_t GetTableFieldNumber<InstPlayerPreviewInfo>()
{
	return 9;
}

template<> std::string GetTableFieldValue(const InstPlayerPreviewInfo &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.ipcInstID);
		case 1: return StringHelper::ToString(entity.ipcNickName);
		case 2: return JsonHelper::BlockToJsonText(entity.ipcAppearance);
		case 3: return StringHelper::ToString(entity.ipcCareer);
		case 4: return StringHelper::ToString(entity.ipcGender);
		case 5: return StringHelper::ToString(entity.ipcLevel);
		case 6: return StringHelper::ToString(entity.ipcMoneyGold);
		case 7: return StringHelper::ToString(entity.ipcMoneyDiamond);
		case 8: return StringHelper::ToString(entity.ipcEffectItems);
	}
	return "";
}

template<> void SetTableFieldValue(InstPlayerPreviewInfo &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.ipcInstID, value);
		case 1: return StringHelper::FromString(entity.ipcNickName, value);
		case 2: return JsonHelper::BlockFromJsonText(entity.ipcAppearance, value);
		case 3: return StringHelper::FromString(entity.ipcCareer, value);
		case 4: return StringHelper::FromString(entity.ipcGender, value);
		case 5: return StringHelper::FromString(entity.ipcLevel, value);
		case 6: return StringHelper::FromString(entity.ipcMoneyGold, value);
		case 7: return StringHelper::FromString(entity.ipcMoneyDiamond, value);
		case 8: return StringHelper::FromString(entity.ipcEffectItems, value);
	}
}

template<> void LoadFromStream(InstPlayerPreviewInfo &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.ipcInstID, stream);
	StreamHelper::FromStream(entity.ipcNickName, stream);
	LoadFromStream(entity.ipcAppearance, stream);
	StreamHelper::FromStream(entity.ipcCareer, stream);
	StreamHelper::FromStream(entity.ipcGender, stream);
	StreamHelper::FromStream(entity.ipcLevel, stream);
	StreamHelper::FromStream(entity.ipcMoneyGold, stream);
	StreamHelper::FromStream(entity.ipcMoneyDiamond, stream);
	StreamHelper::FromStream(entity.ipcEffectItems, stream);
}

template<> void SaveToStream(const InstPlayerPreviewInfo &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.ipcInstID, stream);
	StreamHelper::ToStream(entity.ipcNickName, stream);
	SaveToStream(entity.ipcAppearance, stream);
	StreamHelper::ToStream(entity.ipcCareer, stream);
	StreamHelper::ToStream(entity.ipcGender, stream);
	StreamHelper::ToStream(entity.ipcLevel, stream);
	StreamHelper::ToStream(entity.ipcMoneyGold, stream);
	StreamHelper::ToStream(entity.ipcMoneyDiamond, stream);
	StreamHelper::ToStream(entity.ipcEffectItems, stream);
}

template<> void LoadFromText(InstPlayerPreviewInfo &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const InstPlayerPreviewInfo &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(InstPlayerPreviewInfo &entity, const rapidjson::Value &value)
{
	FromJson(entity.ipcInstID, value, "ipcInstID");
	FromJson(entity.ipcNickName, value, "ipcNickName");
	BlockFromJson(entity.ipcAppearance, value, "ipcAppearance");
	FromJson(entity.ipcCareer, value, "ipcCareer");
	FromJson(entity.ipcGender, value, "ipcGender");
	FromJson(entity.ipcLevel, value, "ipcLevel");
	FromJson(entity.ipcMoneyGold, value, "ipcMoneyGold");
	FromJson(entity.ipcMoneyDiamond, value, "ipcMoneyDiamond");
	FromJson(entity.ipcEffectItems, value, "ipcEffectItems");
}

template<> void JsonHelper::BlockToJson(const InstPlayerPreviewInfo &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.ipcInstID, value, "ipcInstID");
	ToJson(entity.ipcNickName, value, "ipcNickName");
	BlockToJson(entity.ipcAppearance, value, "ipcAppearance");
	ToJson(entity.ipcCareer, value, "ipcCareer");
	ToJson(entity.ipcGender, value, "ipcGender");
	ToJson(entity.ipcLevel, value, "ipcLevel");
	ToJson(entity.ipcMoneyGold, value, "ipcMoneyGold");
	ToJson(entity.ipcMoneyDiamond, value, "ipcMoneyDiamond");
	ToJson(entity.ipcEffectItems, value, "ipcEffectItems");
}

template<> const char *GetTableName<InstPlayerOutlineInfo>()
{
	return "inst_player_char";
}

template<> const char *GetTableKeyName<InstPlayerOutlineInfo>()
{
	return "ipcInstID";
}

template<> uint32 GetTableKeyValue(const InstPlayerOutlineInfo &entity)
{
	return entity.ipcInstID;
}

template<> void SetTableKeyValue(InstPlayerOutlineInfo &entity, uint32 key)
{
	entity.ipcInstID = key;
}

template<> const char *GetTableFieldNameByIndex<InstPlayerOutlineInfo>(size_t index)
{
	switch (index)
	{
		case 0: return "ipcInstID";
		case 1: return "ipcAcctID";
		case 2: return "ipcNickName";
		case 3: return "ipcFlags";
		case 4: return "ipcCareer";
		case 5: return "ipcGender";
		case 6: return "ipcMapType";
		case 7: return "ipcMapID";
		case 8: return "ipcPosX";
		case 9: return "ipcPosY";
		case 10: return "ipcPosZ";
		case 11: return "ipcPosO";
		case 12: return "ipcLevel";
		case 13: return "ipcServerID";
		case 14: return "ipcLastOnlineTime";
		case 15: return "ipcRankValue";
		case 16: return "ipcGsReadValue";
		case 17: return "ipcGsWriteValue";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<InstPlayerOutlineInfo>(const char *name)
{
	if (strcmp(name, "ipcInstID") == 0) return 0;
	if (strcmp(name, "ipcAcctID") == 0) return 1;
	if (strcmp(name, "ipcNickName") == 0) return 2;
	if (strcmp(name, "ipcFlags") == 0) return 3;
	if (strcmp(name, "ipcCareer") == 0) return 4;
	if (strcmp(name, "ipcGender") == 0) return 5;
	if (strcmp(name, "ipcMapType") == 0) return 6;
	if (strcmp(name, "ipcMapID") == 0) return 7;
	if (strcmp(name, "ipcPosX") == 0) return 8;
	if (strcmp(name, "ipcPosY") == 0) return 9;
	if (strcmp(name, "ipcPosZ") == 0) return 10;
	if (strcmp(name, "ipcPosO") == 0) return 11;
	if (strcmp(name, "ipcLevel") == 0) return 12;
	if (strcmp(name, "ipcServerID") == 0) return 13;
	if (strcmp(name, "ipcLastOnlineTime") == 0) return 14;
	if (strcmp(name, "ipcRankValue") == 0) return 15;
	if (strcmp(name, "ipcGsReadValue") == 0) return 16;
	if (strcmp(name, "ipcGsWriteValue") == 0) return 17;
	return -1;
}

template<> size_t GetTableFieldNumber<InstPlayerOutlineInfo>()
{
	return 18;
}

template<> std::string GetTableFieldValue(const InstPlayerOutlineInfo &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.ipcInstID);
		case 1: return StringHelper::ToString(entity.ipcAcctID);
		case 2: return StringHelper::ToString(entity.ipcNickName);
		case 3: return JsonHelper::BlockToJsonText(entity.ipcFlags);
		case 4: return StringHelper::ToString(entity.ipcCareer);
		case 5: return StringHelper::ToString(entity.ipcGender);
		case 6: return StringHelper::ToString(entity.ipcMapType);
		case 7: return StringHelper::ToString(entity.ipcMapID);
		case 8: return StringHelper::ToString(entity.ipcPosX);
		case 9: return StringHelper::ToString(entity.ipcPosY);
		case 10: return StringHelper::ToString(entity.ipcPosZ);
		case 11: return StringHelper::ToString(entity.ipcPosO);
		case 12: return StringHelper::ToString(entity.ipcLevel);
		case 13: return StringHelper::ToString(entity.ipcServerID);
		case 14: return StringHelper::ToString(entity.ipcLastOnlineTime);
		case 15: return JsonHelper::BlockToJsonText(entity.ipcRankValue);
		case 16: return JsonHelper::BlockToJsonText(entity.ipcGsReadValue);
		case 17: return JsonHelper::BlockToJsonText(entity.ipcGsWriteValue);
	}
	return "";
}

template<> void SetTableFieldValue(InstPlayerOutlineInfo &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.ipcInstID, value);
		case 1: return StringHelper::FromString(entity.ipcAcctID, value);
		case 2: return StringHelper::FromString(entity.ipcNickName, value);
		case 3: return JsonHelper::BlockFromJsonText(entity.ipcFlags, value);
		case 4: return StringHelper::FromString(entity.ipcCareer, value);
		case 5: return StringHelper::FromString(entity.ipcGender, value);
		case 6: return StringHelper::FromString(entity.ipcMapType, value);
		case 7: return StringHelper::FromString(entity.ipcMapID, value);
		case 8: return StringHelper::FromString(entity.ipcPosX, value);
		case 9: return StringHelper::FromString(entity.ipcPosY, value);
		case 10: return StringHelper::FromString(entity.ipcPosZ, value);
		case 11: return StringHelper::FromString(entity.ipcPosO, value);
		case 12: return StringHelper::FromString(entity.ipcLevel, value);
		case 13: return StringHelper::FromString(entity.ipcServerID, value);
		case 14: return StringHelper::FromString(entity.ipcLastOnlineTime, value);
		case 15: return JsonHelper::BlockFromJsonText(entity.ipcRankValue, value);
		case 16: return JsonHelper::BlockFromJsonText(entity.ipcGsReadValue, value);
		case 17: return JsonHelper::BlockFromJsonText(entity.ipcGsWriteValue, value);
	}
}

template<> void LoadFromStream(InstPlayerOutlineInfo &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.ipcInstID, stream);
	StreamHelper::FromStream(entity.ipcAcctID, stream);
	StreamHelper::FromStream(entity.ipcNickName, stream);
	LoadFromStream(entity.ipcFlags, stream);
	StreamHelper::FromStream(entity.ipcCareer, stream);
	StreamHelper::FromStream(entity.ipcGender, stream);
	StreamHelper::FromStream(entity.ipcMapType, stream);
	StreamHelper::FromStream(entity.ipcMapID, stream);
	StreamHelper::FromStream(entity.ipcPosX, stream);
	StreamHelper::FromStream(entity.ipcPosY, stream);
	StreamHelper::FromStream(entity.ipcPosZ, stream);
	StreamHelper::FromStream(entity.ipcPosO, stream);
	StreamHelper::FromStream(entity.ipcLevel, stream);
	StreamHelper::FromStream(entity.ipcServerID, stream);
	StreamHelper::FromStream(entity.ipcLastOnlineTime, stream);
	LoadFromStream(entity.ipcRankValue, stream);
	LoadFromStream(entity.ipcGsReadValue, stream);
	LoadFromStream(entity.ipcGsWriteValue, stream);
}

template<> void SaveToStream(const InstPlayerOutlineInfo &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.ipcInstID, stream);
	StreamHelper::ToStream(entity.ipcAcctID, stream);
	StreamHelper::ToStream(entity.ipcNickName, stream);
	SaveToStream(entity.ipcFlags, stream);
	StreamHelper::ToStream(entity.ipcCareer, stream);
	StreamHelper::ToStream(entity.ipcGender, stream);
	StreamHelper::ToStream(entity.ipcMapType, stream);
	StreamHelper::ToStream(entity.ipcMapID, stream);
	StreamHelper::ToStream(entity.ipcPosX, stream);
	StreamHelper::ToStream(entity.ipcPosY, stream);
	StreamHelper::ToStream(entity.ipcPosZ, stream);
	StreamHelper::ToStream(entity.ipcPosO, stream);
	StreamHelper::ToStream(entity.ipcLevel, stream);
	StreamHelper::ToStream(entity.ipcServerID, stream);
	StreamHelper::ToStream(entity.ipcLastOnlineTime, stream);
	SaveToStream(entity.ipcRankValue, stream);
	SaveToStream(entity.ipcGsReadValue, stream);
	SaveToStream(entity.ipcGsWriteValue, stream);
}

template<> void LoadFromText(InstPlayerOutlineInfo &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const InstPlayerOutlineInfo &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(InstPlayerOutlineInfo &entity, const rapidjson::Value &value)
{
	FromJson(entity.ipcInstID, value, "ipcInstID");
	FromJson(entity.ipcAcctID, value, "ipcAcctID");
	FromJson(entity.ipcNickName, value, "ipcNickName");
	BlockFromJson(entity.ipcFlags, value, "ipcFlags");
	FromJson(entity.ipcCareer, value, "ipcCareer");
	FromJson(entity.ipcGender, value, "ipcGender");
	FromJson(entity.ipcMapType, value, "ipcMapType");
	FromJson(entity.ipcMapID, value, "ipcMapID");
	FromJson(entity.ipcPosX, value, "ipcPosX");
	FromJson(entity.ipcPosY, value, "ipcPosY");
	FromJson(entity.ipcPosZ, value, "ipcPosZ");
	FromJson(entity.ipcPosO, value, "ipcPosO");
	FromJson(entity.ipcLevel, value, "ipcLevel");
	FromJson(entity.ipcServerID, value, "ipcServerID");
	FromJson(entity.ipcLastOnlineTime, value, "ipcLastOnlineTime");
	BlockFromJson(entity.ipcRankValue, value, "ipcRankValue");
	BlockFromJson(entity.ipcGsReadValue, value, "ipcGsReadValue");
	BlockFromJson(entity.ipcGsWriteValue, value, "ipcGsWriteValue");
}

template<> void JsonHelper::BlockToJson(const InstPlayerOutlineInfo &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.ipcInstID, value, "ipcInstID");
	ToJson(entity.ipcAcctID, value, "ipcAcctID");
	ToJson(entity.ipcNickName, value, "ipcNickName");
	BlockToJson(entity.ipcFlags, value, "ipcFlags");
	ToJson(entity.ipcCareer, value, "ipcCareer");
	ToJson(entity.ipcGender, value, "ipcGender");
	ToJson(entity.ipcMapType, value, "ipcMapType");
	ToJson(entity.ipcMapID, value, "ipcMapID");
	ToJson(entity.ipcPosX, value, "ipcPosX");
	ToJson(entity.ipcPosY, value, "ipcPosY");
	ToJson(entity.ipcPosZ, value, "ipcPosZ");
	ToJson(entity.ipcPosO, value, "ipcPosO");
	ToJson(entity.ipcLevel, value, "ipcLevel");
	ToJson(entity.ipcServerID, value, "ipcServerID");
	ToJson(entity.ipcLastOnlineTime, value, "ipcLastOnlineTime");
	BlockToJson(entity.ipcRankValue, value, "ipcRankValue");
	BlockToJson(entity.ipcGsReadValue, value, "ipcGsReadValue");
	BlockToJson(entity.ipcGsWriteValue, value, "ipcGsWriteValue");
}

template<> const char *GetTableName<inst_player_char>()
{
	return "inst_player_char";
}

template<> const char *GetTableKeyName<inst_player_char>()
{
	return "ipcInstID";
}

template<> uint32 GetTableKeyValue(const inst_player_char &entity)
{
	return entity.ipcInstID;
}

template<> void SetTableKeyValue(inst_player_char &entity, uint32 key)
{
	entity.ipcInstID = key;
}

template<> const char *GetTableFieldNameByIndex<inst_player_char>(size_t index)
{
	switch (index)
	{
		case 0: return "ipcInstID";
		case 1: return "ipcAcctID";
		case 2: return "ipcNickName";
		case 3: return "ipcFlags";
		case 4: return "ipcAppearance";
		case 5: return "ipcCareer";
		case 6: return "ipcGender";
		case 7: return "ipcMapType";
		case 8: return "ipcMapID";
		case 9: return "ipcPosX";
		case 10: return "ipcPosY";
		case 11: return "ipcPosZ";
		case 12: return "ipcPosO";
		case 13: return "ipcLevel";
		case 14: return "ipcExp";
		case 15: return "ipcCurHP";
		case 16: return "ipcCurMP";
		case 17: return "ipcMoneyGold";
		case 18: return "ipcMoneyDiamond";
		case 19: return "ipcServerID";
		case 20: return "ipcCreateTime";
		case 21: return "ipcLastLoginTime";
		case 22: return "ipcLastOnlineTime";
		case 23: return "ipcDeleteTime";
		case 24: return "ipcQuestsDone";
		case 25: return "ipcQuests";
		case 26: return "ipcEffectItems";
		case 27: return "ipcOtherItems";
		case 28: return "ipcItemStorage";
		case 29: return "ipcShopStatus";
		case 30: return "ipcSpells";
		case 31: return "ipcBuffs";
		case 32: return "ipcCooldowns";
		case 33: return "ipcJsonValue";
		case 34: return "ipcF32Values";
		case 35: return "ipcS32Values";
		case 36: return "ipcS64Values";
		case 37: return "ipcPropertyValue";
		case 38: return "ipcRankValue";
		case 39: return "ipcGsReadValue";
		case 40: return "ipcGsWriteValue";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<inst_player_char>(const char *name)
{
	if (strcmp(name, "ipcInstID") == 0) return 0;
	if (strcmp(name, "ipcAcctID") == 0) return 1;
	if (strcmp(name, "ipcNickName") == 0) return 2;
	if (strcmp(name, "ipcFlags") == 0) return 3;
	if (strcmp(name, "ipcAppearance") == 0) return 4;
	if (strcmp(name, "ipcCareer") == 0) return 5;
	if (strcmp(name, "ipcGender") == 0) return 6;
	if (strcmp(name, "ipcMapType") == 0) return 7;
	if (strcmp(name, "ipcMapID") == 0) return 8;
	if (strcmp(name, "ipcPosX") == 0) return 9;
	if (strcmp(name, "ipcPosY") == 0) return 10;
	if (strcmp(name, "ipcPosZ") == 0) return 11;
	if (strcmp(name, "ipcPosO") == 0) return 12;
	if (strcmp(name, "ipcLevel") == 0) return 13;
	if (strcmp(name, "ipcExp") == 0) return 14;
	if (strcmp(name, "ipcCurHP") == 0) return 15;
	if (strcmp(name, "ipcCurMP") == 0) return 16;
	if (strcmp(name, "ipcMoneyGold") == 0) return 17;
	if (strcmp(name, "ipcMoneyDiamond") == 0) return 18;
	if (strcmp(name, "ipcServerID") == 0) return 19;
	if (strcmp(name, "ipcCreateTime") == 0) return 20;
	if (strcmp(name, "ipcLastLoginTime") == 0) return 21;
	if (strcmp(name, "ipcLastOnlineTime") == 0) return 22;
	if (strcmp(name, "ipcDeleteTime") == 0) return 23;
	if (strcmp(name, "ipcQuestsDone") == 0) return 24;
	if (strcmp(name, "ipcQuests") == 0) return 25;
	if (strcmp(name, "ipcEffectItems") == 0) return 26;
	if (strcmp(name, "ipcOtherItems") == 0) return 27;
	if (strcmp(name, "ipcItemStorage") == 0) return 28;
	if (strcmp(name, "ipcShopStatus") == 0) return 29;
	if (strcmp(name, "ipcSpells") == 0) return 30;
	if (strcmp(name, "ipcBuffs") == 0) return 31;
	if (strcmp(name, "ipcCooldowns") == 0) return 32;
	if (strcmp(name, "ipcJsonValue") == 0) return 33;
	if (strcmp(name, "ipcF32Values") == 0) return 34;
	if (strcmp(name, "ipcS32Values") == 0) return 35;
	if (strcmp(name, "ipcS64Values") == 0) return 36;
	if (strcmp(name, "ipcPropertyValue") == 0) return 37;
	if (strcmp(name, "ipcRankValue") == 0) return 38;
	if (strcmp(name, "ipcGsReadValue") == 0) return 39;
	if (strcmp(name, "ipcGsWriteValue") == 0) return 40;
	return -1;
}

template<> size_t GetTableFieldNumber<inst_player_char>()
{
	return 41;
}

template<> std::string GetTableFieldValue(const inst_player_char &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.ipcInstID);
		case 1: return StringHelper::ToString(entity.ipcAcctID);
		case 2: return StringHelper::ToString(entity.ipcNickName);
		case 3: return JsonHelper::BlockToJsonText(entity.ipcFlags);
		case 4: return JsonHelper::BlockToJsonText(entity.ipcAppearance);
		case 5: return StringHelper::ToString(entity.ipcCareer);
		case 6: return StringHelper::ToString(entity.ipcGender);
		case 7: return StringHelper::ToString(entity.ipcMapType);
		case 8: return StringHelper::ToString(entity.ipcMapID);
		case 9: return StringHelper::ToString(entity.ipcPosX);
		case 10: return StringHelper::ToString(entity.ipcPosY);
		case 11: return StringHelper::ToString(entity.ipcPosZ);
		case 12: return StringHelper::ToString(entity.ipcPosO);
		case 13: return StringHelper::ToString(entity.ipcLevel);
		case 14: return StringHelper::ToString(entity.ipcExp);
		case 15: return StringHelper::ToString(entity.ipcCurHP);
		case 16: return StringHelper::ToString(entity.ipcCurMP);
		case 17: return StringHelper::ToString(entity.ipcMoneyGold);
		case 18: return StringHelper::ToString(entity.ipcMoneyDiamond);
		case 19: return StringHelper::ToString(entity.ipcServerID);
		case 20: return StringHelper::ToString(entity.ipcCreateTime);
		case 21: return StringHelper::ToString(entity.ipcLastLoginTime);
		case 22: return StringHelper::ToString(entity.ipcLastOnlineTime);
		case 23: return StringHelper::ToString(entity.ipcDeleteTime);
		case 24: return StringHelper::ToString(entity.ipcQuestsDone);
		case 25: return StringHelper::ToString(entity.ipcQuests);
		case 26: return StringHelper::ToString(entity.ipcEffectItems);
		case 27: return StringHelper::ToString(entity.ipcOtherItems);
		case 28: return StringHelper::ToString(entity.ipcItemStorage);
		case 29: return StringHelper::ToString(entity.ipcShopStatus);
		case 30: return StringHelper::ToString(entity.ipcSpells);
		case 31: return StringHelper::ToString(entity.ipcBuffs);
		case 32: return StringHelper::ToString(entity.ipcCooldowns);
		case 33: return StringHelper::ToString(entity.ipcJsonValue);
		case 34: return JsonHelper::SequenceToJsonText(entity.ipcF32Values);
		case 35: return JsonHelper::SequenceToJsonText(entity.ipcS32Values);
		case 36: return JsonHelper::SequenceToJsonText(entity.ipcS64Values);
		case 37: return JsonHelper::BlockToJsonText(entity.ipcPropertyValue);
		case 38: return JsonHelper::BlockToJsonText(entity.ipcRankValue);
		case 39: return JsonHelper::BlockToJsonText(entity.ipcGsReadValue);
		case 40: return JsonHelper::BlockToJsonText(entity.ipcGsWriteValue);
	}
	return "";
}

template<> void SetTableFieldValue(inst_player_char &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.ipcInstID, value);
		case 1: return StringHelper::FromString(entity.ipcAcctID, value);
		case 2: return StringHelper::FromString(entity.ipcNickName, value);
		case 3: return JsonHelper::BlockFromJsonText(entity.ipcFlags, value);
		case 4: return JsonHelper::BlockFromJsonText(entity.ipcAppearance, value);
		case 5: return StringHelper::FromString(entity.ipcCareer, value);
		case 6: return StringHelper::FromString(entity.ipcGender, value);
		case 7: return StringHelper::FromString(entity.ipcMapType, value);
		case 8: return StringHelper::FromString(entity.ipcMapID, value);
		case 9: return StringHelper::FromString(entity.ipcPosX, value);
		case 10: return StringHelper::FromString(entity.ipcPosY, value);
		case 11: return StringHelper::FromString(entity.ipcPosZ, value);
		case 12: return StringHelper::FromString(entity.ipcPosO, value);
		case 13: return StringHelper::FromString(entity.ipcLevel, value);
		case 14: return StringHelper::FromString(entity.ipcExp, value);
		case 15: return StringHelper::FromString(entity.ipcCurHP, value);
		case 16: return StringHelper::FromString(entity.ipcCurMP, value);
		case 17: return StringHelper::FromString(entity.ipcMoneyGold, value);
		case 18: return StringHelper::FromString(entity.ipcMoneyDiamond, value);
		case 19: return StringHelper::FromString(entity.ipcServerID, value);
		case 20: return StringHelper::FromString(entity.ipcCreateTime, value);
		case 21: return StringHelper::FromString(entity.ipcLastLoginTime, value);
		case 22: return StringHelper::FromString(entity.ipcLastOnlineTime, value);
		case 23: return StringHelper::FromString(entity.ipcDeleteTime, value);
		case 24: return StringHelper::FromString(entity.ipcQuestsDone, value);
		case 25: return StringHelper::FromString(entity.ipcQuests, value);
		case 26: return StringHelper::FromString(entity.ipcEffectItems, value);
		case 27: return StringHelper::FromString(entity.ipcOtherItems, value);
		case 28: return StringHelper::FromString(entity.ipcItemStorage, value);
		case 29: return StringHelper::FromString(entity.ipcShopStatus, value);
		case 30: return StringHelper::FromString(entity.ipcSpells, value);
		case 31: return StringHelper::FromString(entity.ipcBuffs, value);
		case 32: return StringHelper::FromString(entity.ipcCooldowns, value);
		case 33: return StringHelper::FromString(entity.ipcJsonValue, value);
		case 34: return JsonHelper::SequenceFromJsonText(entity.ipcF32Values, value);
		case 35: return JsonHelper::SequenceFromJsonText(entity.ipcS32Values, value);
		case 36: return JsonHelper::SequenceFromJsonText(entity.ipcS64Values, value);
		case 37: return JsonHelper::BlockFromJsonText(entity.ipcPropertyValue, value);
		case 38: return JsonHelper::BlockFromJsonText(entity.ipcRankValue, value);
		case 39: return JsonHelper::BlockFromJsonText(entity.ipcGsReadValue, value);
		case 40: return JsonHelper::BlockFromJsonText(entity.ipcGsWriteValue, value);
	}
}

template<> void LoadFromStream(inst_player_char &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.ipcInstID, stream);
	StreamHelper::FromStream(entity.ipcAcctID, stream);
	StreamHelper::FromStream(entity.ipcNickName, stream);
	LoadFromStream(entity.ipcFlags, stream);
	LoadFromStream(entity.ipcAppearance, stream);
	StreamHelper::FromStream(entity.ipcCareer, stream);
	StreamHelper::FromStream(entity.ipcGender, stream);
	StreamHelper::FromStream(entity.ipcMapType, stream);
	StreamHelper::FromStream(entity.ipcMapID, stream);
	StreamHelper::FromStream(entity.ipcPosX, stream);
	StreamHelper::FromStream(entity.ipcPosY, stream);
	StreamHelper::FromStream(entity.ipcPosZ, stream);
	StreamHelper::FromStream(entity.ipcPosO, stream);
	StreamHelper::FromStream(entity.ipcLevel, stream);
	StreamHelper::FromStream(entity.ipcExp, stream);
	StreamHelper::FromStream(entity.ipcCurHP, stream);
	StreamHelper::FromStream(entity.ipcCurMP, stream);
	StreamHelper::FromStream(entity.ipcMoneyGold, stream);
	StreamHelper::FromStream(entity.ipcMoneyDiamond, stream);
	StreamHelper::FromStream(entity.ipcServerID, stream);
	StreamHelper::FromStream(entity.ipcCreateTime, stream);
	StreamHelper::FromStream(entity.ipcLastLoginTime, stream);
	StreamHelper::FromStream(entity.ipcLastOnlineTime, stream);
	StreamHelper::FromStream(entity.ipcDeleteTime, stream);
	StreamHelper::FromStream(entity.ipcQuestsDone, stream);
	StreamHelper::FromStream(entity.ipcQuests, stream);
	StreamHelper::FromStream(entity.ipcEffectItems, stream);
	StreamHelper::FromStream(entity.ipcOtherItems, stream);
	StreamHelper::FromStream(entity.ipcItemStorage, stream);
	StreamHelper::FromStream(entity.ipcShopStatus, stream);
	StreamHelper::FromStream(entity.ipcSpells, stream);
	StreamHelper::FromStream(entity.ipcBuffs, stream);
	StreamHelper::FromStream(entity.ipcCooldowns, stream);
	StreamHelper::FromStream(entity.ipcJsonValue, stream);
	StreamHelper::SequenceFromStream(entity.ipcF32Values, stream);
	StreamHelper::SequenceFromStream(entity.ipcS32Values, stream);
	StreamHelper::SequenceFromStream(entity.ipcS64Values, stream);
	LoadFromStream(entity.ipcPropertyValue, stream);
	LoadFromStream(entity.ipcRankValue, stream);
	LoadFromStream(entity.ipcGsReadValue, stream);
	LoadFromStream(entity.ipcGsWriteValue, stream);
}

template<> void SaveToStream(const inst_player_char &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.ipcInstID, stream);
	StreamHelper::ToStream(entity.ipcAcctID, stream);
	StreamHelper::ToStream(entity.ipcNickName, stream);
	SaveToStream(entity.ipcFlags, stream);
	SaveToStream(entity.ipcAppearance, stream);
	StreamHelper::ToStream(entity.ipcCareer, stream);
	StreamHelper::ToStream(entity.ipcGender, stream);
	StreamHelper::ToStream(entity.ipcMapType, stream);
	StreamHelper::ToStream(entity.ipcMapID, stream);
	StreamHelper::ToStream(entity.ipcPosX, stream);
	StreamHelper::ToStream(entity.ipcPosY, stream);
	StreamHelper::ToStream(entity.ipcPosZ, stream);
	StreamHelper::ToStream(entity.ipcPosO, stream);
	StreamHelper::ToStream(entity.ipcLevel, stream);
	StreamHelper::ToStream(entity.ipcExp, stream);
	StreamHelper::ToStream(entity.ipcCurHP, stream);
	StreamHelper::ToStream(entity.ipcCurMP, stream);
	StreamHelper::ToStream(entity.ipcMoneyGold, stream);
	StreamHelper::ToStream(entity.ipcMoneyDiamond, stream);
	StreamHelper::ToStream(entity.ipcServerID, stream);
	StreamHelper::ToStream(entity.ipcCreateTime, stream);
	StreamHelper::ToStream(entity.ipcLastLoginTime, stream);
	StreamHelper::ToStream(entity.ipcLastOnlineTime, stream);
	StreamHelper::ToStream(entity.ipcDeleteTime, stream);
	StreamHelper::ToStream(entity.ipcQuestsDone, stream);
	StreamHelper::ToStream(entity.ipcQuests, stream);
	StreamHelper::ToStream(entity.ipcEffectItems, stream);
	StreamHelper::ToStream(entity.ipcOtherItems, stream);
	StreamHelper::ToStream(entity.ipcItemStorage, stream);
	StreamHelper::ToStream(entity.ipcShopStatus, stream);
	StreamHelper::ToStream(entity.ipcSpells, stream);
	StreamHelper::ToStream(entity.ipcBuffs, stream);
	StreamHelper::ToStream(entity.ipcCooldowns, stream);
	StreamHelper::ToStream(entity.ipcJsonValue, stream);
	StreamHelper::SequenceToStream(entity.ipcF32Values, stream);
	StreamHelper::SequenceToStream(entity.ipcS32Values, stream);
	StreamHelper::SequenceToStream(entity.ipcS64Values, stream);
	SaveToStream(entity.ipcPropertyValue, stream);
	SaveToStream(entity.ipcRankValue, stream);
	SaveToStream(entity.ipcGsReadValue, stream);
	SaveToStream(entity.ipcGsWriteValue, stream);
}

template<> void LoadFromText(inst_player_char &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const inst_player_char &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(inst_player_char &entity, const rapidjson::Value &value)
{
	FromJson(entity.ipcInstID, value, "ipcInstID");
	FromJson(entity.ipcAcctID, value, "ipcAcctID");
	FromJson(entity.ipcNickName, value, "ipcNickName");
	BlockFromJson(entity.ipcFlags, value, "ipcFlags");
	BlockFromJson(entity.ipcAppearance, value, "ipcAppearance");
	FromJson(entity.ipcCareer, value, "ipcCareer");
	FromJson(entity.ipcGender, value, "ipcGender");
	FromJson(entity.ipcMapType, value, "ipcMapType");
	FromJson(entity.ipcMapID, value, "ipcMapID");
	FromJson(entity.ipcPosX, value, "ipcPosX");
	FromJson(entity.ipcPosY, value, "ipcPosY");
	FromJson(entity.ipcPosZ, value, "ipcPosZ");
	FromJson(entity.ipcPosO, value, "ipcPosO");
	FromJson(entity.ipcLevel, value, "ipcLevel");
	FromJson(entity.ipcExp, value, "ipcExp");
	FromJson(entity.ipcCurHP, value, "ipcCurHP");
	FromJson(entity.ipcCurMP, value, "ipcCurMP");
	FromJson(entity.ipcMoneyGold, value, "ipcMoneyGold");
	FromJson(entity.ipcMoneyDiamond, value, "ipcMoneyDiamond");
	FromJson(entity.ipcServerID, value, "ipcServerID");
	FromJson(entity.ipcCreateTime, value, "ipcCreateTime");
	FromJson(entity.ipcLastLoginTime, value, "ipcLastLoginTime");
	FromJson(entity.ipcLastOnlineTime, value, "ipcLastOnlineTime");
	FromJson(entity.ipcDeleteTime, value, "ipcDeleteTime");
	FromJson(entity.ipcQuestsDone, value, "ipcQuestsDone");
	FromJson(entity.ipcQuests, value, "ipcQuests");
	FromJson(entity.ipcEffectItems, value, "ipcEffectItems");
	FromJson(entity.ipcOtherItems, value, "ipcOtherItems");
	FromJson(entity.ipcItemStorage, value, "ipcItemStorage");
	FromJson(entity.ipcShopStatus, value, "ipcShopStatus");
	FromJson(entity.ipcSpells, value, "ipcSpells");
	FromJson(entity.ipcBuffs, value, "ipcBuffs");
	FromJson(entity.ipcCooldowns, value, "ipcCooldowns");
	FromJson(entity.ipcJsonValue, value, "ipcJsonValue");
	SequenceFromJson(entity.ipcF32Values, value, "ipcF32Values");
	SequenceFromJson(entity.ipcS32Values, value, "ipcS32Values");
	SequenceFromJson(entity.ipcS64Values, value, "ipcS64Values");
	BlockFromJson(entity.ipcPropertyValue, value, "ipcPropertyValue");
	BlockFromJson(entity.ipcRankValue, value, "ipcRankValue");
	BlockFromJson(entity.ipcGsReadValue, value, "ipcGsReadValue");
	BlockFromJson(entity.ipcGsWriteValue, value, "ipcGsWriteValue");
}

template<> void JsonHelper::BlockToJson(const inst_player_char &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.ipcInstID, value, "ipcInstID");
	ToJson(entity.ipcAcctID, value, "ipcAcctID");
	ToJson(entity.ipcNickName, value, "ipcNickName");
	BlockToJson(entity.ipcFlags, value, "ipcFlags");
	BlockToJson(entity.ipcAppearance, value, "ipcAppearance");
	ToJson(entity.ipcCareer, value, "ipcCareer");
	ToJson(entity.ipcGender, value, "ipcGender");
	ToJson(entity.ipcMapType, value, "ipcMapType");
	ToJson(entity.ipcMapID, value, "ipcMapID");
	ToJson(entity.ipcPosX, value, "ipcPosX");
	ToJson(entity.ipcPosY, value, "ipcPosY");
	ToJson(entity.ipcPosZ, value, "ipcPosZ");
	ToJson(entity.ipcPosO, value, "ipcPosO");
	ToJson(entity.ipcLevel, value, "ipcLevel");
	ToJson(entity.ipcExp, value, "ipcExp");
	ToJson(entity.ipcCurHP, value, "ipcCurHP");
	ToJson(entity.ipcCurMP, value, "ipcCurMP");
	ToJson(entity.ipcMoneyGold, value, "ipcMoneyGold");
	ToJson(entity.ipcMoneyDiamond, value, "ipcMoneyDiamond");
	ToJson(entity.ipcServerID, value, "ipcServerID");
	ToJson(entity.ipcCreateTime, value, "ipcCreateTime");
	ToJson(entity.ipcLastLoginTime, value, "ipcLastLoginTime");
	ToJson(entity.ipcLastOnlineTime, value, "ipcLastOnlineTime");
	ToJson(entity.ipcDeleteTime, value, "ipcDeleteTime");
	ToJson(entity.ipcQuestsDone, value, "ipcQuestsDone");
	ToJson(entity.ipcQuests, value, "ipcQuests");
	ToJson(entity.ipcEffectItems, value, "ipcEffectItems");
	ToJson(entity.ipcOtherItems, value, "ipcOtherItems");
	ToJson(entity.ipcItemStorage, value, "ipcItemStorage");
	ToJson(entity.ipcShopStatus, value, "ipcShopStatus");
	ToJson(entity.ipcSpells, value, "ipcSpells");
	ToJson(entity.ipcBuffs, value, "ipcBuffs");
	ToJson(entity.ipcCooldowns, value, "ipcCooldowns");
	ToJson(entity.ipcJsonValue, value, "ipcJsonValue");
	SequenceToJson(entity.ipcF32Values, value, "ipcF32Values");
	SequenceToJson(entity.ipcS32Values, value, "ipcS32Values");
	SequenceToJson(entity.ipcS64Values, value, "ipcS64Values");
	BlockToJson(entity.ipcPropertyValue, value, "ipcPropertyValue");
	BlockToJson(entity.ipcRankValue, value, "ipcRankValue");
	BlockToJson(entity.ipcGsReadValue, value, "ipcGsReadValue");
	BlockToJson(entity.ipcGsWriteValue, value, "ipcGsWriteValue");
}
