#include "jsontable/table_helper.h"
#include "inst_mail.h"

InstMailAttachment::InstMailAttachment()
: mailID(0)
{
}

inst_mail::inst_mail()
: mailID(0)
, mailType(0)
, mailFlags(0)
, mailSender(0)
, mailReceiver(0)
, mailDeliverTime(0)
, mailExpireTime(0)
, isGetAttachment(false)
, isViewDetail(false)
{
}
