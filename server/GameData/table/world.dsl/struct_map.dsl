table MapInfo
{
	enum Type {
		VoidSpace,
		WorldMap,
		Dungeon,
		Story,
		Count
	};
	struct Flags {
		bool isParallelMap;
		bool isRecoveryHPDisable;
	};
	required uint32 Id;
	required uint32 type;
	required Flags flags;
	required string strName;
	required string strSceneFile;
	required float viewing_distance;
	required uint32 pop_map_id;
	required float pop_pos_x;
	required float pop_pos_y;
	required float pop_pos_z;
	required float pop_o;
	required float x1, y1, z1;
	required float x2, y2, z2;
	(key=Id, tblname=map_info)
}

table MapZone
{
	required uint32 Id;
	required uint32 parentId;
	required uint32 priority;
	required uint32 map_id;
	required uint32 map_type;
	required float x1, y1, z1;
	required float x2, y2, z2;
	required uint32 pvp_flags;
	(key=Id, tblname=map_zone)
}

table MapGraveyard
{
	required uint32 Id;
	required uint32 map_id;
	required uint32 map_type;
	required float x, y, z, o;
	(key=Id, tblname=map_graveyard)
}

table TeleportPoint
{
	required uint32 Id;
	required string name;
	required uint32 map_id;
	required uint32 map_type;
	required float x, y, z, o;
	required uint32 trait;
	(key=Id, tblname=teleport_point)
}

table WayPoint
{
	required uint32 Id;
	required uint32 first_wp_id;
	required uint32 prev_wp_id;
	required uint32 next_wp_id;
	required uint32 map_id;
	required float x, y, z;
	required int32 keep_idle;
	required uint32 emote_state;
	(key=Id, tblname=way_point)
}

table LandmarkPoint
{
	required uint32 Id;
	required string name;
	required uint32 map_id;
	required uint32 map_type;
	required float x, y, z;
	(key=Id, tblname=landmark_point)
}