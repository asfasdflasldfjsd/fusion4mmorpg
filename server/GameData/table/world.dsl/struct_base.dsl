enum PlayerCareer
{
	None,
	Terran,
	Protoss,
	Demons,
	Max
}

enum PlayerGender
{
	None,
	Male,
	Female,
	Max
}

enum CurrencyType
{
	None,
	Gold,				// 金币
	Diamond,			// 钻石
	Max
}

enum ChequeType
{
	None,
	Gold,				// 金币
	Diamond,			// 钻石
	Exp,				// 经验
	Max
}

table Configure
{
	required uint32 cfgIndex;
	required string cfgName;
	required string cfgVal;
	(tblname=configure)
}