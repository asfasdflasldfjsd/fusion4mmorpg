enum CharRaceType
{
	Player,
	Neutral,
	HostileForces,
	FriendlyForces,
}

enum CharEliteType
{
	Normal,
	Boss,
}

table CharPrototype
{
	struct Flags {
		bool isUndead;
		bool isJoinCombat;
		bool isFastTurn;
		bool isKeepDir;
		bool isActivity;
		bool isIgnoreInvisible;
		bool isSenseCreature;
		bool isSensePlayer;
	};

	required uint32 charTypeId;
	required Flags charFlags;
	required uint32 charRace;
	required uint32 charElite;
	required uint32 level;

	required float speedWalk;
	required float speedRun;
	required float speedTurn;

	required float patrolRange;
	required float boundRadius;
	required float senseRadius;
	required float combatRadius;

	required float combatBestDist;

	required uint32 minRespawnTime;
	required uint32 maxRespawnTime;
	required uint32 deadDisappearTime;

	required double recoveryHPRate;
	required double recoveryHPValue;
	required double recoveryMPRate;
	required double recoveryMPValue;

	required uint32 lootSetID;

	required uint32 aiScriptId;
	required uint32 spawnScriptId;
	required string spawnScriptArgs;
	required uint32 deadScriptId;
	required string deadScriptArgs;
	required uint32 playScriptId;
	required string playScriptArgs;

	(key=charTypeId, tblname=char_prototype)
}

table CreatureSpawn
{
	struct Flags {
		bool isRespawn;
	}
	required uint32 spawnId;
	required Flags flags;
	required uint32 entry;
	required uint32 level;
	required uint32 map_id;
	required uint32 map_type;
	required float x, y, z, o;
	required uint32 idleType;
	required uint32 wayPointId;
	(key=spawnId, tblname=creature_spawn)
}