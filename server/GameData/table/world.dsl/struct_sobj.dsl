table SObjPrototype
{
	struct Flags {
		bool isExclusiveMine;
		bool isRemoveAfterMineDone;
		bool isPlayerInteresting;
		bool isCreatureInteresting;
		bool isActivity;
	};
	struct CostItem {
		uint32 itemId;
		uint32 itemNum;
	};

	required uint32 sobjTypeId;
	required Flags sobjFlags;

	required uint32 minRespawnTime;
	required uint32 maxRespawnTime;

	required uint32 teleportPointID;
	required uint32 teleportDelayTime;

	required uint32 lootSetID;
	required uint32 mineSpellID;
	required uint32 mineSpellLv;
	required uint32 mineAnimTime;
	required CostItem[] mineCostItems;

	required uint32 sobjReqQuestDoing;

	required float radius;

	required uint32 spawnScriptId;
	required string spawnScriptArgs;
	required uint32 playScriptId;
	required string playScriptArgs;

	(key=sobjTypeId, tblname=sobj_prototype)
}

table StaticObjectSpawn
{
	struct Flags {
		bool isRespawn;
	}
	required uint32 spawnId;
	required Flags flags;
	required uint32 entry;
	required uint32 map_id;
	required uint32 map_type;
	required float x, y, z, o;
	required float radius;
	(key=spawnId, tblname=staticobject_spawn)
}