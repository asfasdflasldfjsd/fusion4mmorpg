enum ItemClass
{
	None,
	Equip,
	Quest,
	Material,
	Gemstone,
	Consumable,
	Scrap,
	Other,
}

enum ItemSubClass
{
	Equip_Weapon = 1,
	Equip_Belt,
	Equip_Gloves,
	Equip_Shoes,
}

enum ItemQuality
{
	White,
	Red,
	Count
}

table ItemPrototype
{
	struct Flags {
		bool canUse;
		bool canSell;
		bool canDestroy;
		bool isDestroyAfterUse;
	};

	required uint32 itemTypeID;
	required Flags itemFlags;
	required uint32 itemClass;
	required uint32 itemSubClass;
	required uint32 itemQuality;
	required uint32 itemLevel;
	required uint32 itemStack;

	required uint32 itemSellPrice;

	required uint32 itemLootId;
	required uint32 itemSpellId;
	required uint32 itemSpellLevel;
	required uint32 itemScriptId;
	required string itemScriptArgs;

	(key=itemTypeID, tblname=item_prototype)
}

table ItemEquipPrototype
{
	required uint32 itemTypeID;

	struct Attr {
		uint32 type;
		double value;
	};
	required Attr[] itemEquipAttrs;

	struct Spell {
		uint32 id;
		uint32 level;
	};
	required Spell[] itemEquipSpells;

	(key=itemTypeID, tblname=item_equip_prototype)
}