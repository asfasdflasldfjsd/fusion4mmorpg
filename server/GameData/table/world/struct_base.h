#pragma once

enum class PlayerCareer
{
	None,
	Terran,
	Protoss,
	Demons,
	Max,
};

enum class PlayerGender
{
	None,
	Male,
	Female,
	Max,
};

enum class CurrencyType
{
	None,
	Gold,  // 金币
	Diamond,  // 钻石
	Max,
};

enum class ChequeType
{
	None,
	Gold,  // 金币
	Diamond,  // 钻石
	Exp,  // 经验
	Max,
};

struct Configure
{
	Configure();

	uint32 cfgIndex;
	std::string cfgName;
	std::string cfgVal;
};
