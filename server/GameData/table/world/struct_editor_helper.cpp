#include "jsontable/table_helper.h"
#include "struct_editor.h"

template<> void LoadFromStream(EditorTreeView::Type &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const EditorTreeView::Type &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(EditorTreeView::Type &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const EditorTreeView::Type &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(EditorTreeView::Type &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const EditorTreeView::Type &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> const char *GetTableName<EditorTreeView>()
{
	return "editor_tree_view";
}

template<> const char *GetTableKeyName<EditorTreeView>()
{
	return "etvType";
}

template<> uint32 GetTableKeyValue(const EditorTreeView &entity)
{
	return entity.etvType;
}

template<> void SetTableKeyValue(EditorTreeView &entity, uint32 key)
{
	entity.etvType = key;
}

template<> const char *GetTableFieldNameByIndex<EditorTreeView>(size_t index)
{
	switch (index)
	{
		case 0: return "etvType";
		case 1: return "etvTree";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<EditorTreeView>(const char *name)
{
	if (strcmp(name, "etvType") == 0) return 0;
	if (strcmp(name, "etvTree") == 0) return 1;
	return -1;
}

template<> size_t GetTableFieldNumber<EditorTreeView>()
{
	return 2;
}

template<> std::string GetTableFieldValue(const EditorTreeView &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.etvType);
		case 1: return StringHelper::ToString(entity.etvTree);
	}
	return "";
}

template<> void SetTableFieldValue(EditorTreeView &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.etvType, value);
		case 1: return StringHelper::FromString(entity.etvTree, value);
	}
}

template<> void LoadFromStream(EditorTreeView &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.etvType, stream);
	StreamHelper::FromStream(entity.etvTree, stream);
}

template<> void SaveToStream(const EditorTreeView &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.etvType, stream);
	StreamHelper::ToStream(entity.etvTree, stream);
}

template<> void LoadFromText(EditorTreeView &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const EditorTreeView &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(EditorTreeView &entity, const rapidjson::Value &value)
{
	FromJson(entity.etvType, value, "etvType");
	FromJson(entity.etvTree, value, "etvTree");
}

template<> void JsonHelper::BlockToJson(const EditorTreeView &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.etvType, value, "etvType");
	ToJson(entity.etvTree, value, "etvTree");
}
