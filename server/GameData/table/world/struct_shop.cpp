#include "jsontable/table_helper.h"
#include "struct_shop.h"

ShopPrototype::Flags::Flags()
: isBinding(false)
{
}

ShopPrototype::ShopPrototype()
: Id(0)
, shopType(0)
, itemTypeID(0)
, itemCount(0)
, currencyType(0)
, sellPrice(0)
, discountPrice(0)
, bundleNumMax(0)
, genderLimit(0)
, careerLimit(0)
, dailyBuyLimit(0)
, weeklyBuyLimit(0)
{
}

SpecialShopPrototype::SpecialShopPrototype()
: itemUniqueKey(0)
, itemBeginTime(0)
, itemEndTime(0)
, serverBuyLimit(0)
, charBuyLimit(0)
, dailyServerBuyLimit(0)
, weeklyServerBuyLimit(0)
{
}
