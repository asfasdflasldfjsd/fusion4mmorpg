#pragma once

enum class ATTRARITHTYPE
{
	BASE,
	SCALE,
	FINAL,
	COUNT,
};

enum class ATTRTYPE
{
	NONE,

	HIT_POINT,
	MAGIC_POINT,

	ATTACK_VALUE,
	DEFENSE_VALUE,

	HIT_CHANCE,
	DODGE_CHANCE,
	CRITIHIT_CHANCE,
	CRITIHIT_CHANCE_RESIST,
	CRITIHIT_INTENSITY,
	CRITIHIT_INTENSITY_RESIST,

	COUNT,
};

struct PlayerBase
{
	PlayerBase();

	uint32 level;
	uint64 lvUpExp;
	double damageFactor;
	double recoveryHPRate;
	double recoveryHPValue;
	double recoveryMPRate;
	double recoveryMPValue;
};

struct PlayerAttribute
{
	PlayerAttribute();

	uint32 Id;
	uint32 career;
	uint32 level;
	std::vector<double> attrs;
};

struct CreatureAttribute
{
	CreatureAttribute();

	uint32 Id;
	uint32 elite;
	uint32 level;
	std::vector<double> attrs;
	double damageFactor;
};

struct CreatureCustomAttribute
{
	CreatureCustomAttribute();

	uint32 charTypeId;
	std::vector<double> attrs;
	double damageFactor;
};
