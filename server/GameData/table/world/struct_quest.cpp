#include "jsontable/table_helper.h"
#include "struct_quest.h"

QuestObjInst::QuestObjInst()
: objType(0)
, objID(0)
{
}

QuestScript::QuestScript()
: scriptID(0)
{
}

QuestCheque::QuestCheque()
: chequeType(0)
, chequeValue(0)
{
}

QuestItem::QuestItem()
: itemTypeID(0)
, itemCount(0)
, onlyCareer(0)
, onlyGender(0)
{
}

QuestChequeReq::QuestChequeReq()
: isCost(false)
, isRefund(false)
{
}

QuestItemReq::QuestItemReq()
: isDestroy(false)
, isRefund(false)
, isBinding(false)
{
}

QuestChequeInit::QuestChequeInit()
: isRetrieve(false)
{
}

QuestItemInit::QuestItemInit()
: isBinding(false)
, isRetrieve(false)
{
}

QuestChequeReward::QuestChequeReward()
: isFixed(false)
{
}

QuestItemReward::QuestItemReward()
: isBinding(false)
{
}

QuestCondition::QuestCondition()
: conditionType(0)
, conditionNum(0)
{
}

QuestPrototype::Flags::Flags()
: isWatchStatus(false)
, isAutoAccept(false)
, isAutoSubmit(false)
, isStoryMode(false)
, isEnterSceneAeap(false)
, isLeaveSceneAeap(false)
, isCantArchive(false)
, isCantCancel(false)
, isAnyReqQuest(false)
, isAnyCondition(false)
, isRemoveFailed(false)
{
}

QuestPrototype::QuestPrototype()
: questTypeID(0)
, questClass(0)
, questTimeMax(0)
, questRepeatType(0)
, questRepeatMax(0)
, questReqMinLv(0)
, questReqMaxLv(0)
, questReqCareer(0)
, questReqGender(0)
{
}

QuestCreatureVisible::QuestCreatureVisible()
: questTypeID(0)
, questWhenType(0)
, isVisible(false)
{
}
