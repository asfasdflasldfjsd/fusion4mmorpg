#pragma once

struct SObjPrototype
{
	SObjPrototype();

	struct Flags {
		Flags();
		bool isExclusiveMine;
		bool isRemoveAfterMineDone;
		bool isPlayerInteresting;
		bool isCreatureInteresting;
		bool isActivity;
	};
	struct CostItem {
		CostItem();
		uint32 itemId;
		uint32 itemNum;
	};

	uint32 sobjTypeId;
	Flags sobjFlags;

	uint32 minRespawnTime;
	uint32 maxRespawnTime;

	uint32 teleportPointID;
	uint32 teleportDelayTime;

	uint32 lootSetID;
	uint32 mineSpellID;
	uint32 mineSpellLv;
	uint32 mineAnimTime;
	std::vector<CostItem> mineCostItems;

	uint32 sobjReqQuestDoing;

	float radius;

	uint32 spawnScriptId;
	std::string spawnScriptArgs;
	uint32 playScriptId;
	std::string playScriptArgs;
};


struct StaticObjectSpawn
{
	StaticObjectSpawn();

	struct Flags {
		Flags();
		bool isRespawn;
	};
	uint32 spawnId;
	Flags flags;
	uint32 entry;
	uint32 map_id;
	uint32 map_type;
	float x, y, z, o;
	float radius;
};
