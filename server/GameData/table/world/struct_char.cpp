#include "jsontable/table_helper.h"
#include "struct_char.h"

CharPrototype::Flags::Flags()
: isUndead(false)
, isJoinCombat(false)
, isFastTurn(false)
, isKeepDir(false)
, isActivity(false)
, isIgnoreInvisible(false)
, isSenseCreature(false)
, isSensePlayer(false)
{
}

CharPrototype::CharPrototype()
: charTypeId(0)
, charRace(0)
, charElite(0)
, level(0)
, speedWalk(.0f)
, speedRun(.0f)
, speedTurn(.0f)
, patrolRange(.0f)
, boundRadius(.0f)
, senseRadius(.0f)
, combatRadius(.0f)
, combatBestDist(.0f)
, minRespawnTime(0)
, maxRespawnTime(0)
, deadDisappearTime(0)
, recoveryHPRate(.0)
, recoveryHPValue(.0)
, recoveryMPRate(.0)
, recoveryMPValue(.0)
, lootSetID(0)
, aiScriptId(0)
, spawnScriptId(0)
, deadScriptId(0)
, playScriptId(0)
{
}

CreatureSpawn::Flags::Flags()
: isRespawn(false)
{
}

CreatureSpawn::CreatureSpawn()
: spawnId(0)
, entry(0)
, level(0)
, map_id(0)
, map_type(0)
, x(.0f)
, y(.0f)
, z(.0f)
, o(.0f)
, idleType(0)
, wayPointId(0)
{
}
