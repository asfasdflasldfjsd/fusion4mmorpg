#pragma once

enum class ItemClass
{
	None,
	Equip,
	Quest,
	Material,
	Gemstone,
	Consumable,
	Scrap,
	Other,
};

enum class ItemSubClass
{
	Equip_Weapon = 1,
	Equip_Belt,
	Equip_Gloves,
	Equip_Shoes,
};

enum class ItemQuality
{
	White,
	Red,
	Count,
};

struct ItemPrototype
{
	ItemPrototype();

	struct Flags {
		Flags();
		bool canUse;
		bool canSell;
		bool canDestroy;
		bool isDestroyAfterUse;
	};

	uint32 itemTypeID;
	Flags itemFlags;
	uint32 itemClass;
	uint32 itemSubClass;
	uint32 itemQuality;
	uint32 itemLevel;
	uint32 itemStack;

	uint32 itemSellPrice;

	uint32 itemLootId;
	uint32 itemSpellId;
	uint32 itemSpellLevel;
	uint32 itemScriptId;
	std::string itemScriptArgs;
};


struct ItemEquipPrototype
{
	ItemEquipPrototype();

	uint32 itemTypeID;

	struct Attr {
		Attr();
		uint32 type;
		double value;
	};
	std::vector<Attr> itemEquipAttrs;

	struct Spell {
		Spell();
		uint32 id;
		uint32 level;
	};
	std::vector<Spell> itemEquipSpells;
};
