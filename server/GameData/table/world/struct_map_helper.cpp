#include "jsontable/table_helper.h"
#include "struct_map.h"

template<> void LoadFromStream(MapInfo::Type &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const MapInfo::Type &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(MapInfo::Type &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const MapInfo::Type &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(MapInfo::Type &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const MapInfo::Type &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(MapInfo::Flags &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.isParallelMap, stream);
	StreamHelper::FromStream(entity.isRecoveryHPDisable, stream);
}

template<> void SaveToStream(const MapInfo::Flags &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.isParallelMap, stream);
	StreamHelper::ToStream(entity.isRecoveryHPDisable, stream);
}

template<> void LoadFromText(MapInfo::Flags &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const MapInfo::Flags &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(MapInfo::Flags &entity, const rapidjson::Value &value)
{
	FromJson(entity.isParallelMap, value, "isParallelMap");
	FromJson(entity.isRecoveryHPDisable, value, "isRecoveryHPDisable");
}

template<> void JsonHelper::BlockToJson(const MapInfo::Flags &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.isParallelMap, value, "isParallelMap");
	ToJson(entity.isRecoveryHPDisable, value, "isRecoveryHPDisable");
}

template<> const char *GetTableName<MapInfo>()
{
	return "map_info";
}

template<> const char *GetTableKeyName<MapInfo>()
{
	return "Id";
}

template<> uint32 GetTableKeyValue(const MapInfo &entity)
{
	return entity.Id;
}

template<> void SetTableKeyValue(MapInfo &entity, uint32 key)
{
	entity.Id = key;
}

template<> const char *GetTableFieldNameByIndex<MapInfo>(size_t index)
{
	switch (index)
	{
		case 0: return "Id";
		case 1: return "type";
		case 2: return "flags";
		case 3: return "strName";
		case 4: return "strSceneFile";
		case 5: return "viewing_distance";
		case 6: return "pop_map_id";
		case 7: return "pop_pos_x";
		case 8: return "pop_pos_y";
		case 9: return "pop_pos_z";
		case 10: return "pop_o";
		case 11: return "x1";
		case 12: return "y1";
		case 13: return "z1";
		case 14: return "x2";
		case 15: return "y2";
		case 16: return "z2";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<MapInfo>(const char *name)
{
	if (strcmp(name, "Id") == 0) return 0;
	if (strcmp(name, "type") == 0) return 1;
	if (strcmp(name, "flags") == 0) return 2;
	if (strcmp(name, "strName") == 0) return 3;
	if (strcmp(name, "strSceneFile") == 0) return 4;
	if (strcmp(name, "viewing_distance") == 0) return 5;
	if (strcmp(name, "pop_map_id") == 0) return 6;
	if (strcmp(name, "pop_pos_x") == 0) return 7;
	if (strcmp(name, "pop_pos_y") == 0) return 8;
	if (strcmp(name, "pop_pos_z") == 0) return 9;
	if (strcmp(name, "pop_o") == 0) return 10;
	if (strcmp(name, "x1") == 0) return 11;
	if (strcmp(name, "y1") == 0) return 12;
	if (strcmp(name, "z1") == 0) return 13;
	if (strcmp(name, "x2") == 0) return 14;
	if (strcmp(name, "y2") == 0) return 15;
	if (strcmp(name, "z2") == 0) return 16;
	return -1;
}

template<> size_t GetTableFieldNumber<MapInfo>()
{
	return 17;
}

template<> std::string GetTableFieldValue(const MapInfo &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.Id);
		case 1: return StringHelper::ToString(entity.type);
		case 2: return JsonHelper::BlockToJsonText(entity.flags);
		case 3: return StringHelper::ToString(entity.strName);
		case 4: return StringHelper::ToString(entity.strSceneFile);
		case 5: return StringHelper::ToString(entity.viewing_distance);
		case 6: return StringHelper::ToString(entity.pop_map_id);
		case 7: return StringHelper::ToString(entity.pop_pos_x);
		case 8: return StringHelper::ToString(entity.pop_pos_y);
		case 9: return StringHelper::ToString(entity.pop_pos_z);
		case 10: return StringHelper::ToString(entity.pop_o);
		case 11: return StringHelper::ToString(entity.x1);
		case 12: return StringHelper::ToString(entity.y1);
		case 13: return StringHelper::ToString(entity.z1);
		case 14: return StringHelper::ToString(entity.x2);
		case 15: return StringHelper::ToString(entity.y2);
		case 16: return StringHelper::ToString(entity.z2);
	}
	return "";
}

template<> void SetTableFieldValue(MapInfo &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.Id, value);
		case 1: return StringHelper::FromString(entity.type, value);
		case 2: return JsonHelper::BlockFromJsonText(entity.flags, value);
		case 3: return StringHelper::FromString(entity.strName, value);
		case 4: return StringHelper::FromString(entity.strSceneFile, value);
		case 5: return StringHelper::FromString(entity.viewing_distance, value);
		case 6: return StringHelper::FromString(entity.pop_map_id, value);
		case 7: return StringHelper::FromString(entity.pop_pos_x, value);
		case 8: return StringHelper::FromString(entity.pop_pos_y, value);
		case 9: return StringHelper::FromString(entity.pop_pos_z, value);
		case 10: return StringHelper::FromString(entity.pop_o, value);
		case 11: return StringHelper::FromString(entity.x1, value);
		case 12: return StringHelper::FromString(entity.y1, value);
		case 13: return StringHelper::FromString(entity.z1, value);
		case 14: return StringHelper::FromString(entity.x2, value);
		case 15: return StringHelper::FromString(entity.y2, value);
		case 16: return StringHelper::FromString(entity.z2, value);
	}
}

template<> void LoadFromStream(MapInfo &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.Id, stream);
	StreamHelper::FromStream(entity.type, stream);
	LoadFromStream(entity.flags, stream);
	StreamHelper::FromStream(entity.strName, stream);
	StreamHelper::FromStream(entity.strSceneFile, stream);
	StreamHelper::FromStream(entity.viewing_distance, stream);
	StreamHelper::FromStream(entity.pop_map_id, stream);
	StreamHelper::FromStream(entity.pop_pos_x, stream);
	StreamHelper::FromStream(entity.pop_pos_y, stream);
	StreamHelper::FromStream(entity.pop_pos_z, stream);
	StreamHelper::FromStream(entity.pop_o, stream);
	StreamHelper::FromStream(entity.x1, stream);
	StreamHelper::FromStream(entity.y1, stream);
	StreamHelper::FromStream(entity.z1, stream);
	StreamHelper::FromStream(entity.x2, stream);
	StreamHelper::FromStream(entity.y2, stream);
	StreamHelper::FromStream(entity.z2, stream);
}

template<> void SaveToStream(const MapInfo &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.Id, stream);
	StreamHelper::ToStream(entity.type, stream);
	SaveToStream(entity.flags, stream);
	StreamHelper::ToStream(entity.strName, stream);
	StreamHelper::ToStream(entity.strSceneFile, stream);
	StreamHelper::ToStream(entity.viewing_distance, stream);
	StreamHelper::ToStream(entity.pop_map_id, stream);
	StreamHelper::ToStream(entity.pop_pos_x, stream);
	StreamHelper::ToStream(entity.pop_pos_y, stream);
	StreamHelper::ToStream(entity.pop_pos_z, stream);
	StreamHelper::ToStream(entity.pop_o, stream);
	StreamHelper::ToStream(entity.x1, stream);
	StreamHelper::ToStream(entity.y1, stream);
	StreamHelper::ToStream(entity.z1, stream);
	StreamHelper::ToStream(entity.x2, stream);
	StreamHelper::ToStream(entity.y2, stream);
	StreamHelper::ToStream(entity.z2, stream);
}

template<> void LoadFromText(MapInfo &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const MapInfo &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(MapInfo &entity, const rapidjson::Value &value)
{
	FromJson(entity.Id, value, "Id");
	FromJson(entity.type, value, "type");
	BlockFromJson(entity.flags, value, "flags");
	FromJson(entity.strName, value, "strName");
	FromJson(entity.strSceneFile, value, "strSceneFile");
	FromJson(entity.viewing_distance, value, "viewing_distance");
	FromJson(entity.pop_map_id, value, "pop_map_id");
	FromJson(entity.pop_pos_x, value, "pop_pos_x");
	FromJson(entity.pop_pos_y, value, "pop_pos_y");
	FromJson(entity.pop_pos_z, value, "pop_pos_z");
	FromJson(entity.pop_o, value, "pop_o");
	FromJson(entity.x1, value, "x1");
	FromJson(entity.y1, value, "y1");
	FromJson(entity.z1, value, "z1");
	FromJson(entity.x2, value, "x2");
	FromJson(entity.y2, value, "y2");
	FromJson(entity.z2, value, "z2");
}

template<> void JsonHelper::BlockToJson(const MapInfo &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.Id, value, "Id");
	ToJson(entity.type, value, "type");
	BlockToJson(entity.flags, value, "flags");
	ToJson(entity.strName, value, "strName");
	ToJson(entity.strSceneFile, value, "strSceneFile");
	ToJson(entity.viewing_distance, value, "viewing_distance");
	ToJson(entity.pop_map_id, value, "pop_map_id");
	ToJson(entity.pop_pos_x, value, "pop_pos_x");
	ToJson(entity.pop_pos_y, value, "pop_pos_y");
	ToJson(entity.pop_pos_z, value, "pop_pos_z");
	ToJson(entity.pop_o, value, "pop_o");
	ToJson(entity.x1, value, "x1");
	ToJson(entity.y1, value, "y1");
	ToJson(entity.z1, value, "z1");
	ToJson(entity.x2, value, "x2");
	ToJson(entity.y2, value, "y2");
	ToJson(entity.z2, value, "z2");
}

template<> const char *GetTableName<MapZone>()
{
	return "map_zone";
}

template<> const char *GetTableKeyName<MapZone>()
{
	return "Id";
}

template<> uint32 GetTableKeyValue(const MapZone &entity)
{
	return entity.Id;
}

template<> void SetTableKeyValue(MapZone &entity, uint32 key)
{
	entity.Id = key;
}

template<> const char *GetTableFieldNameByIndex<MapZone>(size_t index)
{
	switch (index)
	{
		case 0: return "Id";
		case 1: return "parentId";
		case 2: return "priority";
		case 3: return "map_id";
		case 4: return "map_type";
		case 5: return "x1";
		case 6: return "y1";
		case 7: return "z1";
		case 8: return "x2";
		case 9: return "y2";
		case 10: return "z2";
		case 11: return "pvp_flags";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<MapZone>(const char *name)
{
	if (strcmp(name, "Id") == 0) return 0;
	if (strcmp(name, "parentId") == 0) return 1;
	if (strcmp(name, "priority") == 0) return 2;
	if (strcmp(name, "map_id") == 0) return 3;
	if (strcmp(name, "map_type") == 0) return 4;
	if (strcmp(name, "x1") == 0) return 5;
	if (strcmp(name, "y1") == 0) return 6;
	if (strcmp(name, "z1") == 0) return 7;
	if (strcmp(name, "x2") == 0) return 8;
	if (strcmp(name, "y2") == 0) return 9;
	if (strcmp(name, "z2") == 0) return 10;
	if (strcmp(name, "pvp_flags") == 0) return 11;
	return -1;
}

template<> size_t GetTableFieldNumber<MapZone>()
{
	return 12;
}

template<> std::string GetTableFieldValue(const MapZone &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.Id);
		case 1: return StringHelper::ToString(entity.parentId);
		case 2: return StringHelper::ToString(entity.priority);
		case 3: return StringHelper::ToString(entity.map_id);
		case 4: return StringHelper::ToString(entity.map_type);
		case 5: return StringHelper::ToString(entity.x1);
		case 6: return StringHelper::ToString(entity.y1);
		case 7: return StringHelper::ToString(entity.z1);
		case 8: return StringHelper::ToString(entity.x2);
		case 9: return StringHelper::ToString(entity.y2);
		case 10: return StringHelper::ToString(entity.z2);
		case 11: return StringHelper::ToString(entity.pvp_flags);
	}
	return "";
}

template<> void SetTableFieldValue(MapZone &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.Id, value);
		case 1: return StringHelper::FromString(entity.parentId, value);
		case 2: return StringHelper::FromString(entity.priority, value);
		case 3: return StringHelper::FromString(entity.map_id, value);
		case 4: return StringHelper::FromString(entity.map_type, value);
		case 5: return StringHelper::FromString(entity.x1, value);
		case 6: return StringHelper::FromString(entity.y1, value);
		case 7: return StringHelper::FromString(entity.z1, value);
		case 8: return StringHelper::FromString(entity.x2, value);
		case 9: return StringHelper::FromString(entity.y2, value);
		case 10: return StringHelper::FromString(entity.z2, value);
		case 11: return StringHelper::FromString(entity.pvp_flags, value);
	}
}

template<> void LoadFromStream(MapZone &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.Id, stream);
	StreamHelper::FromStream(entity.parentId, stream);
	StreamHelper::FromStream(entity.priority, stream);
	StreamHelper::FromStream(entity.map_id, stream);
	StreamHelper::FromStream(entity.map_type, stream);
	StreamHelper::FromStream(entity.x1, stream);
	StreamHelper::FromStream(entity.y1, stream);
	StreamHelper::FromStream(entity.z1, stream);
	StreamHelper::FromStream(entity.x2, stream);
	StreamHelper::FromStream(entity.y2, stream);
	StreamHelper::FromStream(entity.z2, stream);
	StreamHelper::FromStream(entity.pvp_flags, stream);
}

template<> void SaveToStream(const MapZone &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.Id, stream);
	StreamHelper::ToStream(entity.parentId, stream);
	StreamHelper::ToStream(entity.priority, stream);
	StreamHelper::ToStream(entity.map_id, stream);
	StreamHelper::ToStream(entity.map_type, stream);
	StreamHelper::ToStream(entity.x1, stream);
	StreamHelper::ToStream(entity.y1, stream);
	StreamHelper::ToStream(entity.z1, stream);
	StreamHelper::ToStream(entity.x2, stream);
	StreamHelper::ToStream(entity.y2, stream);
	StreamHelper::ToStream(entity.z2, stream);
	StreamHelper::ToStream(entity.pvp_flags, stream);
}

template<> void LoadFromText(MapZone &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const MapZone &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(MapZone &entity, const rapidjson::Value &value)
{
	FromJson(entity.Id, value, "Id");
	FromJson(entity.parentId, value, "parentId");
	FromJson(entity.priority, value, "priority");
	FromJson(entity.map_id, value, "map_id");
	FromJson(entity.map_type, value, "map_type");
	FromJson(entity.x1, value, "x1");
	FromJson(entity.y1, value, "y1");
	FromJson(entity.z1, value, "z1");
	FromJson(entity.x2, value, "x2");
	FromJson(entity.y2, value, "y2");
	FromJson(entity.z2, value, "z2");
	FromJson(entity.pvp_flags, value, "pvp_flags");
}

template<> void JsonHelper::BlockToJson(const MapZone &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.Id, value, "Id");
	ToJson(entity.parentId, value, "parentId");
	ToJson(entity.priority, value, "priority");
	ToJson(entity.map_id, value, "map_id");
	ToJson(entity.map_type, value, "map_type");
	ToJson(entity.x1, value, "x1");
	ToJson(entity.y1, value, "y1");
	ToJson(entity.z1, value, "z1");
	ToJson(entity.x2, value, "x2");
	ToJson(entity.y2, value, "y2");
	ToJson(entity.z2, value, "z2");
	ToJson(entity.pvp_flags, value, "pvp_flags");
}

template<> const char *GetTableName<MapGraveyard>()
{
	return "map_graveyard";
}

template<> const char *GetTableKeyName<MapGraveyard>()
{
	return "Id";
}

template<> uint32 GetTableKeyValue(const MapGraveyard &entity)
{
	return entity.Id;
}

template<> void SetTableKeyValue(MapGraveyard &entity, uint32 key)
{
	entity.Id = key;
}

template<> const char *GetTableFieldNameByIndex<MapGraveyard>(size_t index)
{
	switch (index)
	{
		case 0: return "Id";
		case 1: return "map_id";
		case 2: return "map_type";
		case 3: return "x";
		case 4: return "y";
		case 5: return "z";
		case 6: return "o";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<MapGraveyard>(const char *name)
{
	if (strcmp(name, "Id") == 0) return 0;
	if (strcmp(name, "map_id") == 0) return 1;
	if (strcmp(name, "map_type") == 0) return 2;
	if (strcmp(name, "x") == 0) return 3;
	if (strcmp(name, "y") == 0) return 4;
	if (strcmp(name, "z") == 0) return 5;
	if (strcmp(name, "o") == 0) return 6;
	return -1;
}

template<> size_t GetTableFieldNumber<MapGraveyard>()
{
	return 7;
}

template<> std::string GetTableFieldValue(const MapGraveyard &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.Id);
		case 1: return StringHelper::ToString(entity.map_id);
		case 2: return StringHelper::ToString(entity.map_type);
		case 3: return StringHelper::ToString(entity.x);
		case 4: return StringHelper::ToString(entity.y);
		case 5: return StringHelper::ToString(entity.z);
		case 6: return StringHelper::ToString(entity.o);
	}
	return "";
}

template<> void SetTableFieldValue(MapGraveyard &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.Id, value);
		case 1: return StringHelper::FromString(entity.map_id, value);
		case 2: return StringHelper::FromString(entity.map_type, value);
		case 3: return StringHelper::FromString(entity.x, value);
		case 4: return StringHelper::FromString(entity.y, value);
		case 5: return StringHelper::FromString(entity.z, value);
		case 6: return StringHelper::FromString(entity.o, value);
	}
}

template<> void LoadFromStream(MapGraveyard &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.Id, stream);
	StreamHelper::FromStream(entity.map_id, stream);
	StreamHelper::FromStream(entity.map_type, stream);
	StreamHelper::FromStream(entity.x, stream);
	StreamHelper::FromStream(entity.y, stream);
	StreamHelper::FromStream(entity.z, stream);
	StreamHelper::FromStream(entity.o, stream);
}

template<> void SaveToStream(const MapGraveyard &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.Id, stream);
	StreamHelper::ToStream(entity.map_id, stream);
	StreamHelper::ToStream(entity.map_type, stream);
	StreamHelper::ToStream(entity.x, stream);
	StreamHelper::ToStream(entity.y, stream);
	StreamHelper::ToStream(entity.z, stream);
	StreamHelper::ToStream(entity.o, stream);
}

template<> void LoadFromText(MapGraveyard &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const MapGraveyard &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(MapGraveyard &entity, const rapidjson::Value &value)
{
	FromJson(entity.Id, value, "Id");
	FromJson(entity.map_id, value, "map_id");
	FromJson(entity.map_type, value, "map_type");
	FromJson(entity.x, value, "x");
	FromJson(entity.y, value, "y");
	FromJson(entity.z, value, "z");
	FromJson(entity.o, value, "o");
}

template<> void JsonHelper::BlockToJson(const MapGraveyard &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.Id, value, "Id");
	ToJson(entity.map_id, value, "map_id");
	ToJson(entity.map_type, value, "map_type");
	ToJson(entity.x, value, "x");
	ToJson(entity.y, value, "y");
	ToJson(entity.z, value, "z");
	ToJson(entity.o, value, "o");
}

template<> const char *GetTableName<TeleportPoint>()
{
	return "teleport_point";
}

template<> const char *GetTableKeyName<TeleportPoint>()
{
	return "Id";
}

template<> uint32 GetTableKeyValue(const TeleportPoint &entity)
{
	return entity.Id;
}

template<> void SetTableKeyValue(TeleportPoint &entity, uint32 key)
{
	entity.Id = key;
}

template<> const char *GetTableFieldNameByIndex<TeleportPoint>(size_t index)
{
	switch (index)
	{
		case 0: return "Id";
		case 1: return "name";
		case 2: return "map_id";
		case 3: return "map_type";
		case 4: return "x";
		case 5: return "y";
		case 6: return "z";
		case 7: return "o";
		case 8: return "trait";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<TeleportPoint>(const char *name)
{
	if (strcmp(name, "Id") == 0) return 0;
	if (strcmp(name, "name") == 0) return 1;
	if (strcmp(name, "map_id") == 0) return 2;
	if (strcmp(name, "map_type") == 0) return 3;
	if (strcmp(name, "x") == 0) return 4;
	if (strcmp(name, "y") == 0) return 5;
	if (strcmp(name, "z") == 0) return 6;
	if (strcmp(name, "o") == 0) return 7;
	if (strcmp(name, "trait") == 0) return 8;
	return -1;
}

template<> size_t GetTableFieldNumber<TeleportPoint>()
{
	return 9;
}

template<> std::string GetTableFieldValue(const TeleportPoint &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.Id);
		case 1: return StringHelper::ToString(entity.name);
		case 2: return StringHelper::ToString(entity.map_id);
		case 3: return StringHelper::ToString(entity.map_type);
		case 4: return StringHelper::ToString(entity.x);
		case 5: return StringHelper::ToString(entity.y);
		case 6: return StringHelper::ToString(entity.z);
		case 7: return StringHelper::ToString(entity.o);
		case 8: return StringHelper::ToString(entity.trait);
	}
	return "";
}

template<> void SetTableFieldValue(TeleportPoint &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.Id, value);
		case 1: return StringHelper::FromString(entity.name, value);
		case 2: return StringHelper::FromString(entity.map_id, value);
		case 3: return StringHelper::FromString(entity.map_type, value);
		case 4: return StringHelper::FromString(entity.x, value);
		case 5: return StringHelper::FromString(entity.y, value);
		case 6: return StringHelper::FromString(entity.z, value);
		case 7: return StringHelper::FromString(entity.o, value);
		case 8: return StringHelper::FromString(entity.trait, value);
	}
}

template<> void LoadFromStream(TeleportPoint &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.Id, stream);
	StreamHelper::FromStream(entity.name, stream);
	StreamHelper::FromStream(entity.map_id, stream);
	StreamHelper::FromStream(entity.map_type, stream);
	StreamHelper::FromStream(entity.x, stream);
	StreamHelper::FromStream(entity.y, stream);
	StreamHelper::FromStream(entity.z, stream);
	StreamHelper::FromStream(entity.o, stream);
	StreamHelper::FromStream(entity.trait, stream);
}

template<> void SaveToStream(const TeleportPoint &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.Id, stream);
	StreamHelper::ToStream(entity.name, stream);
	StreamHelper::ToStream(entity.map_id, stream);
	StreamHelper::ToStream(entity.map_type, stream);
	StreamHelper::ToStream(entity.x, stream);
	StreamHelper::ToStream(entity.y, stream);
	StreamHelper::ToStream(entity.z, stream);
	StreamHelper::ToStream(entity.o, stream);
	StreamHelper::ToStream(entity.trait, stream);
}

template<> void LoadFromText(TeleportPoint &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const TeleportPoint &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(TeleportPoint &entity, const rapidjson::Value &value)
{
	FromJson(entity.Id, value, "Id");
	FromJson(entity.name, value, "name");
	FromJson(entity.map_id, value, "map_id");
	FromJson(entity.map_type, value, "map_type");
	FromJson(entity.x, value, "x");
	FromJson(entity.y, value, "y");
	FromJson(entity.z, value, "z");
	FromJson(entity.o, value, "o");
	FromJson(entity.trait, value, "trait");
}

template<> void JsonHelper::BlockToJson(const TeleportPoint &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.Id, value, "Id");
	ToJson(entity.name, value, "name");
	ToJson(entity.map_id, value, "map_id");
	ToJson(entity.map_type, value, "map_type");
	ToJson(entity.x, value, "x");
	ToJson(entity.y, value, "y");
	ToJson(entity.z, value, "z");
	ToJson(entity.o, value, "o");
	ToJson(entity.trait, value, "trait");
}

template<> const char *GetTableName<WayPoint>()
{
	return "way_point";
}

template<> const char *GetTableKeyName<WayPoint>()
{
	return "Id";
}

template<> uint32 GetTableKeyValue(const WayPoint &entity)
{
	return entity.Id;
}

template<> void SetTableKeyValue(WayPoint &entity, uint32 key)
{
	entity.Id = key;
}

template<> const char *GetTableFieldNameByIndex<WayPoint>(size_t index)
{
	switch (index)
	{
		case 0: return "Id";
		case 1: return "first_wp_id";
		case 2: return "prev_wp_id";
		case 3: return "next_wp_id";
		case 4: return "map_id";
		case 5: return "x";
		case 6: return "y";
		case 7: return "z";
		case 8: return "keep_idle";
		case 9: return "emote_state";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<WayPoint>(const char *name)
{
	if (strcmp(name, "Id") == 0) return 0;
	if (strcmp(name, "first_wp_id") == 0) return 1;
	if (strcmp(name, "prev_wp_id") == 0) return 2;
	if (strcmp(name, "next_wp_id") == 0) return 3;
	if (strcmp(name, "map_id") == 0) return 4;
	if (strcmp(name, "x") == 0) return 5;
	if (strcmp(name, "y") == 0) return 6;
	if (strcmp(name, "z") == 0) return 7;
	if (strcmp(name, "keep_idle") == 0) return 8;
	if (strcmp(name, "emote_state") == 0) return 9;
	return -1;
}

template<> size_t GetTableFieldNumber<WayPoint>()
{
	return 10;
}

template<> std::string GetTableFieldValue(const WayPoint &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.Id);
		case 1: return StringHelper::ToString(entity.first_wp_id);
		case 2: return StringHelper::ToString(entity.prev_wp_id);
		case 3: return StringHelper::ToString(entity.next_wp_id);
		case 4: return StringHelper::ToString(entity.map_id);
		case 5: return StringHelper::ToString(entity.x);
		case 6: return StringHelper::ToString(entity.y);
		case 7: return StringHelper::ToString(entity.z);
		case 8: return StringHelper::ToString(entity.keep_idle);
		case 9: return StringHelper::ToString(entity.emote_state);
	}
	return "";
}

template<> void SetTableFieldValue(WayPoint &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.Id, value);
		case 1: return StringHelper::FromString(entity.first_wp_id, value);
		case 2: return StringHelper::FromString(entity.prev_wp_id, value);
		case 3: return StringHelper::FromString(entity.next_wp_id, value);
		case 4: return StringHelper::FromString(entity.map_id, value);
		case 5: return StringHelper::FromString(entity.x, value);
		case 6: return StringHelper::FromString(entity.y, value);
		case 7: return StringHelper::FromString(entity.z, value);
		case 8: return StringHelper::FromString(entity.keep_idle, value);
		case 9: return StringHelper::FromString(entity.emote_state, value);
	}
}

template<> void LoadFromStream(WayPoint &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.Id, stream);
	StreamHelper::FromStream(entity.first_wp_id, stream);
	StreamHelper::FromStream(entity.prev_wp_id, stream);
	StreamHelper::FromStream(entity.next_wp_id, stream);
	StreamHelper::FromStream(entity.map_id, stream);
	StreamHelper::FromStream(entity.x, stream);
	StreamHelper::FromStream(entity.y, stream);
	StreamHelper::FromStream(entity.z, stream);
	StreamHelper::FromStream(entity.keep_idle, stream);
	StreamHelper::FromStream(entity.emote_state, stream);
}

template<> void SaveToStream(const WayPoint &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.Id, stream);
	StreamHelper::ToStream(entity.first_wp_id, stream);
	StreamHelper::ToStream(entity.prev_wp_id, stream);
	StreamHelper::ToStream(entity.next_wp_id, stream);
	StreamHelper::ToStream(entity.map_id, stream);
	StreamHelper::ToStream(entity.x, stream);
	StreamHelper::ToStream(entity.y, stream);
	StreamHelper::ToStream(entity.z, stream);
	StreamHelper::ToStream(entity.keep_idle, stream);
	StreamHelper::ToStream(entity.emote_state, stream);
}

template<> void LoadFromText(WayPoint &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const WayPoint &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(WayPoint &entity, const rapidjson::Value &value)
{
	FromJson(entity.Id, value, "Id");
	FromJson(entity.first_wp_id, value, "first_wp_id");
	FromJson(entity.prev_wp_id, value, "prev_wp_id");
	FromJson(entity.next_wp_id, value, "next_wp_id");
	FromJson(entity.map_id, value, "map_id");
	FromJson(entity.x, value, "x");
	FromJson(entity.y, value, "y");
	FromJson(entity.z, value, "z");
	FromJson(entity.keep_idle, value, "keep_idle");
	FromJson(entity.emote_state, value, "emote_state");
}

template<> void JsonHelper::BlockToJson(const WayPoint &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.Id, value, "Id");
	ToJson(entity.first_wp_id, value, "first_wp_id");
	ToJson(entity.prev_wp_id, value, "prev_wp_id");
	ToJson(entity.next_wp_id, value, "next_wp_id");
	ToJson(entity.map_id, value, "map_id");
	ToJson(entity.x, value, "x");
	ToJson(entity.y, value, "y");
	ToJson(entity.z, value, "z");
	ToJson(entity.keep_idle, value, "keep_idle");
	ToJson(entity.emote_state, value, "emote_state");
}

template<> const char *GetTableName<LandmarkPoint>()
{
	return "landmark_point";
}

template<> const char *GetTableKeyName<LandmarkPoint>()
{
	return "Id";
}

template<> uint32 GetTableKeyValue(const LandmarkPoint &entity)
{
	return entity.Id;
}

template<> void SetTableKeyValue(LandmarkPoint &entity, uint32 key)
{
	entity.Id = key;
}

template<> const char *GetTableFieldNameByIndex<LandmarkPoint>(size_t index)
{
	switch (index)
	{
		case 0: return "Id";
		case 1: return "name";
		case 2: return "map_id";
		case 3: return "map_type";
		case 4: return "x";
		case 5: return "y";
		case 6: return "z";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<LandmarkPoint>(const char *name)
{
	if (strcmp(name, "Id") == 0) return 0;
	if (strcmp(name, "name") == 0) return 1;
	if (strcmp(name, "map_id") == 0) return 2;
	if (strcmp(name, "map_type") == 0) return 3;
	if (strcmp(name, "x") == 0) return 4;
	if (strcmp(name, "y") == 0) return 5;
	if (strcmp(name, "z") == 0) return 6;
	return -1;
}

template<> size_t GetTableFieldNumber<LandmarkPoint>()
{
	return 7;
}

template<> std::string GetTableFieldValue(const LandmarkPoint &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.Id);
		case 1: return StringHelper::ToString(entity.name);
		case 2: return StringHelper::ToString(entity.map_id);
		case 3: return StringHelper::ToString(entity.map_type);
		case 4: return StringHelper::ToString(entity.x);
		case 5: return StringHelper::ToString(entity.y);
		case 6: return StringHelper::ToString(entity.z);
	}
	return "";
}

template<> void SetTableFieldValue(LandmarkPoint &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.Id, value);
		case 1: return StringHelper::FromString(entity.name, value);
		case 2: return StringHelper::FromString(entity.map_id, value);
		case 3: return StringHelper::FromString(entity.map_type, value);
		case 4: return StringHelper::FromString(entity.x, value);
		case 5: return StringHelper::FromString(entity.y, value);
		case 6: return StringHelper::FromString(entity.z, value);
	}
}

template<> void LoadFromStream(LandmarkPoint &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.Id, stream);
	StreamHelper::FromStream(entity.name, stream);
	StreamHelper::FromStream(entity.map_id, stream);
	StreamHelper::FromStream(entity.map_type, stream);
	StreamHelper::FromStream(entity.x, stream);
	StreamHelper::FromStream(entity.y, stream);
	StreamHelper::FromStream(entity.z, stream);
}

template<> void SaveToStream(const LandmarkPoint &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.Id, stream);
	StreamHelper::ToStream(entity.name, stream);
	StreamHelper::ToStream(entity.map_id, stream);
	StreamHelper::ToStream(entity.map_type, stream);
	StreamHelper::ToStream(entity.x, stream);
	StreamHelper::ToStream(entity.y, stream);
	StreamHelper::ToStream(entity.z, stream);
}

template<> void LoadFromText(LandmarkPoint &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const LandmarkPoint &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(LandmarkPoint &entity, const rapidjson::Value &value)
{
	FromJson(entity.Id, value, "Id");
	FromJson(entity.name, value, "name");
	FromJson(entity.map_id, value, "map_id");
	FromJson(entity.map_type, value, "map_type");
	FromJson(entity.x, value, "x");
	FromJson(entity.y, value, "y");
	FromJson(entity.z, value, "z");
}

template<> void JsonHelper::BlockToJson(const LandmarkPoint &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.Id, value, "Id");
	ToJson(entity.name, value, "name");
	ToJson(entity.map_id, value, "map_id");
	ToJson(entity.map_type, value, "map_type");
	ToJson(entity.x, value, "x");
	ToJson(entity.y, value, "y");
	ToJson(entity.z, value, "z");
}
