#include "jsontable/table_helper.h"
#include "struct_quest.h"

template<> void LoadFromStream(QuestNavIdx &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const QuestNavIdx &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(QuestNavIdx &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestNavIdx &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestNavIdx &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const QuestNavIdx &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(QuestClassType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const QuestClassType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(QuestClassType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestClassType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestClassType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const QuestClassType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(QuestRepeatType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const QuestRepeatType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(QuestRepeatType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestRepeatType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestRepeatType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const QuestRepeatType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(QuestWhenType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const QuestWhenType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(QuestWhenType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestWhenType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestWhenType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const QuestWhenType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(QuestObjType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const QuestObjType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(QuestObjType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestObjType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestObjType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const QuestObjType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(QuestObjInst &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.objType, stream);
	StreamHelper::FromStream(entity.objID, stream);
}

template<> void SaveToStream(const QuestObjInst &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.objType, stream);
	StreamHelper::ToStream(entity.objID, stream);
}

template<> void LoadFromText(QuestObjInst &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestObjInst &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestObjInst &entity, const rapidjson::Value &value)
{
	FromJson(entity.objType, value, "objType");
	FromJson(entity.objID, value, "objID");
}

template<> void JsonHelper::BlockToJson(const QuestObjInst &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.objType, value, "objType");
	ToJson(entity.objID, value, "objID");
}

template<> void LoadFromStream(QuestScript &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.scriptID, stream);
	StreamHelper::FromStream(entity.scriptArgs, stream);
}

template<> void SaveToStream(const QuestScript &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.scriptID, stream);
	StreamHelper::ToStream(entity.scriptArgs, stream);
}

template<> void LoadFromText(QuestScript &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestScript &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestScript &entity, const rapidjson::Value &value)
{
	FromJson(entity.scriptID, value, "scriptID");
	FromJson(entity.scriptArgs, value, "scriptArgs");
}

template<> void JsonHelper::BlockToJson(const QuestScript &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.scriptID, value, "scriptID");
	ToJson(entity.scriptArgs, value, "scriptArgs");
}

template<> void LoadFromStream(QuestCheque &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.chequeType, stream);
	StreamHelper::FromStream(entity.chequeValue, stream);
}

template<> void SaveToStream(const QuestCheque &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.chequeType, stream);
	StreamHelper::ToStream(entity.chequeValue, stream);
}

template<> void LoadFromText(QuestCheque &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestCheque &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestCheque &entity, const rapidjson::Value &value)
{
	FromJson(entity.chequeType, value, "chequeType");
	FromJson(entity.chequeValue, value, "chequeValue");
}

template<> void JsonHelper::BlockToJson(const QuestCheque &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.chequeType, value, "chequeType");
	ToJson(entity.chequeValue, value, "chequeValue");
}

template<> void LoadFromStream(QuestItem &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.itemTypeID, stream);
	StreamHelper::FromStream(entity.itemCount, stream);
	StreamHelper::FromStream(entity.onlyCareer, stream);
	StreamHelper::FromStream(entity.onlyGender, stream);
}

template<> void SaveToStream(const QuestItem &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.itemTypeID, stream);
	StreamHelper::ToStream(entity.itemCount, stream);
	StreamHelper::ToStream(entity.onlyCareer, stream);
	StreamHelper::ToStream(entity.onlyGender, stream);
}

template<> void LoadFromText(QuestItem &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestItem &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestItem &entity, const rapidjson::Value &value)
{
	FromJson(entity.itemTypeID, value, "itemTypeID");
	FromJson(entity.itemCount, value, "itemCount");
	FromJson(entity.onlyCareer, value, "onlyCareer");
	FromJson(entity.onlyGender, value, "onlyGender");
}

template<> void JsonHelper::BlockToJson(const QuestItem &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.itemTypeID, value, "itemTypeID");
	ToJson(entity.itemCount, value, "itemCount");
	ToJson(entity.onlyCareer, value, "onlyCareer");
	ToJson(entity.onlyGender, value, "onlyGender");
}

template<> void LoadFromStream(QuestChequeReq &entity, std::istream &stream)
{
	LoadFromStream<QuestCheque>(entity, stream);
	StreamHelper::FromStream(entity.isCost, stream);
	StreamHelper::FromStream(entity.isRefund, stream);
}

template<> void SaveToStream(const QuestChequeReq &entity, std::ostream &stream)
{
	SaveToStream<QuestCheque>(entity, stream);
	StreamHelper::ToStream(entity.isCost, stream);
	StreamHelper::ToStream(entity.isRefund, stream);
}

template<> void LoadFromText(QuestChequeReq &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestChequeReq &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestChequeReq &entity, const rapidjson::Value &value)
{
	BlockFromJson<QuestCheque>(entity, value);
	FromJson(entity.isCost, value, "isCost");
	FromJson(entity.isRefund, value, "isRefund");
}

template<> void JsonHelper::BlockToJson(const QuestChequeReq &entity, rapidjson::Value &value)
{
	BlockToJson<QuestCheque>(entity, value);
	ToJson(entity.isCost, value, "isCost");
	ToJson(entity.isRefund, value, "isRefund");
}

template<> void LoadFromStream(QuestItemReq &entity, std::istream &stream)
{
	LoadFromStream<QuestItem>(entity, stream);
	StreamHelper::FromStream(entity.isDestroy, stream);
	StreamHelper::FromStream(entity.isRefund, stream);
	StreamHelper::FromStream(entity.isBinding, stream);
}

template<> void SaveToStream(const QuestItemReq &entity, std::ostream &stream)
{
	SaveToStream<QuestItem>(entity, stream);
	StreamHelper::ToStream(entity.isDestroy, stream);
	StreamHelper::ToStream(entity.isRefund, stream);
	StreamHelper::ToStream(entity.isBinding, stream);
}

template<> void LoadFromText(QuestItemReq &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestItemReq &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestItemReq &entity, const rapidjson::Value &value)
{
	BlockFromJson<QuestItem>(entity, value);
	FromJson(entity.isDestroy, value, "isDestroy");
	FromJson(entity.isRefund, value, "isRefund");
	FromJson(entity.isBinding, value, "isBinding");
}

template<> void JsonHelper::BlockToJson(const QuestItemReq &entity, rapidjson::Value &value)
{
	BlockToJson<QuestItem>(entity, value);
	ToJson(entity.isDestroy, value, "isDestroy");
	ToJson(entity.isRefund, value, "isRefund");
	ToJson(entity.isBinding, value, "isBinding");
}

template<> void LoadFromStream(QuestChequeInit &entity, std::istream &stream)
{
	LoadFromStream<QuestCheque>(entity, stream);
	StreamHelper::FromStream(entity.isRetrieve, stream);
}

template<> void SaveToStream(const QuestChequeInit &entity, std::ostream &stream)
{
	SaveToStream<QuestCheque>(entity, stream);
	StreamHelper::ToStream(entity.isRetrieve, stream);
}

template<> void LoadFromText(QuestChequeInit &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestChequeInit &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestChequeInit &entity, const rapidjson::Value &value)
{
	BlockFromJson<QuestCheque>(entity, value);
	FromJson(entity.isRetrieve, value, "isRetrieve");
}

template<> void JsonHelper::BlockToJson(const QuestChequeInit &entity, rapidjson::Value &value)
{
	BlockToJson<QuestCheque>(entity, value);
	ToJson(entity.isRetrieve, value, "isRetrieve");
}

template<> void LoadFromStream(QuestItemInit &entity, std::istream &stream)
{
	LoadFromStream<QuestItem>(entity, stream);
	StreamHelper::FromStream(entity.isBinding, stream);
	StreamHelper::FromStream(entity.isRetrieve, stream);
}

template<> void SaveToStream(const QuestItemInit &entity, std::ostream &stream)
{
	SaveToStream<QuestItem>(entity, stream);
	StreamHelper::ToStream(entity.isBinding, stream);
	StreamHelper::ToStream(entity.isRetrieve, stream);
}

template<> void LoadFromText(QuestItemInit &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestItemInit &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestItemInit &entity, const rapidjson::Value &value)
{
	BlockFromJson<QuestItem>(entity, value);
	FromJson(entity.isBinding, value, "isBinding");
	FromJson(entity.isRetrieve, value, "isRetrieve");
}

template<> void JsonHelper::BlockToJson(const QuestItemInit &entity, rapidjson::Value &value)
{
	BlockToJson<QuestItem>(entity, value);
	ToJson(entity.isBinding, value, "isBinding");
	ToJson(entity.isRetrieve, value, "isRetrieve");
}

template<> void LoadFromStream(QuestChequeReward &entity, std::istream &stream)
{
	LoadFromStream<QuestCheque>(entity, stream);
	StreamHelper::FromStream(entity.isFixed, stream);
}

template<> void SaveToStream(const QuestChequeReward &entity, std::ostream &stream)
{
	SaveToStream<QuestCheque>(entity, stream);
	StreamHelper::ToStream(entity.isFixed, stream);
}

template<> void LoadFromText(QuestChequeReward &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestChequeReward &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestChequeReward &entity, const rapidjson::Value &value)
{
	BlockFromJson<QuestCheque>(entity, value);
	FromJson(entity.isFixed, value, "isFixed");
}

template<> void JsonHelper::BlockToJson(const QuestChequeReward &entity, rapidjson::Value &value)
{
	BlockToJson<QuestCheque>(entity, value);
	ToJson(entity.isFixed, value, "isFixed");
}

template<> void LoadFromStream(QuestItemReward &entity, std::istream &stream)
{
	LoadFromStream<QuestItem>(entity, stream);
	StreamHelper::FromStream(entity.isBinding, stream);
}

template<> void SaveToStream(const QuestItemReward &entity, std::ostream &stream)
{
	SaveToStream<QuestItem>(entity, stream);
	StreamHelper::ToStream(entity.isBinding, stream);
}

template<> void LoadFromText(QuestItemReward &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestItemReward &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestItemReward &entity, const rapidjson::Value &value)
{
	BlockFromJson<QuestItem>(entity, value);
	FromJson(entity.isBinding, value, "isBinding");
}

template<> void JsonHelper::BlockToJson(const QuestItemReward &entity, rapidjson::Value &value)
{
	BlockToJson<QuestItem>(entity, value);
	ToJson(entity.isBinding, value, "isBinding");
}

template<> void LoadFromStream(QuestConditionType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const QuestConditionType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(QuestConditionType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestConditionType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestConditionType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const QuestConditionType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> void LoadFromStream(QuestCondition &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.conditionType, stream);
	StreamHelper::SequenceFromStream(entity.conditionIds, stream);
	StreamHelper::FromStream(entity.conditionNum, stream);
	StreamHelper::SequenceFromStream(entity.conditionArgs, stream);
	StreamHelper::SequenceFromStream(entity.conditionFlags, stream);
	LoadFromStream(entity.forNavInfo, stream);
}

template<> void SaveToStream(const QuestCondition &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.conditionType, stream);
	StreamHelper::SequenceToStream(entity.conditionIds, stream);
	StreamHelper::ToStream(entity.conditionNum, stream);
	StreamHelper::SequenceToStream(entity.conditionArgs, stream);
	StreamHelper::SequenceToStream(entity.conditionFlags, stream);
	SaveToStream(entity.forNavInfo, stream);
}

template<> void LoadFromText(QuestCondition &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestCondition &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestCondition &entity, const rapidjson::Value &value)
{
	FromJson(entity.conditionType, value, "conditionType");
	SequenceFromJson(entity.conditionIds, value, "conditionIds");
	FromJson(entity.conditionNum, value, "conditionNum");
	SequenceFromJson(entity.conditionArgs, value, "conditionArgs");
	SequenceFromJson(entity.conditionFlags, value, "conditionFlags");
	BlockFromJson(entity.forNavInfo, value, "forNavInfo");
}

template<> void JsonHelper::BlockToJson(const QuestCondition &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.conditionType, value, "conditionType");
	SequenceToJson(entity.conditionIds, value, "conditionIds");
	ToJson(entity.conditionNum, value, "conditionNum");
	SequenceToJson(entity.conditionArgs, value, "conditionArgs");
	SequenceToJson(entity.conditionFlags, value, "conditionFlags");
	BlockToJson(entity.forNavInfo, value, "forNavInfo");
}

template<> void LoadFromStream(QuestPrototype::Flags &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.isWatchStatus, stream);
	StreamHelper::FromStream(entity.isAutoAccept, stream);
	StreamHelper::FromStream(entity.isAutoSubmit, stream);
	StreamHelper::FromStream(entity.isStoryMode, stream);
	StreamHelper::FromStream(entity.isEnterSceneAeap, stream);
	StreamHelper::FromStream(entity.isLeaveSceneAeap, stream);
	StreamHelper::FromStream(entity.isCantArchive, stream);
	StreamHelper::FromStream(entity.isCantCancel, stream);
	StreamHelper::FromStream(entity.isAnyReqQuest, stream);
	StreamHelper::FromStream(entity.isAnyCondition, stream);
	StreamHelper::FromStream(entity.isRemoveFailed, stream);
}

template<> void SaveToStream(const QuestPrototype::Flags &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.isWatchStatus, stream);
	StreamHelper::ToStream(entity.isAutoAccept, stream);
	StreamHelper::ToStream(entity.isAutoSubmit, stream);
	StreamHelper::ToStream(entity.isStoryMode, stream);
	StreamHelper::ToStream(entity.isEnterSceneAeap, stream);
	StreamHelper::ToStream(entity.isLeaveSceneAeap, stream);
	StreamHelper::ToStream(entity.isCantArchive, stream);
	StreamHelper::ToStream(entity.isCantCancel, stream);
	StreamHelper::ToStream(entity.isAnyReqQuest, stream);
	StreamHelper::ToStream(entity.isAnyCondition, stream);
	StreamHelper::ToStream(entity.isRemoveFailed, stream);
}

template<> void LoadFromText(QuestPrototype::Flags &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestPrototype::Flags &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestPrototype::Flags &entity, const rapidjson::Value &value)
{
	FromJson(entity.isWatchStatus, value, "isWatchStatus");
	FromJson(entity.isAutoAccept, value, "isAutoAccept");
	FromJson(entity.isAutoSubmit, value, "isAutoSubmit");
	FromJson(entity.isStoryMode, value, "isStoryMode");
	FromJson(entity.isEnterSceneAeap, value, "isEnterSceneAeap");
	FromJson(entity.isLeaveSceneAeap, value, "isLeaveSceneAeap");
	FromJson(entity.isCantArchive, value, "isCantArchive");
	FromJson(entity.isCantCancel, value, "isCantCancel");
	FromJson(entity.isAnyReqQuest, value, "isAnyReqQuest");
	FromJson(entity.isAnyCondition, value, "isAnyCondition");
	FromJson(entity.isRemoveFailed, value, "isRemoveFailed");
}

template<> void JsonHelper::BlockToJson(const QuestPrototype::Flags &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.isWatchStatus, value, "isWatchStatus");
	ToJson(entity.isAutoAccept, value, "isAutoAccept");
	ToJson(entity.isAutoSubmit, value, "isAutoSubmit");
	ToJson(entity.isStoryMode, value, "isStoryMode");
	ToJson(entity.isEnterSceneAeap, value, "isEnterSceneAeap");
	ToJson(entity.isLeaveSceneAeap, value, "isLeaveSceneAeap");
	ToJson(entity.isCantArchive, value, "isCantArchive");
	ToJson(entity.isCantCancel, value, "isCantCancel");
	ToJson(entity.isAnyReqQuest, value, "isAnyReqQuest");
	ToJson(entity.isAnyCondition, value, "isAnyCondition");
	ToJson(entity.isRemoveFailed, value, "isRemoveFailed");
}

template<> const char *GetTableName<QuestPrototype>()
{
	return "quest_prototype";
}

template<> const char *GetTableKeyName<QuestPrototype>()
{
	return "questTypeID";
}

template<> uint32 GetTableKeyValue(const QuestPrototype &entity)
{
	return entity.questTypeID;
}

template<> void SetTableKeyValue(QuestPrototype &entity, uint32 key)
{
	entity.questTypeID = key;
}

template<> const char *GetTableFieldNameByIndex<QuestPrototype>(size_t index)
{
	switch (index)
	{
		case 0: return "questTypeID";
		case 1: return "questClass";
		case 2: return "questFlags";
		case 3: return "questPublisher";
		case 4: return "questVerifier";
		case 5: return "questGuider";
		case 6: return "questTimeMax";
		case 7: return "questRepeatType";
		case 8: return "questRepeatMax";
		case 9: return "questReqMinLv";
		case 10: return "questReqMaxLv";
		case 11: return "questReqCareer";
		case 12: return "questReqGender";
		case 13: return "questReqQuests";
		case 14: return "questReqCheques";
		case 15: return "questReqItems";
		case 16: return "questReqExtra";
		case 17: return "questInitCheques";
		case 18: return "questInitItems";
		case 19: return "questInitExtra";
		case 20: return "questRewardCheques";
		case 21: return "questRewardItems";
		case 22: return "questRewardExtra";
		case 23: return "questScripts";
		case 24: return "questConditions";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<QuestPrototype>(const char *name)
{
	if (strcmp(name, "questTypeID") == 0) return 0;
	if (strcmp(name, "questClass") == 0) return 1;
	if (strcmp(name, "questFlags") == 0) return 2;
	if (strcmp(name, "questPublisher") == 0) return 3;
	if (strcmp(name, "questVerifier") == 0) return 4;
	if (strcmp(name, "questGuider") == 0) return 5;
	if (strcmp(name, "questTimeMax") == 0) return 6;
	if (strcmp(name, "questRepeatType") == 0) return 7;
	if (strcmp(name, "questRepeatMax") == 0) return 8;
	if (strcmp(name, "questReqMinLv") == 0) return 9;
	if (strcmp(name, "questReqMaxLv") == 0) return 10;
	if (strcmp(name, "questReqCareer") == 0) return 11;
	if (strcmp(name, "questReqGender") == 0) return 12;
	if (strcmp(name, "questReqQuests") == 0) return 13;
	if (strcmp(name, "questReqCheques") == 0) return 14;
	if (strcmp(name, "questReqItems") == 0) return 15;
	if (strcmp(name, "questReqExtra") == 0) return 16;
	if (strcmp(name, "questInitCheques") == 0) return 17;
	if (strcmp(name, "questInitItems") == 0) return 18;
	if (strcmp(name, "questInitExtra") == 0) return 19;
	if (strcmp(name, "questRewardCheques") == 0) return 20;
	if (strcmp(name, "questRewardItems") == 0) return 21;
	if (strcmp(name, "questRewardExtra") == 0) return 22;
	if (strcmp(name, "questScripts") == 0) return 23;
	if (strcmp(name, "questConditions") == 0) return 24;
	return -1;
}

template<> size_t GetTableFieldNumber<QuestPrototype>()
{
	return 25;
}

template<> std::string GetTableFieldValue(const QuestPrototype &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.questTypeID);
		case 1: return StringHelper::ToString(entity.questClass);
		case 2: return JsonHelper::BlockToJsonText(entity.questFlags);
		case 3: return JsonHelper::BlockToJsonText(entity.questPublisher);
		case 4: return JsonHelper::BlockToJsonText(entity.questVerifier);
		case 5: return JsonHelper::BlockToJsonText(entity.questGuider);
		case 6: return StringHelper::ToString(entity.questTimeMax);
		case 7: return StringHelper::ToString(entity.questRepeatType);
		case 8: return StringHelper::ToString(entity.questRepeatMax);
		case 9: return StringHelper::ToString(entity.questReqMinLv);
		case 10: return StringHelper::ToString(entity.questReqMaxLv);
		case 11: return StringHelper::ToString(entity.questReqCareer);
		case 12: return StringHelper::ToString(entity.questReqGender);
		case 13: return JsonHelper::SequenceToJsonText(entity.questReqQuests);
		case 14: return JsonHelper::BlockSequenceToJsonText(entity.questReqCheques);
		case 15: return JsonHelper::BlockSequenceToJsonText(entity.questReqItems);
		case 16: return JsonHelper::BlockToJsonText(entity.questReqExtra);
		case 17: return JsonHelper::BlockSequenceToJsonText(entity.questInitCheques);
		case 18: return JsonHelper::BlockSequenceToJsonText(entity.questInitItems);
		case 19: return JsonHelper::BlockToJsonText(entity.questInitExtra);
		case 20: return JsonHelper::BlockSequenceToJsonText(entity.questRewardCheques);
		case 21: return JsonHelper::BlockSequenceToJsonText(entity.questRewardItems);
		case 22: return JsonHelper::BlockToJsonText(entity.questRewardExtra);
		case 23: return JsonHelper::BlockSequenceToJsonText(entity.questScripts);
		case 24: return JsonHelper::BlockSequenceToJsonText(entity.questConditions);
	}
	return "";
}

template<> void SetTableFieldValue(QuestPrototype &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.questTypeID, value);
		case 1: return StringHelper::FromString(entity.questClass, value);
		case 2: return JsonHelper::BlockFromJsonText(entity.questFlags, value);
		case 3: return JsonHelper::BlockFromJsonText(entity.questPublisher, value);
		case 4: return JsonHelper::BlockFromJsonText(entity.questVerifier, value);
		case 5: return JsonHelper::BlockFromJsonText(entity.questGuider, value);
		case 6: return StringHelper::FromString(entity.questTimeMax, value);
		case 7: return StringHelper::FromString(entity.questRepeatType, value);
		case 8: return StringHelper::FromString(entity.questRepeatMax, value);
		case 9: return StringHelper::FromString(entity.questReqMinLv, value);
		case 10: return StringHelper::FromString(entity.questReqMaxLv, value);
		case 11: return StringHelper::FromString(entity.questReqCareer, value);
		case 12: return StringHelper::FromString(entity.questReqGender, value);
		case 13: return JsonHelper::SequenceFromJsonText(entity.questReqQuests, value);
		case 14: return JsonHelper::BlockSequenceFromJsonText(entity.questReqCheques, value);
		case 15: return JsonHelper::BlockSequenceFromJsonText(entity.questReqItems, value);
		case 16: return JsonHelper::BlockFromJsonText(entity.questReqExtra, value);
		case 17: return JsonHelper::BlockSequenceFromJsonText(entity.questInitCheques, value);
		case 18: return JsonHelper::BlockSequenceFromJsonText(entity.questInitItems, value);
		case 19: return JsonHelper::BlockFromJsonText(entity.questInitExtra, value);
		case 20: return JsonHelper::BlockSequenceFromJsonText(entity.questRewardCheques, value);
		case 21: return JsonHelper::BlockSequenceFromJsonText(entity.questRewardItems, value);
		case 22: return JsonHelper::BlockFromJsonText(entity.questRewardExtra, value);
		case 23: return JsonHelper::BlockSequenceFromJsonText(entity.questScripts, value);
		case 24: return JsonHelper::BlockSequenceFromJsonText(entity.questConditions, value);
	}
}

template<> void LoadFromStream(QuestPrototype &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.questTypeID, stream);
	StreamHelper::FromStream(entity.questClass, stream);
	LoadFromStream(entity.questFlags, stream);
	LoadFromStream(entity.questPublisher, stream);
	LoadFromStream(entity.questVerifier, stream);
	LoadFromStream(entity.questGuider, stream);
	StreamHelper::FromStream(entity.questTimeMax, stream);
	StreamHelper::FromStream(entity.questRepeatType, stream);
	StreamHelper::FromStream(entity.questRepeatMax, stream);
	StreamHelper::FromStream(entity.questReqMinLv, stream);
	StreamHelper::FromStream(entity.questReqMaxLv, stream);
	StreamHelper::FromStream(entity.questReqCareer, stream);
	StreamHelper::FromStream(entity.questReqGender, stream);
	StreamHelper::SequenceFromStream(entity.questReqQuests, stream);
	StreamHelper::BlockSequenceFromStream(entity.questReqCheques, stream);
	StreamHelper::BlockSequenceFromStream(entity.questReqItems, stream);
	LoadFromStream(entity.questReqExtra, stream);
	StreamHelper::BlockSequenceFromStream(entity.questInitCheques, stream);
	StreamHelper::BlockSequenceFromStream(entity.questInitItems, stream);
	LoadFromStream(entity.questInitExtra, stream);
	StreamHelper::BlockSequenceFromStream(entity.questRewardCheques, stream);
	StreamHelper::BlockSequenceFromStream(entity.questRewardItems, stream);
	LoadFromStream(entity.questRewardExtra, stream);
	StreamHelper::BlockSequenceFromStream(entity.questScripts, stream);
	StreamHelper::BlockSequenceFromStream(entity.questConditions, stream);
}

template<> void SaveToStream(const QuestPrototype &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.questTypeID, stream);
	StreamHelper::ToStream(entity.questClass, stream);
	SaveToStream(entity.questFlags, stream);
	SaveToStream(entity.questPublisher, stream);
	SaveToStream(entity.questVerifier, stream);
	SaveToStream(entity.questGuider, stream);
	StreamHelper::ToStream(entity.questTimeMax, stream);
	StreamHelper::ToStream(entity.questRepeatType, stream);
	StreamHelper::ToStream(entity.questRepeatMax, stream);
	StreamHelper::ToStream(entity.questReqMinLv, stream);
	StreamHelper::ToStream(entity.questReqMaxLv, stream);
	StreamHelper::ToStream(entity.questReqCareer, stream);
	StreamHelper::ToStream(entity.questReqGender, stream);
	StreamHelper::SequenceToStream(entity.questReqQuests, stream);
	StreamHelper::BlockSequenceToStream(entity.questReqCheques, stream);
	StreamHelper::BlockSequenceToStream(entity.questReqItems, stream);
	SaveToStream(entity.questReqExtra, stream);
	StreamHelper::BlockSequenceToStream(entity.questInitCheques, stream);
	StreamHelper::BlockSequenceToStream(entity.questInitItems, stream);
	SaveToStream(entity.questInitExtra, stream);
	StreamHelper::BlockSequenceToStream(entity.questRewardCheques, stream);
	StreamHelper::BlockSequenceToStream(entity.questRewardItems, stream);
	SaveToStream(entity.questRewardExtra, stream);
	StreamHelper::BlockSequenceToStream(entity.questScripts, stream);
	StreamHelper::BlockSequenceToStream(entity.questConditions, stream);
}

template<> void LoadFromText(QuestPrototype &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestPrototype &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestPrototype &entity, const rapidjson::Value &value)
{
	FromJson(entity.questTypeID, value, "questTypeID");
	FromJson(entity.questClass, value, "questClass");
	BlockFromJson(entity.questFlags, value, "questFlags");
	BlockFromJson(entity.questPublisher, value, "questPublisher");
	BlockFromJson(entity.questVerifier, value, "questVerifier");
	BlockFromJson(entity.questGuider, value, "questGuider");
	FromJson(entity.questTimeMax, value, "questTimeMax");
	FromJson(entity.questRepeatType, value, "questRepeatType");
	FromJson(entity.questRepeatMax, value, "questRepeatMax");
	FromJson(entity.questReqMinLv, value, "questReqMinLv");
	FromJson(entity.questReqMaxLv, value, "questReqMaxLv");
	FromJson(entity.questReqCareer, value, "questReqCareer");
	FromJson(entity.questReqGender, value, "questReqGender");
	SequenceFromJson(entity.questReqQuests, value, "questReqQuests");
	BlockSequenceFromJson(entity.questReqCheques, value, "questReqCheques");
	BlockSequenceFromJson(entity.questReqItems, value, "questReqItems");
	BlockFromJson(entity.questReqExtra, value, "questReqExtra");
	BlockSequenceFromJson(entity.questInitCheques, value, "questInitCheques");
	BlockSequenceFromJson(entity.questInitItems, value, "questInitItems");
	BlockFromJson(entity.questInitExtra, value, "questInitExtra");
	BlockSequenceFromJson(entity.questRewardCheques, value, "questRewardCheques");
	BlockSequenceFromJson(entity.questRewardItems, value, "questRewardItems");
	BlockFromJson(entity.questRewardExtra, value, "questRewardExtra");
	BlockSequenceFromJson(entity.questScripts, value, "questScripts");
	BlockSequenceFromJson(entity.questConditions, value, "questConditions");
}

template<> void JsonHelper::BlockToJson(const QuestPrototype &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.questTypeID, value, "questTypeID");
	ToJson(entity.questClass, value, "questClass");
	BlockToJson(entity.questFlags, value, "questFlags");
	BlockToJson(entity.questPublisher, value, "questPublisher");
	BlockToJson(entity.questVerifier, value, "questVerifier");
	BlockToJson(entity.questGuider, value, "questGuider");
	ToJson(entity.questTimeMax, value, "questTimeMax");
	ToJson(entity.questRepeatType, value, "questRepeatType");
	ToJson(entity.questRepeatMax, value, "questRepeatMax");
	ToJson(entity.questReqMinLv, value, "questReqMinLv");
	ToJson(entity.questReqMaxLv, value, "questReqMaxLv");
	ToJson(entity.questReqCareer, value, "questReqCareer");
	ToJson(entity.questReqGender, value, "questReqGender");
	SequenceToJson(entity.questReqQuests, value, "questReqQuests");
	BlockSequenceToJson(entity.questReqCheques, value, "questReqCheques");
	BlockSequenceToJson(entity.questReqItems, value, "questReqItems");
	BlockToJson(entity.questReqExtra, value, "questReqExtra");
	BlockSequenceToJson(entity.questInitCheques, value, "questInitCheques");
	BlockSequenceToJson(entity.questInitItems, value, "questInitItems");
	BlockToJson(entity.questInitExtra, value, "questInitExtra");
	BlockSequenceToJson(entity.questRewardCheques, value, "questRewardCheques");
	BlockSequenceToJson(entity.questRewardItems, value, "questRewardItems");
	BlockToJson(entity.questRewardExtra, value, "questRewardExtra");
	BlockSequenceToJson(entity.questScripts, value, "questScripts");
	BlockSequenceToJson(entity.questConditions, value, "questConditions");
}

template<> const char *GetTableName<QuestCreatureVisible>()
{
	return "quest_creature_visible";
}

template<> const char *GetTableKeyName<QuestCreatureVisible>()
{
	return "";
}

template<> uint32 GetTableKeyValue(const QuestCreatureVisible &entity)
{
	return 0;
}

template<> void SetTableKeyValue(QuestCreatureVisible &entity, uint32 key)
{
}

template<> const char *GetTableFieldNameByIndex<QuestCreatureVisible>(size_t index)
{
	switch (index)
	{
		case 0: return "questTypeID";
		case 1: return "questWhenType";
		case 2: return "isVisible";
		case 3: return "spawnIDs";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<QuestCreatureVisible>(const char *name)
{
	if (strcmp(name, "questTypeID") == 0) return 0;
	if (strcmp(name, "questWhenType") == 0) return 1;
	if (strcmp(name, "isVisible") == 0) return 2;
	if (strcmp(name, "spawnIDs") == 0) return 3;
	return -1;
}

template<> size_t GetTableFieldNumber<QuestCreatureVisible>()
{
	return 4;
}

template<> std::string GetTableFieldValue(const QuestCreatureVisible &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.questTypeID);
		case 1: return StringHelper::ToString(entity.questWhenType);
		case 2: return StringHelper::ToString(entity.isVisible);
		case 3: return JsonHelper::SequenceToJsonText(entity.spawnIDs);
	}
	return "";
}

template<> void SetTableFieldValue(QuestCreatureVisible &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.questTypeID, value);
		case 1: return StringHelper::FromString(entity.questWhenType, value);
		case 2: return StringHelper::FromString(entity.isVisible, value);
		case 3: return JsonHelper::SequenceFromJsonText(entity.spawnIDs, value);
	}
}

template<> void LoadFromStream(QuestCreatureVisible &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.questTypeID, stream);
	StreamHelper::FromStream(entity.questWhenType, stream);
	StreamHelper::FromStream(entity.isVisible, stream);
	StreamHelper::SequenceFromStream(entity.spawnIDs, stream);
}

template<> void SaveToStream(const QuestCreatureVisible &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.questTypeID, stream);
	StreamHelper::ToStream(entity.questWhenType, stream);
	StreamHelper::ToStream(entity.isVisible, stream);
	StreamHelper::SequenceToStream(entity.spawnIDs, stream);
}

template<> void LoadFromText(QuestCreatureVisible &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const QuestCreatureVisible &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(QuestCreatureVisible &entity, const rapidjson::Value &value)
{
	FromJson(entity.questTypeID, value, "questTypeID");
	FromJson(entity.questWhenType, value, "questWhenType");
	FromJson(entity.isVisible, value, "isVisible");
	SequenceFromJson(entity.spawnIDs, value, "spawnIDs");
}

template<> void JsonHelper::BlockToJson(const QuestCreatureVisible &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.questTypeID, value, "questTypeID");
	ToJson(entity.questWhenType, value, "questWhenType");
	ToJson(entity.isVisible, value, "isVisible");
	SequenceToJson(entity.spawnIDs, value, "spawnIDs");
}
