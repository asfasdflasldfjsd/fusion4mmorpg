#pragma once

struct EditorTreeView
{
	EditorTreeView();

	enum class Type {
		None,
		Spell,
		Quest,
		Char,
		SObj,
		Item,
		Loot,
	};
	uint32 etvType;
	std::string etvTree;
};
