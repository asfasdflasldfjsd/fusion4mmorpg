#include "jsontable/table_helper.h"
#include "struct_scriptable.h"

template<> void LoadFromStream(ScriptType &entity, std::istream &stream)
{
	StreamHelper::EnumFromStream(entity, stream);
}

template<> void SaveToStream(const ScriptType &entity, std::ostream &stream)
{
	StreamHelper::EnumToStream(entity, stream);
}

template<> void LoadFromText(ScriptType &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const ScriptType &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(ScriptType &entity, const rapidjson::Value &value)
{
	EnumFromJson(entity, value);
}

template<> void JsonHelper::BlockToJson(const ScriptType &entity, rapidjson::Value &value)
{
	EnumToJson(entity, value);
}

template<> const char *GetTableName<Scriptable>()
{
	return "scriptable";
}

template<> const char *GetTableKeyName<Scriptable>()
{
	return "scriptId";
}

template<> uint32 GetTableKeyValue(const Scriptable &entity)
{
	return entity.scriptId;
}

template<> void SetTableKeyValue(Scriptable &entity, uint32 key)
{
	entity.scriptId = key;
}

template<> const char *GetTableFieldNameByIndex<Scriptable>(size_t index)
{
	switch (index)
	{
		case 0: return "scriptId";
		case 1: return "scriptType";
		case 2: return "scriptFile";
	}
	return "";
}

template<> ssize_t GetTableFieldIndexByName<Scriptable>(const char *name)
{
	if (strcmp(name, "scriptId") == 0) return 0;
	if (strcmp(name, "scriptType") == 0) return 1;
	if (strcmp(name, "scriptFile") == 0) return 2;
	return -1;
}

template<> size_t GetTableFieldNumber<Scriptable>()
{
	return 3;
}

template<> std::string GetTableFieldValue(const Scriptable &entity, size_t index)
{
	switch (index)
	{
		case 0: return StringHelper::ToString(entity.scriptId);
		case 1: return StringHelper::ToString(entity.scriptType);
		case 2: return StringHelper::ToString(entity.scriptFile);
	}
	return "";
}

template<> void SetTableFieldValue(Scriptable &entity, size_t index, const std::string_view &value)
{
	switch (index)
	{
		case 0: return StringHelper::FromString(entity.scriptId, value);
		case 1: return StringHelper::FromString(entity.scriptType, value);
		case 2: return StringHelper::FromString(entity.scriptFile, value);
	}
}

template<> void LoadFromStream(Scriptable &entity, std::istream &stream)
{
	StreamHelper::FromStream(entity.scriptId, stream);
	StreamHelper::FromStream(entity.scriptType, stream);
	StreamHelper::FromStream(entity.scriptFile, stream);
}

template<> void SaveToStream(const Scriptable &entity, std::ostream &stream)
{
	StreamHelper::ToStream(entity.scriptId, stream);
	StreamHelper::ToStream(entity.scriptType, stream);
	StreamHelper::ToStream(entity.scriptFile, stream);
}

template<> void LoadFromText(Scriptable &entity, const char *text)
{
	JsonHelper::BlockFromJsonText(entity, text);
}

template<> std::string SaveToText(const Scriptable &entity)
{
	return JsonHelper::BlockToJsonText(entity);
}

template<> void JsonHelper::BlockFromJson(Scriptable &entity, const rapidjson::Value &value)
{
	FromJson(entity.scriptId, value, "scriptId");
	FromJson(entity.scriptType, value, "scriptType");
	FromJson(entity.scriptFile, value, "scriptFile");
}

template<> void JsonHelper::BlockToJson(const Scriptable &entity, rapidjson::Value &value)
{
	SetJsonObjectValue(value);
	ToJson(entity.scriptId, value, "scriptId");
	ToJson(entity.scriptType, value, "scriptType");
	ToJson(entity.scriptFile, value, "scriptFile");
}
