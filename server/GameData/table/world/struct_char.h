#pragma once

enum class CharRaceType
{
	Player,
	Neutral,
	HostileForces,
	FriendlyForces,
};

enum class CharEliteType
{
	Normal,
	Boss,
};

struct CharPrototype
{
	CharPrototype();

	struct Flags {
		Flags();
		bool isUndead;
		bool isJoinCombat;
		bool isFastTurn;
		bool isKeepDir;
		bool isActivity;
		bool isIgnoreInvisible;
		bool isSenseCreature;
		bool isSensePlayer;
	};

	uint32 charTypeId;
	Flags charFlags;
	uint32 charRace;
	uint32 charElite;
	uint32 level;

	float speedWalk;
	float speedRun;
	float speedTurn;

	float patrolRange;
	float boundRadius;
	float senseRadius;
	float combatRadius;

	float combatBestDist;

	uint32 minRespawnTime;
	uint32 maxRespawnTime;
	uint32 deadDisappearTime;

	double recoveryHPRate;
	double recoveryHPValue;
	double recoveryMPRate;
	double recoveryMPValue;

	uint32 lootSetID;

	uint32 aiScriptId;
	uint32 spawnScriptId;
	std::string spawnScriptArgs;
	uint32 deadScriptId;
	std::string deadScriptArgs;
	uint32 playScriptId;
	std::string playScriptArgs;
};


struct CreatureSpawn
{
	CreatureSpawn();

	struct Flags {
		Flags();
		bool isRespawn;
	};
	uint32 spawnId;
	Flags flags;
	uint32 entry;
	uint32 level;
	uint32 map_id;
	uint32 map_type;
	float x, y, z, o;
	uint32 idleType;
	uint32 wayPointId;
};
