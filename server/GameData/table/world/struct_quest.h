#pragma once

enum class QuestNavIdx
{
	Publisher = -1,
	Verifier = -2,
	Guider = -3,
};

enum class QuestClassType
{
	None,
	MainLine,
	BranchLine,
};

enum class QuestRepeatType
{
	None,
	Daily,
	Weekly,
	Monthly,
	Forever,
	Count,
};

enum class QuestWhenType
{
	Accept,
	Finish,
	Failed,
	Cancel,
	Submit,
	Count,
};

enum class QuestObjType
{
	None,
	Guide,
	NPC,
	SObj,
	NPCPt,
	SObjPt,
	Count,
};

struct QuestObjInst
{
	QuestObjInst();

	uint32 objType;
	uint32 objID;
};

struct QuestScript
{
	QuestScript();

	uint32 scriptID;
	std::string scriptArgs;
};

struct QuestCheque
{
	QuestCheque();

	uint32 chequeType;
	uint32 chequeValue;
};

struct QuestItem
{
	QuestItem();

	uint32 itemTypeID;
	uint32 itemCount;
	uint32 onlyCareer;
	uint32 onlyGender;
};

struct QuestChequeReq : QuestCheque
{
	QuestChequeReq();

	bool isCost;
	bool isRefund;
};

struct QuestItemReq : QuestItem
{
	QuestItemReq();

	bool isDestroy;
	bool isRefund;
	bool isBinding;
};

struct QuestChequeInit : QuestCheque
{
	QuestChequeInit();

	bool isRetrieve;
};

struct QuestItemInit : QuestItem
{
	QuestItemInit();

	bool isBinding;
	bool isRetrieve;
};

struct QuestChequeReward : QuestCheque
{
	QuestChequeReward();

	bool isFixed;
};

struct QuestItemReward : QuestItem
{
	QuestItemReward();

	bool isBinding;
};

enum class QuestConditionType
{
	None,
	TalkNPC,
	KillCreature,
	HaveCheque,
	HaveItem,
	UseItem,
	PlayStory,
	Count,
};

struct QuestCondition
{
	QuestCondition();

	uint32 conditionType;
	std::vector<uint32> conditionIds;
	uint64 conditionNum;
	std::vector<uint32> conditionArgs;
	std::vector<bool> conditionFlags;
	QuestObjInst forNavInfo;
};

struct QuestPrototype
{
	QuestPrototype();

	struct Flags {
		Flags();
		bool isWatchStatus;
		bool isAutoAccept;
		bool isAutoSubmit;
		bool isStoryMode;
		bool isEnterSceneAeap;
		bool isLeaveSceneAeap;
		bool isCantArchive;
		bool isCantCancel;
		bool isAnyReqQuest;
		bool isAnyCondition;
		bool isRemoveFailed;
	};

	uint32 questTypeID;
	uint32 questClass;
	Flags questFlags;
	QuestObjInst questPublisher;
	QuestObjInst questVerifier;
	QuestObjInst questGuider;
	uint32 questTimeMax;

	uint32 questRepeatType;
	uint32 questRepeatMax;

	uint32 questReqMinLv;
	uint32 questReqMaxLv;
	uint32 questReqCareer;
	uint32 questReqGender;
	std::vector<uint32> questReqQuests;
	std::vector<QuestChequeReq> questReqCheques;
	std::vector<QuestItemReq> questReqItems;
	QuestScript questReqExtra;

	std::vector<QuestChequeInit> questInitCheques;
	std::vector<QuestItemInit> questInitItems;
	QuestScript questInitExtra;

	std::vector<QuestChequeReward> questRewardCheques;
	std::vector<QuestItemReward> questRewardItems;
	QuestScript questRewardExtra;

	std::vector<QuestScript> questScripts;
	std::vector<QuestCondition> questConditions;
};


struct QuestCreatureVisible
{
	QuestCreatureVisible();

	uint32 questTypeID;
	int8 questWhenType;
	bool isVisible;
	std::vector<uint32> spawnIDs;
};
