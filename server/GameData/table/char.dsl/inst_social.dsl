table social_friend
{
	required uint32 characterId;
	required uint32 friendId;
	required int64 createTime;
}

table social_ignore
{
	required uint32 characterId;
	required uint32 ignoreId;
	required int64 createTime;
}