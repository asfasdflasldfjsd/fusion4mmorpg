struct IpcFlags
{
	bool isInit;
}

struct IpcAppearance
{
	uint32 charTypeId;
	uint8 hairType;
	uint8 hairColor;
	uint8 face;
	uint8 skin;
}

struct IpcPropertyValue
{
	uint32 bagCapacity;
	uint32 bankCapacity;
}

struct IpcRankValue
{
	int64 lastLevelTime;
	int64 lastFightValueTime;
}

struct IpcGsReadValue
{
	uint64 lastFightValue;
}

struct IpcGsWriteValue
{
	int64 guildScore;
}

table InstPlayerPreviewInfo
{
	required uint32 ipcInstID;
	required string ipcNickName;
	required IpcAppearance ipcAppearance;
	required uint8 ipcCareer;
	required uint8 ipcGender;
	required uint32 ipcLevel;
	required int64 ipcMoneyGold;
	required int64 ipcMoneyDiamond;
	required string ipcEffectItems;
	(key=ipcInstID,tblname=inst_player_char)
}

table InstPlayerOutlineInfo
{
	required uint32 ipcInstID;
	required uint32 ipcAcctID;
	required string ipcNickName;
	required IpcFlags ipcFlags;
	required uint8 ipcCareer;
	required uint8 ipcGender;
	required uint16 ipcMapType;
	required uint16 ipcMapID;
	required float ipcPosX;
	required float ipcPosY;
	required float ipcPosZ;
	required float ipcPosO;
	required uint32 ipcLevel;
	required uint32 ipcServerID;
	required int64 ipcLastOnlineTime;
	required IpcRankValue ipcRankValue;
	required IpcGsReadValue ipcGsReadValue;
	required IpcGsWriteValue ipcGsWriteValue;
	(key=ipcInstID,tblname=inst_player_char)
}

table inst_player_char
{
	required uint32 ipcInstID;
	required uint32 ipcAcctID;
	required string ipcNickName;
	required IpcFlags ipcFlags;
	required IpcAppearance ipcAppearance;
	required uint8 ipcCareer;
	required uint8 ipcGender;
	required uint16 ipcMapType;
	required uint16 ipcMapID;
	required float ipcPosX;
	required float ipcPosY;
	required float ipcPosZ;
	required float ipcPosO;
	required uint32 ipcLevel;
	required int64 ipcExp;
	required int64 ipcCurHP;
	required int64 ipcCurMP;
	required int64 ipcMoneyGold;
	required int64 ipcMoneyDiamond;
	required uint32 ipcServerID;
	required int64 ipcCreateTime;
	required int64 ipcLastLoginTime;
	required int64 ipcLastOnlineTime;
	required int64 ipcDeleteTime;
	required string ipcQuestsDone;
	required string ipcQuests;
	required string ipcEffectItems;
	required string ipcOtherItems;
	required string ipcItemStorage;
	required string ipcShopStatus;
	required string ipcSpells;
	required string ipcBuffs;
	required string ipcCooldowns;
	required string ipcJsonValue;
	required float[] ipcF32Values;
	required int32[] ipcS32Values;
	required int64[] ipcS64Values;
	required IpcPropertyValue ipcPropertyValue;
	required IpcRankValue ipcRankValue;
	required IpcGsReadValue ipcGsReadValue;
	required IpcGsWriteValue ipcGsWriteValue;
	(key=ipcInstID)
}