#pragma once

enum MULTILANG
{
	LANG_EN,
	LANG_CN,
	LANG_MAX
};

enum class STRING_TEXT_TYPE;
inline uint32 MakeTextID(uint32 id, STRING_TEXT_TYPE type) {
	return ((uint32)type << 24) + id;
}
