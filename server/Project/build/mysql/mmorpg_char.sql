-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2019-07-24 02:59:01
-- 服务器版本： 8.0.12
-- PHP 版本： 7.1.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `mmorpg_char`
--

-- --------------------------------------------------------

--
-- 表的结构 `guild`
--

CREATE TABLE `guild` (
  `Id` int(10) UNSIGNED NOT NULL,
  `gsId` int(10) UNSIGNED NOT NULL,
  `name` varchar(256) NOT NULL,
  `buildTime` bigint(20) NOT NULL,
  `level` int(10) UNSIGNED NOT NULL,
  `levelTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- 表的结构 `guild_apply`
--

CREATE TABLE `guild_apply` (
  `playerId` int(10) UNSIGNED NOT NULL,
  `guildId` int(10) UNSIGNED NOT NULL,
  `applyTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- 表的结构 `guild_member`
--

CREATE TABLE `guild_member` (
  `playerId` int(10) UNSIGNED NOT NULL,
  `guildId` int(10) UNSIGNED NOT NULL,
  `guildTitle` tinyint(4) NOT NULL,
  `joinTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- 表的结构 `inst_configure`
--

CREATE TABLE `inst_configure` (
  `cfgID` int(10) UNSIGNED NOT NULL,
  `cfgValue` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- 表的结构 `inst_mail`
--

CREATE TABLE `inst_mail` (
  `mailID` int(10) UNSIGNED NOT NULL,
  `mailType` int(10) UNSIGNED NOT NULL,
  `mailFlags` int(10) UNSIGNED NOT NULL,
  `mailSender` int(10) UNSIGNED NOT NULL,
  `mailSenderName` varchar(128) NOT NULL,
  `mailReceiver` int(10) UNSIGNED NOT NULL,
  `mailDeliverTime` bigint(20) NOT NULL,
  `mailExpireTime` bigint(20) NOT NULL,
  `mailSubject` varchar(256) NOT NULL,
  `mailBody` text NOT NULL,
  `mailCheques` json NOT NULL,
  `mailItems` json NOT NULL,
  `isGetAttachment` tinyint(1) NOT NULL,
  `isViewDetail` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- 表的结构 `inst_player_char`
--

CREATE TABLE `inst_player_char` (
  `ipcInstID` int(10) UNSIGNED NOT NULL,
  `ipcAcctID` int(10) UNSIGNED NOT NULL,
  `ipcNickName` varchar(128) NOT NULL,
  `ipcFlags` json NOT NULL,
  `ipcAppearance` json NOT NULL,
  `ipcCareer` tinyint(3) UNSIGNED NOT NULL,
  `ipcGender` tinyint(3) UNSIGNED NOT NULL,
  `ipcMapType` smallint(5) UNSIGNED NOT NULL,
  `ipcMapID` smallint(5) UNSIGNED NOT NULL,
  `ipcPosX` float NOT NULL,
  `ipcPosY` float NOT NULL,
  `ipcPosZ` float NOT NULL,
  `ipcPosO` float NOT NULL,
  `ipcLevel` int(10) UNSIGNED NOT NULL,
  `ipcExp` bigint(20) NOT NULL,
  `ipcCurHP` bigint(20) NOT NULL,
  `ipcCurMP` bigint(20) NOT NULL,
  `ipcMoneyGold` bigint(20) NOT NULL,
  `ipcMoneyDiamond` bigint(20) NOT NULL,
  `ipcServerID` int(10) UNSIGNED NOT NULL,
  `ipcCreateTime` bigint(20) NOT NULL,
  `ipcLastLoginTime` bigint(20) NOT NULL,
  `ipcLastOnlineTime` bigint(20) NOT NULL,
  `ipcDeleteTime` bigint(20) NOT NULL,
  `ipcQuestsDone` text NOT NULL,
  `ipcQuests` text NOT NULL,
  `ipcEffectItems` text NOT NULL,
  `ipcOtherItems` text NOT NULL,
  `ipcItemStorage` text NOT NULL,
  `ipcShopStatus` text NOT NULL,
  `ipcSpells` text NOT NULL,
  `ipcBuffs` text NOT NULL,
  `ipcCooldowns` text NOT NULL,
  `ipcJsonValue` json NOT NULL,
  `ipcF32Values` json NOT NULL,
  `ipcS32Values` json NOT NULL,
  `ipcS64Values` json NOT NULL,
  `ipcPropertyValue` json NOT NULL,
  `ipcRankValue` json NOT NULL,
  `ipcGsReadValue` json NOT NULL,
  `ipcGsWriteValue` json NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- 表的结构 `social_friend`
--

CREATE TABLE `social_friend` (
  `characterId` int(10) UNSIGNED NOT NULL,
  `friendId` int(10) UNSIGNED NOT NULL,
  `createTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- 表的结构 `social_ignore`
--

CREATE TABLE `social_ignore` (
  `characterId` int(10) UNSIGNED NOT NULL,
  `ignoreId` int(10) UNSIGNED NOT NULL,
  `createTime` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- 转储表的索引
--

--
-- 表的索引 `guild`
--
ALTER TABLE `guild`
  ADD PRIMARY KEY (`Id`);

--
-- 表的索引 `guild_apply`
--
ALTER TABLE `guild_apply`
  ADD UNIQUE KEY `playerId` (`playerId`,`guildId`);

--
-- 表的索引 `guild_member`
--
ALTER TABLE `guild_member`
  ADD PRIMARY KEY (`playerId`);

--
-- 表的索引 `inst_configure`
--
ALTER TABLE `inst_configure`
  ADD PRIMARY KEY (`cfgID`);

--
-- 表的索引 `inst_mail`
--
ALTER TABLE `inst_mail`
  ADD PRIMARY KEY (`mailID`),
  ADD KEY `mailReceiver` (`mailReceiver`);

--
-- 表的索引 `inst_player_char`
--
ALTER TABLE `inst_player_char`
  ADD PRIMARY KEY (`ipcInstID`),
  ADD UNIQUE KEY `ipcNickName` (`ipcNickName`),
  ADD KEY `ipcAcctID` (`ipcAcctID`);

--
-- 表的索引 `social_friend`
--
ALTER TABLE `social_friend`
  ADD UNIQUE KEY `characterId` (`characterId`,`friendId`);

--
-- 表的索引 `social_ignore`
--
ALTER TABLE `social_ignore`
  ADD UNIQUE KEY `characterId` (`characterId`,`ignoreId`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `inst_mail`
--
ALTER TABLE `inst_mail`
  MODIFY `mailID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
