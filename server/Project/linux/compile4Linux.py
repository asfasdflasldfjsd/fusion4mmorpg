# -*- coding: utf-8 -*-

import os
from convert4Linux import *

def RunCommand(command):
    os.chdir('build')
    os.system(command)
    os.chdir('..')

if __name__ == '__main__':
    FixProjectInclues()
    while True:
        cmd = input('Input a command[convert/clean/build]:')
        if cmd == 'convert':
            DumpProject()
            RunCommand('cmake -DCMAKE_BUILD_TYPE=Debug .')
        elif cmd == 'clean':
            RunCommand('make clean')
        elif cmd == 'build':
            RunCommand('make -j4')
