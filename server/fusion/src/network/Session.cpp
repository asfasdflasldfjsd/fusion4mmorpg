#include "Session.h"
#include "SessionManager.h"
#include "ConnectionManager.h"
#include "Logger.h"
#include "System.h"

Session::Session()
: status_(Idle)
, manager_(nullptr)
, overflow_packet_count_(0)
, overflow_packet_max_size_(SIZE_MAX)
, shutdown_time_(-1)
, last_recv_pck_time_(GET_APP_TIME)
, last_send_pck_time_(GET_APP_TIME)
{
}

Session::~Session()
{
    ClearRecvPacket();
}

void Session::Update()
{
    uint32 opcode = OPCODE_NONE;
    INetPacket *pck = nullptr;
    TRY_BEGIN {

        while (IsActive() && recv_queue_.Dequeue(pck)) {
            opcode = pck->GetOpcode();
            switch (HandlePacket(pck)) {
            case SessionHandleSuccess:
                break;
            case SessionHandleCapture:
                pck = nullptr;
                break;
            case SessionHandleUnhandle:
                DLOG("SessionHandleUnhandle Opcode[%u].", opcode);
                break;
            case SessionHandleWarning:
                WLOG("Handle Opcode[%u] Warning!", opcode);
                break;
            case SessionHandleError:
                ELOG("Handle Opcode[%u] Error!", opcode);
                break;
            case SessionHandleKill:
            default:
                WLOG("Fatal error occurred when processing opcode[%u], "
                     "the session has been removed.", opcode);
                ShutdownSession();
                break;
            }
            SAFE_DELETE(pck);
        }

    } TRY_END
    CATCH_BEGIN(const IException &e) {
        WLOG("Handle session opcode[%u] exception occurred.", opcode);
        e.Print();
        KillSession();
    } CATCH_END
    CATCH_BEGIN(...) {
        WLOG("Handle session opcode[%u] unknown exception occurred.", opcode);
        KillSession();
    } CATCH_END

    SAFE_DELETE(pck);
}

void Session::ConnectServer(const std::string &address, const std::string &port)
{
    std::shared_ptr<Connection> connPtr = sConnectionManager.NewConnection(*this);
    connPtr->AsyncConnect(address, port);
    sSessionManager.AddSession(this);
}

void Session::SetConnection(std::shared_ptr<Connection> &&conn)
{
    connection_ = std::move(conn);
}

const std::shared_ptr<Connection> &Session::GetConnection() const
{
    return connection_;
}

void Session::DeleteObject()
{
    delete this;
}

void Session::OnTick()
{
}

void Session::OnConnected()
{
}

void Session::OnManaged()
{
}

void Session::Disconnect()
{
    if (connection_ && connection_->IsActive()) {
        connection_->PostCloseRequest();
    }
}

bool Session::IsIndependent() const
{
    return !connection_ || connection_.use_count() == 1;
}

bool Session::HasSendDataAwaiting() const
{
    return connection_->HasSendDataAwaiting();
}

size_t Session::GetSendDataSize() const
{
    return connection_->GetSendDataSize();
}

int Session::GetConnectionLoadValue() const
{
    return 1;
}

const std::string &Session::GetHost() const
{
    return connection_->addr();
}

unsigned long Session::GetIPv4() const
{
    boost::asio::ip::address addr;
    addr.from_string(connection_->addr().c_str());
    return addr.is_v4() ? addr.to_v4().to_ulong() : 0;
}

unsigned short Session::GetPort() const
{
    return connection_->port();
}

void Session::ClearShutdownFlag()
{
    shutdown_time_.store(-1);
}

bool Session::GrabShutdownFlag()
{
    time_t expected = -1;
    return shutdown_time_.compare_exchange_strong(expected, GET_UNIX_TIME);
}

bool Session::IsShutdownExpired() const
{
    return shutdown_time_.load() + 30 < GET_UNIX_TIME;
}

void Session::KillSession()
{
    if (manager_ != nullptr) {
        manager_->KillSession(this);
    }
}

void Session::ShutdownSession()
{
    if (manager_ != nullptr) {
        manager_->ShutdownSession(this);
    }
}

void Session::OnShutdownSession()
{
}

void Session::OnRecvPacket(INetPacket *pck)
{
    recv_queue_.Enqueue(pck);
}

void Session::PushRecvPacket(INetPacket *pck)
{
    if (IsActive()) {
        if (pck->GetOpcode() == OPCODE_LARGE_PACKET) {
            PushRecvFragmentPacket(pck);
        } else {
            OnRecvPacket(pck);
        }
        last_recv_pck_time_ = GET_APP_TIME;
    } else {
        delete pck;
    }
}

void Session::PushSendPacket(const INetPacket &pck)
{
    if (IsActive() && connection_) {
        if (pck.GetReadableSize() > INetPacket::MAX_BUFFER_SIZE) {
            PushSendOverflowPacket(pck);
        } else {
            connection_->GetSendBuffer().WritePacket(pck);
        }
        connection_->PostWriteRequest();
        last_send_pck_time_ = GET_APP_TIME;
    }
}

void Session::PushSendPacket(const std::string_view &data)
{
    if (IsActive() && connection_) {
        connection_->GetSendBuffer().WritePacket(data);
        connection_->PostWriteRequest();
        last_send_pck_time_ = GET_APP_TIME;
    }
}

void Session::PushSendPacket(const INetPacket &pck, const INetPacket &data)
{
    if (IsActive() && connection_) {
        if (pck.GetReadableSize() + data.GetReadableSize() +
                INetPacket::Header::SIZE > INetPacket::MAX_BUFFER_SIZE) {
            PushSendOverflowPacket(pck, data);
        } else {
            connection_->GetSendBuffer().WritePacket(pck, data);
        }
        connection_->PostWriteRequest();
        last_send_pck_time_ = GET_APP_TIME;
    }
}

void Session::PushSendPacket(const INetPacket &pck, const std::string_view &data)
{
    if (IsActive() && connection_) {
        if (pck.GetReadableSize() + data.size() > INetPacket::MAX_BUFFER_SIZE) {
            PushSendOverflowPacket(pck, data);
        } else {
            connection_->GetSendBuffer().WritePacket(pck, data);
        }
        connection_->PostWriteRequest();
        last_send_pck_time_ = GET_APP_TIME;
    }
}

void Session::PushSendPacket(const INetPacket *pcks[], size_t pck_num,
                             const std::string_view datas[], size_t data_num)
{
    if (IsActive() && connection_) {
        assert(pck_num >= 1);
        auto packet_total_size = pcks[0]->GetReadableSize();
        for (size_t i = 1; i < pck_num; ++i) {
            packet_total_size +=
                pcks[i]->GetReadableSize() + INetPacket::Header::SIZE;
        }
        for (size_t i = 0; i < data_num; ++i) {
            packet_total_size += datas[i].size();
        }
        if (packet_total_size > INetPacket::MAX_BUFFER_SIZE) {
            PushSendOverflowPacket(
                packet_total_size, pcks, pck_num, datas, data_num);
        } else {
            connection_->GetSendBuffer().WritePacket(
                packet_total_size, pcks, pck_num, datas, data_num);
        }
        connection_->PostWriteRequest();
        last_send_pck_time_ = GET_APP_TIME;
    }
}

class Session::LargePacketHelper {
public:
    static INetPacket &UnpackPacket(INetPacket &pck) {
        const uint32 len = pck.Read<uint32>();
        pck.SetOpcode(pck.Read<uint16>());
        if (len != pck.GetReadableSize() + LARGE_PACKET_HEADER_SIZE) {
            THROW_EXCEPTION(NetStreamException());
        }
        return pck;
    }
    static void WritePacketHeader(INetPacket &pck, size_t size, uint32 opcode) {
        pck.Write<uint32>(uint32(size + LARGE_PACKET_HEADER_SIZE));
        pck.Write<uint16>(opcode);
    }
    static const size_t LARGE_PACKET_HEADER_SIZE = 4 + 2;
};

void Session::PushRecvFragmentPacket(INetPacket *pck)
{
    const size_t packet_total_size = pck->GetTotalSize();
    const uint32 number = pck->Read<uint32>();
    do {
        std::lock_guard<std::mutex> lock(fragment_mutex_);
        INetPacket *&packet = fragment_packets_[number];
        if (packet == nullptr) {
            packet = pck;
        } else {
            packet->Append(pck->GetReadableBuffer(), pck->GetReadableSize());
            delete pck;
        }
        if (packet_total_size < INetPacket::MAX_BUFFER_SIZE) {
            OnRecvPacket(&LargePacketHelper::UnpackPacket(*packet));
            fragment_packets_.erase(number);
        } else if (packet->GetTotalSize() > overflow_packet_max_size_) {
            THROW_EXCEPTION(NetStreamException());
        }
    } while (0);
}

void Session::PushSendOverflowPacket(const INetPacket &pck)
{
    ConstNetBuffer datas[] = {
        { pck.GetReadableBuffer(), pck.GetReadableSize() },
    };
    PushSendFragmentPacket(pck.GetOpcode(), datas, ARRAY_SIZE(datas));
}

void Session::PushSendOverflowPacket(const INetPacket &pck, const INetPacket &data)
{
    TNetPacket<INetPacket::Header::SIZE> wrapper;
    wrapper.WriteHeader(INetPacket::Header(data.GetOpcode(),
        (data.GetReadableSize() + INetPacket::Header::SIZE) & 0xffff));

    ConstNetBuffer datas[] = {
        { pck.GetReadableBuffer(), pck.GetReadableSize() },
        { wrapper.GetBuffer(), wrapper.GetTotalSize() },
        { data.GetReadableBuffer(), data.GetReadableSize() },
    };
    PushSendFragmentPacket(pck.GetOpcode(), datas, ARRAY_SIZE(datas));
}

void Session::PushSendOverflowPacket(const INetPacket &pck, const std::string_view &data)
{
    ConstNetBuffer datas[] = {
        { pck.GetReadableBuffer(), pck.GetReadableSize() },
        { data.data(), data.size() },
    };
    PushSendFragmentPacket(pck.GetOpcode(), datas, ARRAY_SIZE(datas));
}

void Session::PushSendOverflowPacket(size_t packet_total_size,
                                     const INetPacket *pcks[], size_t pck_num,
                                     const std::string_view datas[], size_t data_num)
{
    std::vector<TNetPacket<INetPacket::Header::SIZE>> wrappers(pck_num - 1);
    std::vector<ConstNetBuffer> buffers((pck_num << 1) - 1 + data_num);
    REINIT_OBJECT(&buffers[0], pcks[0]->GetReadableBuffer(), pcks[0]->GetReadableSize());
    packet_total_size -= pcks[0]->GetReadableSize();
    for (size_t i = 1; i < pck_num; ++i) {
        wrappers[i - 1].WriteHeader(
            INetPacket::Header(pcks[i]->GetOpcode(), packet_total_size & 0xffff));
        REINIT_OBJECT(&buffers[(i << 1) - 1],
            wrappers[i - 1].GetBuffer(), wrappers[i - 1].GetTotalSize());
        REINIT_OBJECT(&buffers[(i << 1)],
            pcks[i]->GetReadableBuffer(), pcks[i]->GetReadableSize());
        packet_total_size -= pcks[i]->GetReadableSize() + INetPacket::Header::SIZE;
    }
    for (size_t i = 0; i < data_num; ++i) {
        REINIT_OBJECT(
            &buffers[(pck_num << 1) - 1 + i], datas[i].data(), datas[i].size());
        packet_total_size -= datas[i].size();
    }
    assert(packet_total_size == 0);
    PushSendFragmentPacket(pcks[0]->GetOpcode(), buffers.data(), buffers.size());
}

void Session::PushSendFragmentPacket(uint32 opcode, ConstNetBuffer datas[], size_t count)
{
    size_t data_total_size = 0;
    for (size_t i = 0; i < count; ++i) {
        data_total_size += datas[i].GetTotalSize();
    }

    NetPacket packet(OPCODE_LARGE_PACKET);
    packet << (uint32)overflow_packet_count_.fetch_add(1);
    const size_t packet_prefix_size = packet.GetTotalSize();
    LargePacketHelper::WritePacketHeader(packet, data_total_size, opcode);

    bool is_residual_data = true;
    for (size_t i = 0; i < count; ++i) {
        auto &data = datas[i];
        while (!data.IsReadableEmpty()) {
            const char *data_buffer = data.GetReadableBuffer();
            const size_t packet_space_size = INetPacket::MAX_BUFFER_SIZE - packet.GetTotalSize();
            const size_t data_avail_size = std::min(data.GetReadableSize(), packet_space_size);
            if (data_avail_size < packet_space_size && data_avail_size < data_total_size) {
                packet.Append(data_buffer, data_avail_size);
            } else {
                connection_->GetSendBuffer().WritePacket(
                    packet, std::string_view(data_buffer, data_avail_size));
                is_residual_data = data_avail_size >= packet_space_size;
                packet.Shrink(packet_prefix_size);
            }
            data.AdjustReadPos(data_avail_size);
            data_total_size -= data_avail_size;
        }
    }

    if (is_residual_data) {
        connection_->GetSendBuffer().WritePacket(packet);
    }

    DBGASSERT(data_total_size == 0);
}

void Session::ClearRecvPacket()
{
    INetPacket *pck = nullptr;
    while (recv_queue_.Dequeue(pck)) {
        delete pck;
    }
    do {
        std::lock_guard<std::mutex> lock(fragment_mutex_);
        for (auto &pair : fragment_packets_) {
            delete pair.second;
        }
        fragment_packets_.clear();
    } while (0);
}
