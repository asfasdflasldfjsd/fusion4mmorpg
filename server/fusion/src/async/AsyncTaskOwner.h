#pragma once

#include "enable_linked_from_this.h"
#include "noncopyable.h"
#include "MultiBufferQueue.h"
#include "ThreadSafeSet.h"

class AsyncTask;

class AsyncTaskOwner : public noncopyable,
    public enable_linked_from_this<AsyncTaskOwner>
{
public:
    AsyncTaskOwner();
    virtual ~AsyncTaskOwner();

    void UpdateTask();

    void AddTask(AsyncTask *task);
    void AddSubject(const void *subject);

    bool HasTask();
    bool HasSubject() const;

private:
    MultiBufferQueue<AsyncTask*> tasks_;
    ThreadSafeSet<const void *> subjects_;
};
