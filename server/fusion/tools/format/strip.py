import os

def strip_file(filepath):
    if os.path.splitext(filepath)[1] in ('.h', '.cpp', '.lua', '.py', '.inl'):
        with open(filepath, 'r') as fp:
            lines = fp.readlines()
        with open(filepath, 'wb') as fp:
            for line in lines:
                line = line.rstrip().replace('\t', '\x20' * 4) + '\n'
                fp.write(line.encode('utf-8'))

def strip_directory(dirpath, recursive = True):
    if os.path.exists(dirpath):
        for entry in os.listdir(dirpath):
            entrypath = os.path.join(dirpath, entry)
            if os.path.isfile(entrypath):
                strip_file(entrypath)
            elif os.path.isdir(entrypath):
                if recursive:
                    strip_directory(entrypath)
