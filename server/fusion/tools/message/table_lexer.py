import sys
import ply.lex

class TableLexer:
    reserved_map = {}

    reserved = (
        'ENUM',
    )

    tokens = reserved + (
        'ID', 'NUMBER', 'PREPROCESSOR', 'COMMENT',
        'LBRACE', 'RBRACE', 'EQUALS', 'COMMA', 'SEMI',
        'NSCOLON',
    )

    t_NUMBER = r'(0[xX][a-fA-F\d]+)|(0[0-7]*)|(-?[1-9]\d*)'

    t_LBRACE = r'\{'
    t_RBRACE = r'\}'
    t_EQUALS = r'='
    t_COMMA = r','
    t_SEMI = r';'

    t_NSCOLON = r'::'

    t_ignore = '\x20\t\f\v\r'

    def t_ID(self, t):
        r'[A-Za-z_][\w]*'
        t.type = self.reserved_map.get(t.value, 'ID')
        t.value = self.Token(t.type, t.value, t.lineno)
        return t

    def t_PREPROCESSOR(self, t):
        r'\#.*'
        t.value = self.Token(t.type, t.value, t.lineno)
        return t

    def t_COMMENT(self, t):
        r'//.*'
        t.value = self.Token(t.type, t.value, t.lineno,
            tags = self.get_tags_from_comment(t.value))
        return t

    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    def t_error(self, t):
        print('%s(%d,%d): Illegal char %s.' % (self.file, self.lexer.lineno,
            self.find_column(self.input, t), repr(t.value[0])))
        sys.exit(1)

    def __init__(self, **kwargs):
        self.lexer = ply.lex.lex(module=self, **kwargs)
        if not self.reserved_map:
            self.init()

    def parse(self, file):
        self.file = file
        with open(file, 'r') as fo:
            self.input = fo.read()
        self.lexer.input(self.input)

    @classmethod
    def init(cls):
        for r in cls.reserved:
            cls.reserved_map[r.lower()] = r

    @staticmethod
    def find_column(input, token):
        return token.lexpos - input.rfind('\n', 0, token.lexpos)

    @staticmethod
    def get_tags_from_comment(s):
        start = s.find(r'`')
        if start == -1:
            return None
        end = s.find(r'`', start + 1)
        if end == -1:
            return None
        tags = s[start+1:end].split(',')
        tags = [tag.strip() for tag in tags]
        tags = [tag for tag in tags if tag]
        return tags

    class Token:
        def __init__(self, type, value, lineno, **kwargs):
            self.type, self.value, self.lineno = type, value, lineno
            tags = kwargs and kwargs['tags'] or None
            if tags: self.tags = tags
