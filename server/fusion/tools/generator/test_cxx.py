import sys
from table_cxx_generator import *

if __name__ == '__main__':
    cfg = CxxTableConfig('uint32', '0')
    for i in range(1, len(sys.argv), 2):
        to_cxx_files(sys.argv[i], sys.argv[i+1], cfg)
