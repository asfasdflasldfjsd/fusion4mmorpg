#pragma once

struct GErrorCodeP1 {
	GErrorCodeP1(GErrorCode errCode, uint32 p1 = 0)
		:errCode(errCode), p1(p1) {}
	GErrorCode errCode;
	uint32 p1;
};

struct GErrorCodeP2 {
	GErrorCodeP2(GErrorCode errCode, uint32 p1 = 0, uint32 p2 = 0)
		:errCode(errCode), p1(p1), p2(p2) {}
	GErrorCode errCode;
	uint32 p1, p2;
};

struct GErrorCodeP3 {
	GErrorCodeP3(GErrorCode errCode, uint32 p1 = 0, uint32 p2 = 0, uint32 p3 = 0)
		:errCode(errCode), p1(p1), p2(p2), p3(p3) {}
	GErrorCode errCode;
	uint32 p1, p2, p3;
};

struct GErrorInfo {
	template <typename... Args>
	GErrorInfo(GErrorCode errCode, Args&&... args)
		: errCode(errCode) {
		NetBuffer buffer(std::forward<Args>(args)...);
		errArgs = buffer.CastBufferString();
	}
	GErrorCode errCode;
	std::string errArgs;
};
