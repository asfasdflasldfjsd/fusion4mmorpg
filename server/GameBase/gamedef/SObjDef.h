#pragma once

struct SObjSpawnArgs
{
	bool isRespawn = false;
	uint32 lifeTime = 0;
	float radius = .0f;
};
