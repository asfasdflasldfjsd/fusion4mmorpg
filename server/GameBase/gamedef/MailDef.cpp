#include "preHeader.h"
#include "MailDef.h"

inst_mail NewSystemMailInstance(ObjGUID receiver,
	uint32 flags, const std::string& subject, const std::string& body)
{
	inst_mail mailProp;
	mailProp.mailType = (u32)MAIL_TYPE::SYSTEM;
	mailProp.mailFlags = flags;
	mailProp.mailReceiver = receiver.UID;
	mailProp.mailDeliverTime = GET_UNIX_TIME;
	mailProp.mailExpireTime = GET_UNIX_TIME + MAIL_EXPIRE_TIME;
	mailProp.mailSubject = subject;
	mailProp.mailBody = body;
	return mailProp;
}

void AppendCheque2MailInstance(
	inst_mail& mailProp, uint32 chequeType, uint32 chequeValue)
{
	TextPacker packer;
	packer << chequeType << chequeValue;
	mailProp.mailCheques.push_back(packer.str());
}

void AppendCheque2MailInstance(
	inst_mail& mailProp, const std::string& chequeProp)
{
	mailProp.mailCheques.push_back(chequeProp);
}

void AppendItem2MailInstance(inst_mail& mailProp,
	uint32 itemTypeID, uint32 itemNum, uint32 itemOwner)
{
	inst_item_prop itemProp;
	itemProp.itemTypeID = itemTypeID;
	itemProp.itemCount = itemNum;
	itemProp.itemOwner = itemOwner;
	TextPacker packer;
	itemProp.Save(packer);
	mailProp.mailItems.push_back(packer.str());
}

void AppendItem2MailInstance(
	inst_mail& mailProp, const std::string& itemProp)
{
	mailProp.mailItems.push_back(itemProp);
}
