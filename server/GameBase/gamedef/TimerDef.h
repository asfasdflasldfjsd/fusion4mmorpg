#pragma once

#define MAX_SPELL_EFFECT_NUM (8)

enum TimerType
{
	TimerTypeSpellChantStage,
	TimerTypeSpellChannelStage,
	TimerTypeSpellCleanupStage,
	TimerTypeSpellDelayTrigger,
	TimerTypeSpellDelayTriggerEnd = TimerTypeSpellDelayTrigger + MAX_SPELL_EFFECT_NUM,
	TimerTypeSpellDelayEffective,
	TimerTypeSpellDelayEffectiveEnd = TimerTypeSpellDelayEffective + MAX_SPELL_EFFECT_NUM,
	TimerTypeWatchShutdownInstance,
	TimerTypeTryShutdownInstance,
	TimerTypeShutdownInstance,
	TimerTypeDisappear,
	TimerTypeMax
};
