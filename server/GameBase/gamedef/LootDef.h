#pragma once

struct LootSetGroupItemPrototype
{
	const ItemPrototype* pItemProto;
	const LootSetGroupItem* lsgiInfo;
};

struct LootSetGroupChequePrototype
{
	const LootSetGroupCheque* lsgcInfo;
};

struct LootSetGroupPrototype
{
	const LootSetGroup* lsgInfo;
	std::vector<LootSetGroupItemPrototype> lsgiList;
	std::vector<LootSetGroupChequePrototype> lsgcList;
};

struct LootSetPrototype
{
	const LootSet* lsInfo;
	std::vector<LootSetGroupPrototype> lsgList;
};

struct LootItem
{
	bool isPicked = false;
	const ItemPrototype* pItemProto = NULL;
	uint32 itemCount = 0;
	bool isBindToPicker = false;
};

struct LootCheque
{
	bool isPicked = false;
	uint32 chequeType = 0;
	uint32 chequeValue = 0;
};

struct LootPrizes
{
	std::vector<ObjGUID> lootPickers;
	std::vector<LootItem> lootItems;
	std::vector<LootCheque> lootCheques;
};
