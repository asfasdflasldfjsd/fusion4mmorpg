#pragma once

#define MAIL_EXPIRE_TIME (60*60*24*30)

struct MailCheque {
	uint32 chequeType = 0;
	uint32 chequeValue = 0;
};

inst_mail NewSystemMailInstance(ObjGUID receiver,
	uint32 flags, const std::string& subject, const std::string& body);
void AppendCheque2MailInstance(
	inst_mail& mailProp, uint32 chequeType, uint32 chequeValue);
void AppendCheque2MailInstance(
	inst_mail& mailProp, const std::string& chequeProp);
void AppendItem2MailInstance(inst_mail& mailProp,
	uint32 itemTypeID, uint32 itemNum, uint32 itemOwner);
void AppendItem2MailInstance(
	inst_mail& mailProp, const std::string& itemProp);
