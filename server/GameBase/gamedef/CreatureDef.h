#pragma once

#define CREATURE_PATROL_MOVE_CD (5000)

struct CreatureSpawnArgs
{
	bool isRespawn = false;
	uint32 lifeTime = 0;
	uint32 idleType = 0;
	uint32 level = 0;
};

enum CreatureIdleType
{
	CreatureIdleNone,
	CreatureIdleWalkPatrol,
	CreatureIdleRunPatrol,
};
