#pragma once

#define MAX_CHAT_SPEAK_DIST (20.f)

enum class MessageFlag
{
	Marquee = 1 << 0,
};

enum class ChannelType
{
	tInvalid = -1,
	tWhisper = 0,
	tSpeak,
	tTeam,
	tGuild,
	tWorld,
	tServer,
	ChannelTypeMax
};

enum class ChatThingType
{
	Item,
};
