#pragma once

#include "Singleton.h"

struct GameConfigInstance
{
	void InitData();

	uint32 MaxPlayerLevel;

	uint32 BornMapType, BornMapID;
	vector3f1f BornPos;

	std::unordered_map<uint32, float> MapTypeViewingDistances;
};

class ShellOfGameConfig : public Singleton<ShellOfGameConfig>
{
public:
	ShellOfGameConfig();
	virtual ~ShellOfGameConfig();

	bool LoadNewCfgInst();

	const GameConfigInstance* GetCfgInst() const { return m_cfgInst; }

private:
	void SaveCfgInst(GameConfigInstance* cfgInst);

	GameConfigInstance* m_cfgInst;
	std::vector<GameConfigInstance*> m_obsoleteInsts;
};

#define sShellOfGameConfig (*ShellOfGameConfig::instance())
#define sGameConfig (*sShellOfGameConfig.GetCfgInst())
