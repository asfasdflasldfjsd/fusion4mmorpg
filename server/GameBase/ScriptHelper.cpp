#include "preHeader.h"
#include "ScriptHelper.h"

const std::string& GetScriptFileById(uint32 scriptFileId)
{
	auto pScriptable = GetDBEntry<Scriptable>(scriptFileId);
	return pScriptable != NULL ? pScriptable->scriptFile : emptyString;
}

int DoScriptFile(lua_State* L, const std::string& scriptFile)
{
	lua::push<bool>::invoke(L, sLuaMgr.DoFile(L, scriptFile));
	return 1;
}

int DoScriptFileById(lua_State* L, uint32 scriptFileId)
{
	lua::push<bool>::invoke(L, sLuaMgr.DoFile(L, GetScriptFileById(scriptFileId)));
	return 1;
}
