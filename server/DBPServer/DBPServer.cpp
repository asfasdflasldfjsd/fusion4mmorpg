#include "preHeader.h"
#include "DBPServer.h"

DBPServer::DBPServer()
{
}

DBPServer::~DBPServer()
{
}

void DBPServer::Finish()
{
	sSessionManager.Stop();
	return FrameWorker::Finish();
}

void DBPServer::Update(uint64 diffTime)
{
	AsyncTaskOwner::UpdateTask();
	sSessionManager.Update();
}

void DBPServer::OnTick()
{
	sSessionManager.Tick();
}
