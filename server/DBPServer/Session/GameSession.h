#pragma once

#include "rpc/RPCSession.h"

class GameSessionHandler;

class GameSession : public RPCSession,
	public enable_linked_from_this<GameSession>
{
public:
	GameSession();
	virtual ~GameSession();

	virtual int HandlePacket(INetPacket *pck);
	virtual void OnShutdownSession();

	static void Feedback(const std::weak_ptr<GameSession>& gsPtr,
		const INetStream& data, uint64 sn, int32 err = RPCErrorNone,
		bool eof = true);
	static void FeedbackError(const std::weak_ptr<GameSession>& gsPtr,
		uint64 sn, int32 err);

private:
	friend GameSessionHandler;
	bool m_isReady;
};

#define GSRPCAsyncTaskArgs \
	pckPtr = std::shared_ptr<INetPacket>(&pck), \
	gsPtr = pSession->linked_from_this(), \
	info
#define GSAsyncTaskArgs \
	pckPtr = std::shared_ptr<INetPacket>(&pck)
