#pragma once

class Character;

class SocialData
{
public:
	SocialData(Character* pOwner);
	~SocialData();

	void LoadFriendFromDB(social_friend&& tblVal);
	void LoadIgnoreFromDB(social_ignore&& tblVal);

	GErrorCode AddFriend(Character* pChar);
	GErrorCode RemoveFriend(Character* pChar, int flags = 0);

	GErrorCode AddIgnore(Character* pChar);
	GErrorCode RemoveIgnore(Character* pChar, int flags = 0);

	void OnRemoveCharacter();

	bool IsFriend(ObjGUID characterGuid) const;
	bool IsIgnore(ObjGUID characterGuid) const;

private:
	enum class FollowerType { Friend, Ignore };
	void AddFollower(Character* pChar, FollowerType asType);
	void RemoveFollower(Character* pChar, FollowerType asType);

	struct FollowerInfo {
		Character* pChar;
		FollowerType asType;
	};
	std::unordered_multimap<uint32, FollowerInfo> m_followerList;

	struct FriendInfo {
		Character* pChar;
		social_friend tblVal;
	};
	struct IgnoreInfo {
		Character* pChar;
		social_ignore tblVal;
	};
	std::unordered_map<uint32, FriendInfo> m_friendList;
	std::unordered_map<uint32, IgnoreInfo> m_ignoreList;

	Character* const m_pOwner;
};
