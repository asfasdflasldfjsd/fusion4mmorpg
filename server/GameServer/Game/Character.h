#pragma once

#include "SocialData.h"

class Account;
class Team;

class Character
{
public:
	Character(ObjGUID guid, uint32 acct, uint32 gsId);
	~Character();

	const ObjGUID guid;
	const uint32 acct;
	const uint32 gsId;
	std::string name;
	uint8 career;
	uint8 gender;

	uint32 lastLevel;
	double lastPercHP;
	double lastPercMP;

	ObjGUID lastInstOwner;
	InstGUID lastInstGuid;
	vector3f1f lastPos;

	uint32 guildId;
	int8 guildTitle;
	std::string guildName;

	bool isOnline;
	int64 lastOnlineTime;

	enum SERVER_FLAG {
		eDeleted,
		eInMigration,
		eUninitialized,
	};
	uint32 serverFlags;
	IpcFlags charFlags;

	IpcRankValue rankVals;
	IpcGsReadValue gsReadVals;

	SocialData socialData;
	Team* pTeam;

	void SendError(GErrorCode error) const;
	void SendError(const GErrorCodeP1& error) const;
	void SendError(const GErrorCodeP2& error) const;
	void SendError(const GErrorCodeP3& error) const;
	void SendError(const GErrorInfo& error) const;

	void SendPacket(const INetPacket& pck) const;

	void SetAccount(Account* pAccount) { m_pAccount = pAccount; }
	auto GetAccount() const -> Account* { return m_pAccount; }
	bool IsAccountOnline() const { return m_pAccount != NULL; }

private:
	Account* m_pAccount;
};
