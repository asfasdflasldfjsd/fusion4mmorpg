#include "preHeader.h"
#include "SocialServerSessionHandler.h"
#include "protocol/InternalProtocol.h"

SocialServerSessionHandler::SocialServerSessionHandler()
{
	handlers_[GameServer2SocialServer::SGX_REGISTER_RESP] = &SocialServerSessionHandler::HandleRegisterResp;
	handlers_[GameServer2SocialServer::SGX_TO_ALL_MAP_SERVER_PACKET] = &SocialServerSessionHandler::HandleToAllMapServerPacket;
	handlers_[GameServer2SocialServer::SGX_TO_MAP_SERVER_INSTANCE_PACKET] = &SocialServerSessionHandler::HandleToMapServerInstancePacket;
	handlers_[GameServer2SocialServer::SGX_TO_MAP_SERVER_PLAYER_PACKET] = &SocialServerSessionHandler::HandleToMapServerPlayerPacket;
	handlers_[GameServer2SocialServer::SGX_TO_INSTANCE_PACKET] = &SocialServerSessionHandler::HandleToInstancePacket;
	handlers_[GameServer2SocialServer::SGX_TO_PLAYER_PACKET] = &SocialServerSessionHandler::HandleToPlayerPacket;
	handlers_[GameServer2SocialServer::SGX_KICK] = &SocialServerSessionHandler::HandleKick;
	rpc_handlers_[GameServer2SocialServer::SGX_PULL_CHARACTER_INFOS] = &SocialServerSessionHandler::HandlePullCharacterInfos;
	handlers_[GameServer2SocialServer::SGX_PUSH_CHARACTER_GUILDS] = &SocialServerSessionHandler::HandlePushCharacterGuilds;
	handlers_[GameServer2SocialServer::SGX_CHARACTER_JOIN_GUILD] = &SocialServerSessionHandler::HandleCharacterJoinGuild;
	handlers_[GameServer2SocialServer::SGX_CHARACTER_QUIT_GUILD] = &SocialServerSessionHandler::HandleCharacterQuitGuild;
};

SocialServerSessionHandler::~SocialServerSessionHandler()
{
}
