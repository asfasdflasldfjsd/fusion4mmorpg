#pragma once

#include "Singleton.h"
#include "network/Listener.h"

class MapServerListener : public Listener, public Singleton<MapServerListener>
{
public:
	THREAD_RUNTIME(MapServerListener)

	MapServerListener();
	virtual ~MapServerListener();

private:
	virtual std::string GetBindAddress();
	virtual std::string GetBindPort();

	virtual Session *NewSessionObject();

	uint32 m_sn;
};

#define sMapServerListener (*MapServerListener::instance())
