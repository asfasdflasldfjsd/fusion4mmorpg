#include "preHeader.h"
#include "Session/ClientPacketHandler.h"
#include "Game/Account.h"
#include "Game/Character.h"
#include "Manager/SpecialShopMgr.h"

int ClientPacketHandler::HandleGetShopItemList(Account *pAccount, INetPacket &pck)
{
	size_t anchor = pck.GetReadPos();
	uint32 shopType;
	pck >> shopType;
	if (!pck.IsReadableEmpty()) {
		return SessionHandleKill;
	}

	pck.AdjustReadPos(anchor - pck.GetReadPos());
	sSpecialShopMgr.PackShopItemList(pck, shopType);

	return SessionHandleForward;
}

int ClientPacketHandler::HandleBuySpecialShopItem(Account *pAccount, INetPacket &pck)
{
	uint64 itemUniqueKey;
	uint32 num;
	pck >> itemUniqueKey >> num;
	Coroutine::Spawn([=, pChar = pAccount->GetCharacter()](Coroutine::YieldContext& ctx) {
		auto errCode = sSpecialShopMgr.HandleBuySpecialShopItem(ctx, pChar, itemUniqueKey, num);
		if (errCode != CommonSuccess) {
			pChar->SendError(errCode);
		}
	});
	return SessionHandleSuccess;
}
