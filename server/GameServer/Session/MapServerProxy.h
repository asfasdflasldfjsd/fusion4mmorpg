#pragma once

class Character;
class MapServerSession;

class MapServerProxy
{
public:
	MapServerProxy(MapServerSession* pSession);
	~MapServerProxy();

	void AddCharacter(Character* pChar);
	void RemoveCharacter(Character* pChar);
	Character* GetCharacter(ObjGUID guid) const;

	void SendPacket(const INetPacket& pck) const;
	void SendPacket(const INetPacket& pck, const INetPacket& data) const;

	void TransInstancePacket(InstGUID guid, const INetPacket& pck) const;
	void TransPlayerPacket(ObjGUID guid, const INetPacket& pck) const;
	void TransRPCInvoke2InstancePacket(InstGUID guid, const INetPacket& pck,
		const std::function<void(INetStream&, int32, bool)>& cb,
		AsyncTaskOwner* owner, time_t timeout) const;
	void TransRPCInvoke2PlayerPacket(ObjGUID guid, const INetPacket& pck,
		const std::function<void(INetStream&, int32, bool)>& cb,
		AsyncTaskOwner* owner, time_t timeout) const;
	void TransRPCReply2InstancePacket(InstGUID guid, const INetPacket& pck,
		uint64 sn, int32 err, bool eof) const;
	void TransRPCReply2PlayerPacket(ObjGUID guid, const INetPacket& pck,
		uint64 sn, int32 err, bool eof) const;

	MapServerSession* GetSession() const { return m_pSession; }

private:
	void PushPlayer2GateServer(Character* pChar, bool isEnter) const;

	MapServerSession* const m_pSession;

	std::unordered_map<ObjGUID, Character*> m_players;
};
