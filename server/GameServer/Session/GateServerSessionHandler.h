#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class GateServerSession;

class GateServerSessionHandler :
	public MessageHandler<GateServerSessionHandler, GateServerSession, GateServer2GameServer::GATE2GAME_MESSAGE_COUNT>,
	public Singleton<GateServerSessionHandler>
{
public:
	GateServerSessionHandler();
	virtual ~GateServerSessionHandler();
private:
	int HandleRegister(GateServerSession *pSession, INetPacket &pck);
	int HandleTransPlayerPacket(GateServerSession *pSession, INetPacket &pck);
	int HandleAccountLoginSucc(GateServerSession *pSession, INetPacket &pck);
	int HandleAccountLogout(GateServerSession *pSession, INetPacket &pck);
};

#define sGateServerSessionHandler (*GateServerSessionHandler::instance())
