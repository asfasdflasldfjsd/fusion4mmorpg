#pragma once

#include "rpc/RPCSession.h"

class MapServerProxy;
class MapServerSessionHandler;

class MapServerSession : public RPCSession
{
public:
	MapServerSession(uint32 sn);
	virtual ~MapServerSession();

	virtual int HandlePacket(INetPacket *pck);
	virtual void OnShutdownSession();

	void PackMapServerInfo(INetPacket& pck) const;

	uint32 sn() const { return m_sn; }
	uint32 service() const { return m_service; }

	bool IsReady() const { return m_serviceId != 0; }

	void SetServiceId(uint32 serviceId) { m_serviceId = serviceId; }
	uint32 GetServiceId() const { return m_serviceId; }

	void SetProxy(MapServerProxy* pProxy) { m_pProxy = pProxy; }
	MapServerProxy* GetProxy() const { return m_pProxy; }

private:
	friend MapServerSessionHandler;
	const uint32 m_sn;

	uint32 m_service;
	uint32 m_gsIdx;
	std::string m_host;
	std::string m_port;

	uint32 m_serviceId;
	MapServerProxy* m_pProxy;
};
