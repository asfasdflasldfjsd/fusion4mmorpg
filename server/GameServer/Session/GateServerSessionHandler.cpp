#include "preHeader.h"
#include "GateServerSessionHandler.h"
#include "protocol/InternalProtocol.h"

GateServerSessionHandler::GateServerSessionHandler()
{
	handlers_[GateServer2GameServer::CGG_REGISTER] = &GateServerSessionHandler::HandleRegister;
	handlers_[GateServer2GameServer::CGG_TRANS_PLAYER_PACKET] = &GateServerSessionHandler::HandleTransPlayerPacket;
	handlers_[GateServer2GameServer::CGG_ACCOUNT_LOGIN_SUCC] = &GateServerSessionHandler::HandleAccountLoginSucc;
	handlers_[GateServer2GameServer::CGG_ACCOUNT_LOGOUT] = &GateServerSessionHandler::HandleAccountLogout;
};

GateServerSessionHandler::~GateServerSessionHandler()
{
}
