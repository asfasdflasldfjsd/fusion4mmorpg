#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/InternalProtocol.h"

class SocialServerSession;

class SocialServerSessionHandler :
	public MessageHandler<SocialServerSessionHandler, SocialServerSession, GameServer2SocialServer::GAME2SOCIAL_MESSAGE_COUNT>,
	public Singleton<SocialServerSessionHandler>
{
public:
	SocialServerSessionHandler();
	virtual ~SocialServerSessionHandler();
private:
	int HandleRegisterResp(SocialServerSession *pSession, INetPacket &pck);
	int HandleToAllMapServerPacket(SocialServerSession *pSession, INetPacket &pck);
	int HandleToMapServerInstancePacket(SocialServerSession *pSession, INetPacket &pck);
	int HandleToMapServerPlayerPacket(SocialServerSession *pSession, INetPacket &pck);
	int HandleToInstancePacket(SocialServerSession *pSession, INetPacket &pck);
	int HandleToPlayerPacket(SocialServerSession *pSession, INetPacket &pck);
	int HandleKick(SocialServerSession *pSession, INetPacket &pck);
	int HandlePullCharacterInfos(SocialServerSession *pSession, INetPacket &pck, const RPCActor::RequestMetaInfo &info);
	int HandlePushCharacterGuilds(SocialServerSession *pSession, INetPacket &pck);
	int HandleCharacterJoinGuild(SocialServerSession *pSession, INetPacket &pck);
	int HandleCharacterQuitGuild(SocialServerSession *pSession, INetPacket &pck);
};

#define sSocialServerSessionHandler (*SocialServerSessionHandler::instance())
