#pragma once

#include "Singleton.h"

class MailMgr : public WheelTimerOwner, public Singleton<MailMgr>
{
public:
	MailMgr();
	virtual ~MailMgr();

	void Init();

	GErrorCode HandleMsMailRequest(INetPacket& pck);

private:
	virtual WheelTimerMgr *GetWheelTimerMgr();

	void AcquireMailExpireTime();
	void DeleteMailExpireTime();

	GErrorCode HandleSendSingleMail(INetPacket& pck);
	GErrorCode HandleSendMultiSameMail(INetPacket& pck);
	GErrorCode HandleSendMultiDifferentMail(INetPacket& pck);
};

#define sMailMgr (*MailMgr::instance())
