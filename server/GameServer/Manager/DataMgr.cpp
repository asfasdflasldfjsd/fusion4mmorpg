#include "preHeader.h"
#include "DataMgr.h"
#include "InstCfgMgr.h"

DataMgr::DataMgr()
: m_startServerTime(-1)
{
}

DataMgr::~DataMgr()
{
}

void DataMgr::LoadData()
{
	InitStartServerTime();
}

void DataMgr::InitStartServerTime()
{
	auto& cfgVal = sInstCfgMgr.GetInstCfgVal(InstCfg_ServerStartTime);
	if (!cfgVal.empty()) {
		m_startServerTime = std::stoll(cfgVal);
	} else {
		m_startServerTime = GET_UNIX_TIME;
		sInstCfgMgr.SaveInstCfgVal(
			InstCfg_ServerStartTime, std::to_string(m_startServerTime));
	}
}
