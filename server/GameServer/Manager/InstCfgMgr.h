#pragma once

#include "Singleton.h"

enum InstCfgID {
	InstCfg_None,
	InstCfg_ServerStartTime,
	InstCfg_SpecialShopStatus,
};

#define BEGIN_DELAY_SAVE_INST_CFG_VAL \
	static bool IsDelaySaving = false; \
	if (!IsDelaySaving) { \
		IsDelaySaving = true; \
		sGameServer.CreateTimerX([=]() {
#define END_DELAY_SAVE_INST_CFG_VAL \
			IsDelaySaving = false; \
		}, SAVE_DELAY_MAX, 1); \
	}

class InstCfgMgr : public Singleton<InstCfgMgr>
{
public:
	InstCfgMgr();
	virtual ~InstCfgMgr();

	bool LoadDataFromDB();

	const std::string& GetInstCfgVal(uint32 cfgId) const;
	void SaveInstCfgVal(uint32 cfgId, const std::string& cfgVal);

private:
	std::unordered_map<uint32, inst_configure> m_instCfgs;
};

#define sInstCfgMgr (*InstCfgMgr::instance())
