#pragma once

#include "Singleton.h"

class DataMgr : public Singleton<DataMgr>
{
public:
	DataMgr();
	virtual ~DataMgr();

	void LoadData();

public:
	time_t getStartServerTime() const { return m_startServerTime; }
private:
	void InitStartServerTime();
	time_t m_startServerTime;
};

#define sDataMgr (*DataMgr::instance())
