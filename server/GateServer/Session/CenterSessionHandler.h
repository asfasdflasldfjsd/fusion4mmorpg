#pragma once

#include "Singleton.h"
#include "MessageHandler.h"
#include "protocol/CenterProtocol.h"

class CenterSession;

class CenterSessionHandler :
	public MessageHandler<CenterSessionHandler, CenterSession, CenterProtocol::CENTER_PROTOCOL_COUNT>,
	public Singleton<CenterSessionHandler>
{
public:
	CenterSessionHandler();
	virtual ~CenterSessionHandler();
private:
	int HandleRegisterResp(CenterSession *pSession, INetPacket &pck);
};

#define sCenterSessionHandler (*CenterSessionHandler::instance())
