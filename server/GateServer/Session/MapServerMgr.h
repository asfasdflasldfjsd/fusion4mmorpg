#pragma once

#include "Singleton.h"
#include "MapServerSession.h"

class MapServerMgr : public Singleton<MapServerMgr>
{
public:
	MapServerMgr();
	virtual ~MapServerMgr();

	void CheckConnections();

	void AddMapServer(MapServerSession* pSession);
	void RemoveMapServer(MapServerSession* pSession);
	MapServerSession* GetMapServer(uint32 msSN) const;

	void ClearAllMapServers();

private:
	std::unordered_map<uint32, MapServerSession*> m_MapServerMap;
};

#define sMapServerMgr (*MapServerMgr::instance())
