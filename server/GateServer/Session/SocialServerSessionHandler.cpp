#include "preHeader.h"
#include "SocialServerSessionHandler.h"
#include "protocol/InternalProtocol.h"

SocialServerSessionHandler::SocialServerSessionHandler()
{
	handlers_[GateServer2SocialServer::SGT_REGISTER_RESP] = &SocialServerSessionHandler::HandleRegisterResp;
	handlers_[GateServer2SocialServer::SGT_PUSH_PACKET_TO_ALL_CLIENT] = &SocialServerSessionHandler::HandlePushPacketToAllClient;
};

SocialServerSessionHandler::~SocialServerSessionHandler()
{
}
