#pragma once

#include "network/Session.h"

class ClientSessionHandler;

class ClientSession : public Session
{
public:
	ClientSession(uint32 sn);
	virtual ~ClientSession();

	virtual int HandlePacket(INetPacket *pck);
	virtual void OnManaged();
	virtual void OnShutdownSession();

	void SetAccountOK(uint32 acct);
	void SetCharacterOnline(uint32 charId);
	void SetCharacterOffline();
	void SetCharacterEnterMapServer(uint32 msSN);
	void SetCharacterLeaveMapServer();

	int32 lang() const { return m_lang; }
	const std::string& channel() const { return m_channel; }
	const std::string& platform() const { return m_platform; }
	uint32 logicGsID() const { return m_logicGsID; }

	uint32 sn() const { return m_sn; }
	uint32 getAcctId() const { return m_acctId; }
	uint32 getCharId() const { return m_charId; }
	ObjGUID getPlayerGuid() const { return m_playerGuid; }
	uint32 getMSSN() const { return m_msSN; }
	bool IsAccountOK() const { return m_isAccountOK; }
	bool IsCharacterOK() const { return m_isCharacterOK; }
	bool IsCharacterInMapServer() const { return m_isCharacterInMapServer; }

private:
	virtual void OnRecvPacket(INetPacket *pck);

	friend ClientSessionHandler;
	const uint32 m_sn;

	int32 m_lang;
	std::string m_channel;
	std::string m_platform;
	uint32 m_logicGsID;

	bool m_isAccountOK;
	uint32 m_acctId;
	bool m_isCharacterOK;
	uint32 m_charId;
	ObjGUID m_playerGuid;
	bool m_isCharacterInMapServer;
	uint32 m_msSN;
};
