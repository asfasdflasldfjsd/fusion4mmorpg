#pragma once

#include "Singleton.h"
#include "ClientSession.h"

class ClientMgr : public Singleton<ClientMgr>
{
public:
	ClientMgr();
	virtual ~ClientMgr();

	void AddClient(ClientSession* pSession);
	void RemoveClient(ClientSession* pSession);
	ClientSession* GetClient(uint32 sn) const;

	void KickClient(ClientSession* pSession, GErrorCode error);
	void KillClient(ClientSession* pSession);

	void KickAllClient(GErrorCode error);

	void SendLogoutAccount2GameServer(ClientSession* pSession) const;

	void BroadcastPacket2AllClient(const INetPacket& pck) const;

private:
	std::unordered_map<uint32, ClientSession*> m_ClientInfoMap;
};

#define sClientMgr (*ClientMgr::instance())
