#include "preHeader.h"
#include "WorldHook.h"
#include "MapInstance.h"

WorldHook::WorldHook(MapInstance* pMapInstance)
: IMapHook(pMapInstance)
{
}

WorldHook::~WorldHook()
{
}

void WorldHook::InitLuaEnv()
{
	IMapHook::InitLuaEnv();

	auto L = m_pMapInstance->L;
	lua::class_add<WorldHook>(L, "WorldHook");
	lua::class_inh<WorldHook, IMapHook>(L);

	static const std::string scriptFile = "scripts/Hook/world.lua";
	RunScriptFile(m_pMapInstance->L, scriptFile, m_pMapInstance, this);
}
