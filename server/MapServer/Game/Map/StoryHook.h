#pragma once

#include "IMapHook.h"

class StoryHook : public IMapHook
{
public:
	StoryHook(MapInstance* pMapInstance);
	virtual ~StoryHook();

	virtual void InitLuaEnv();

	virtual void InitPlayArgs(const std::string_view& args);

	void Play();

private:
	enum HookTimerType {
		PlayTimeout = TimerTypeMax,
		DelayStage,
	};

	void FinishPlayStory(bool isSucc,
		uint32 questTypeID, uint32 backMapType, uint32 delayLeaveMS);
};
