#pragma once

#include "tile/TileDefine.h"
#include "Scene.h"

class GameMap
{
public:
	GameMap(const MapInfo* pMapInfo);
	~GameMap();

	void LoadDBRes();

	uint32 GetMapId() const { return m_pMapInfo->Id; }
	const MapInfo* GetMapInfo() const { return m_pMapInfo; }
	const TileDefine& GetTileDefine() const { return m_tileDefine; }
	Scene* GetScene() const { return m_pScene; }

	const std::vector<const MapZone*>& GetMapZones(uint32 type) const;
	const std::vector<const MapGraveyard*>& GetMapGraveyards(uint32 type) const;
	const std::vector<const TeleportPoint*>& GetTeleportPoints(uint32 type) const;

	const std::vector<const CreatureSpawn*>& GetCreatureSpawns(uint32 type) const;
	const std::vector<const StaticObjectSpawn*>& GetStaticObjectSpawns(uint32 type) const;

private:
	void LoadMapZones();
	void LoadMapGraveyards();
	void LoadTeleportPoints();

	void LoadCreatureSpawns();
	void LoadStaticObjectSpawns();

	void LoadSceneRes();

	const MapInfo* const m_pMapInfo;
	const TileDefine m_tileDefine;
	Scene* m_pScene;

	std::vector<const MapZone*> m_mapZones[(int)MapInfo::Type::Count];
	std::vector<const MapGraveyard*> m_mapGraveyards[(int)MapInfo::Type::Count];
	std::vector<const TeleportPoint*> m_teleportPoints[(int)MapInfo::Type::Count];

	std::vector<const CreatureSpawn*> m_creatureSpawns[(int)MapInfo::Type::Count];
	std::vector<const StaticObjectSpawn*> m_staticObjectSpawns[(int)MapInfo::Type::Count];
};

inline const std::vector<const MapZone*>& GameMap::GetMapZones(uint32 type) const {
	return m_mapZones[type];
}
inline const std::vector<const MapGraveyard*>& GameMap::GetMapGraveyards(uint32 type) const {
	return m_mapGraveyards[type];
}
inline const std::vector<const TeleportPoint*>& GameMap::GetTeleportPoints(uint32 type) const {
	return m_teleportPoints[type];
}
inline const std::vector<const CreatureSpawn*>& GameMap::GetCreatureSpawns(uint32 type) const {
	return m_creatureSpawns[type];
}
inline const std::vector<const StaticObjectSpawn*>& GameMap::GetStaticObjectSpawns(uint32 type) const {
	return m_staticObjectSpawns[type];
}
