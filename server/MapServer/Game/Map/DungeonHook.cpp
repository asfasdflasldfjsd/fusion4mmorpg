#include "preHeader.h"
#include "DungeonHook.h"
#include "MapInstance.h"

DungeonHook::DungeonHook(MapInstance* pMapInstance)
: IMapHook(pMapInstance)
{
}

DungeonHook::~DungeonHook()
{
}

void DungeonHook::InitLuaEnv()
{
	IMapHook::InitLuaEnv();

	auto L = m_pMapInstance->L;
	lua::class_add<DungeonHook>(L, "DungeonHook");
	lua::class_inh<DungeonHook, IMapHook>(L);

	static const std::string scriptFile = "scripts/Hook/dungeon.lua";
	RunScriptFile(m_pMapInstance->L, scriptFile, m_pMapInstance, this);
}
