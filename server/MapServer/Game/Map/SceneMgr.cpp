#include "preHeader.h"
#include "SceneMgr.h"

SceneMgr::SceneMgr()
{
}

SceneMgr::~SceneMgr()
{
	for (auto& pair : m_scenes) {
		delete pair.second;
	}
}

Scene* SceneMgr::CreateAndGetScene(const std::string& sceneDir)
{
	auto itr = m_scenes.find(sceneDir);
	if (itr != m_scenes.end()) {
		return itr->second;
	}

	Scene* pScene = new Scene;
	if (!pScene->Init(sceneDir)) {
		ELOG("Init scene `%s` failed.", sceneDir.c_str());
	}

	m_scenes.emplace(sceneDir, pScene);
	return pScene;
}
