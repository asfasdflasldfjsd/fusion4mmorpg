#pragma once

#include "Singleton.h"
#include "Scene.h"

class SceneMgr : public Singleton<SceneMgr>
{
public:
	SceneMgr();
	virtual ~SceneMgr();

	Scene* CreateAndGetScene(const std::string& sceneDir);

private:
	std::unordered_map<std::string, Scene*> m_scenes;
};

#define sSceneMgr (*SceneMgr::instance())
