#pragma once

#include "IMapHook.h"

class DungeonHook : public IMapHook
{
public:
	DungeonHook(MapInstance* pMapInstance);
	virtual ~DungeonHook();

	virtual void InitLuaEnv();
};
