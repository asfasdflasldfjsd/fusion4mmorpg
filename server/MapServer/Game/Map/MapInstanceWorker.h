#pragma once

#include "FrameWorker.h"

class MapInstance;

class MapInstanceWorker : public FrameWorker
{
public:
	THREAD_RUNTIME(MapInstanceWorker)

	MapInstanceWorker();
	virtual ~MapInstanceWorker();

	void AddMapInstance(MapInstance* pMapInstance);

	uint32 GetLoadValue () const { return m_loadValue.load(); }

private:
	virtual bool Initialize();

	virtual void Update(uint64 diffTime);
	virtual void OnTick();

	void DynamicPushMapInstance();
	void CleanMapInstance();

	static uint32 EvalMapInstanceLoadValue(MapInstance* pMapInstance);

	MultiBufferQueue<MapInstance*> m_MapInsertPool;
	std::vector<MapInstance*> m_MapInstances;
	std::atomic<uint32> m_loadValue;
};
