#pragma once

#include "IMapHook.h"

class WorldHook : public IMapHook
{
public:
	WorldHook(MapInstance* pMapInstance);
	virtual ~WorldHook();

	virtual void InitLuaEnv();
};
