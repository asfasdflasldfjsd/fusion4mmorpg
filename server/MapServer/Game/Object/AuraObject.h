#pragma once

#include "LocatableObject.h"

class Unit;
class Spell;

class AuraObject : public LocatableObject
{
public:
	AuraObject();
	virtual ~AuraObject();

	bool InitAuraObject(const AuraPrototype* pProto,
		Spell* pSpell, uint32 effectIndex, Unit* pOwner);

	virtual void Update(uint64 diffTime);

	virtual void OnPushToWorld();
	virtual void OnDelete();

	virtual void BuildCreatePacketForPlayer(INetPacket& pck, Player* pPlayer);

	static AuraPrototype ParseAuraPrototype(const char* args);

	const AuraPrototype* getProto() const { return m_pProto; }
	const SpellPrototype* getSpellProto() const { return m_pSpellProto; }
	Unit* getCaster() const { return m_pCaster; }
	Unit* getOwner() const { return m_pOwner; }

protected:
	void SubInitLuaEnv();

private:
	virtual bool TestInteresting(AoiActor *actor) const;
	virtual void OnAddMarker(AoiActor *actor);
	virtual void OnRemoveMarker(AoiActor *actor);

	bool CanApplyAuraEffect(Unit* pTarget) const;
	void EnterAuraEffect(Unit* pTarget);
	void LeaveAuraEffect(Unit* pTarget);

	const AuraPrototype* m_pProto;
	const SpellPrototype* m_pSpellProto;
	uint32 m_spellLevel;
	uint32 m_effectIndex;
	uint64 m_spellInstGuid;
	Unit* m_pCaster;
	Unit* m_pOwner;

	const uint64 m_effSpellInstGuid;
};
