#pragma once

#include "LocatableObject.h"

class Unit;
class Spell;

class StaticObject : public LocatableObject
{
public:
	StaticObject();
	virtual ~StaticObject();

	bool InitStaticObject(const StaticObjectSpawn* pSpawn);

	virtual void Update(uint64 diffTime);

	virtual void OnPushToWorld();
	virtual void OnDelete();

	virtual const std::string& GetName() const;

	virtual void BuildCreatePacketForPlayer(INetPacket& pck, Player* pPlayer);

	void SetSpawnPrivate(const std::shared_ptr<StaticObjectSpawn>& pSpawn);
	const std::shared_ptr<StaticObjectSpawn>& GetSpawnPrivate() const;

	uint32 GetSpawnId() const { return m_pSpawn->spawnId; }
	const StaticObjectSpawn* GetSpawn() const { return m_pSpawn; }

	uint32 GetEntry() const { return m_pProto->sobjTypeId; }
	const SObjPrototype* GetProto() const { return m_pProto; }

protected:
	const SObjPrototype* m_pProto;
	const StaticObjectSpawn* m_pSpawn;
	std::shared_ptr<StaticObjectSpawn> m_pSpawnPrivate;

protected:
	virtual void SubInitLuaEnv();

public:
	virtual bool TestInteresting(AoiActor *actor) const;
	virtual void OnAddMarker(AoiActor *actor);
	virtual void OnRemoveMarker(AoiActor *actor);

public:
	GErrorCode CanMine(Player* pPlayer);
	void StartMine(Player* pPlayer, Spell* pSpell, size_t effectIndex);
	void StopMine(Player* pPlayer, Spell* pSpell, size_t effectIndex);
	void FinishMine(Player* pPlayer, Spell* pSpell, size_t effectIndex);
private:
	bool IsValidMineObj(ObjGUID playerGuid,
		uint64 spellInstGuid, Player** pPlayerPtr = NULL) const;
	std::unordered_map<ObjGUID, uint64> m_mineInfos;

public:
	bool IsVisibleByPlayer(const Player* pPlayer);
	GErrorCode CanInteractive(Player* pPlayer);
	GErrorCode TryInteractive(Player* pPlayer);

private:
	void ObjectHookEvent_OnSObjUnitEnter(Unit* pUnit);
	void ObjectHookEvent_OnSObjUnitLeave(Unit* pUnit);
};
