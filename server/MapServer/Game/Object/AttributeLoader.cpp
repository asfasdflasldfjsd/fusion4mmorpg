#include "preHeader.h"
#include "Creature.h"
#include "Player.h"

void Player::LoadBase(double attrs[])
{
	auto pPrototype = GetDBEntry<PlayerAttribute>(((u32)GetCareer() << 24) + GetLevel());
	if (pPrototype != NULL) {
		std::copy_n(pPrototype->attrs.begin(), (int)ATTRTYPE::COUNT, attrs);
		return;
	}
}

void Player::LoadBody(AttrPartProxy& proxy)
{
}

void Player::LoadEquip(AttrPartProxy& proxy)
{
	m_pItemStorage->ForeachItem(ItemSlotEquipType, [&proxy](uint32, Item* pItem) {
		pItem->ApplyAttributes(proxy);
	});
}

void Player::LoadExtraAttrs()
{
	auto pPrototype = GetDBEntry<PlayerBase>(GetLevel());
	if (pPrototype != NULL) {
		m_attribute.SetAttrEx(ATTREXTYPE::DAMAGE_FACTOR, pPrototype->damageFactor);
		m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_HP_RATE, pPrototype->recoveryHPRate);
		m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_HP_VALUE, pPrototype->recoveryHPValue);
		m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_MP_RATE, pPrototype->recoveryMPRate);
		m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_MP_VALUE, pPrototype->recoveryMPValue);
	}
}

void Creature::LoadBase(double attrs[])
{
	auto pCustomPrototype = GetDBEntry<CreatureCustomAttribute>(GetEntry());
	if (pCustomPrototype != NULL) {
		std::copy_n(pCustomPrototype->attrs.begin(), (int)ATTRTYPE::COUNT, attrs);
		return;
	}
	auto pPrototype = GetDBEntry<CreatureAttribute>(((u32)GetElite() << 24) + GetLevel());
	if (pPrototype != NULL) {
		std::copy_n(pPrototype->attrs.begin(), (int)ATTRTYPE::COUNT, attrs);
		return;
	}
}

void Creature::LoadBody(AttrPartProxy& proxy)
{
}

void Creature::LoadEquip(AttrPartProxy& proxy)
{
}

void Creature::LoadExtraAttrs()
{
	auto pCustomPrototype = GetDBEntry<CreatureCustomAttribute>(GetEntry());
	if (pCustomPrototype != NULL) {
		m_attribute.SetAttrEx(ATTREXTYPE::DAMAGE_FACTOR, pCustomPrototype->damageFactor);
	} else {
		auto pPrototype = GetDBEntry<CreatureAttribute>(((u32)GetElite() << 24) + GetLevel());
		if (pPrototype != NULL) {
			m_attribute.SetAttrEx(ATTREXTYPE::DAMAGE_FACTOR, pCustomPrototype->damageFactor);
		}
	}

	m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_HP_RATE, m_pProto->recoveryHPRate);
	m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_HP_VALUE, m_pProto->recoveryHPValue);
	m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_MP_RATE, m_pProto->recoveryMPRate);
	m_attribute.SetAttrEx(ATTREXTYPE::RECOVERY_MP_VALUE, m_pProto->recoveryMPValue);
}
