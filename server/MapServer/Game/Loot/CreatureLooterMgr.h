#pragma once

class Creature;
class Player;

class CreatureLooterMgr
{
public:
	CreatureLooterMgr(Creature* pCreature);
	~CreatureLooterMgr();

	void AddPlayer(Player* pPlayer, uint64 damage);

	Player* CalcFinalLooter() const;

private:
	struct Looter {
		uint64 totalDamage = 0;
	};
	Creature* const m_pCreature;
	std::unordered_map<ObjGUID, Looter> m_allLooters;
};
