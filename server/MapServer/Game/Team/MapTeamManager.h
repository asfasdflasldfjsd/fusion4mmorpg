#pragma once

#include "MapTeam.h"

class MapTeamManager
{
public:
	MapTeamManager(MapInstance* pMapInstance);
	~MapTeamManager();

	void LoadTeamFromGSPacket(INetPacket& pack, uint16 gsId);
	void DeleteTeam(uint16 gsId, uint32 teamId);

	void SetTeamOrPullTeam(Player* pPlayer, uint32 teamId);

	MapTeam* GetTeam(uint16 gsId, uint32 teamId) const;

private:
	MapTeam* CreateAndGetTeam(uint16 gsId, uint32 teamId);

	MapInstance* const m_pMapInstance;
	std::unordered_map<uint64, MapTeam*> m_allTeams;
};
