#pragma once

#include "Singleton.h"
#include "FrameWorker.h"

class MapServer : public FrameWorker, public AsyncTaskOwner,
	public WheelTimerOwner, public WheelTriggerOwner,
	public Singleton<MapServer>
{
public:
	THREAD_RUNTIME(MapServer)

	MapServer();
	virtual ~MapServer();

	void SendSingleMail(uint16 gsId, const inst_mail& mailProp);
	void SendMultiMail(const std::vector<ObjGUID>& receiverGuids,
		const inst_mail& mailProp);
	void SendMultiMail(const std::vector<uint16>& receiverGsIds,
		const std::vector<inst_mail>& mailProps);

	void SendPacketToAllPlayer(const INetPacket& pck);
	void SendSysMsgToAll(ChannelType channelType, uint32 msgFlags,
		uint32 msgId, const std::string_view& msgArgs = emptyStringView);

	void OfflinePlayer(size_t gsIdx = -1);
	void OfflineAllPlayer();

	WheelTimerMgr sWheelTimerMgr;

private:
	virtual WheelTimerMgr *GetWheelTimerMgr();

	virtual bool Initialize();
	virtual void Finish();

	virtual void Update(uint64 diffTime);
	virtual void OnTick();

	void OnNewDaily();
	void OnNewWeekly();
	void OnNewMonthly();
};

#define sMapServer (*MapServer::instance())
