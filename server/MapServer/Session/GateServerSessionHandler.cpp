#include "preHeader.h"
#include "GateServerSessionHandler.h"
#include "protocol/InternalProtocol.h"

GateServerSessionHandler::GateServerSessionHandler()
{
	handlers_[GateServer2MapServer::CGM_REGISTER] = &GateServerSessionHandler::HandleRegister;
};

GateServerSessionHandler::~GateServerSessionHandler()
{
}
