#include "preHeader.h"
#include "DBPServerMgr.h"

DBPServerMgr::DBPServerMgr()
{
	m_DBPServerList.reserve(CROSS_SERVER_MAX);
}

DBPServerMgr::~DBPServerMgr()
{
	for (auto pSession : m_DBPServerList) {
		delete pSession;
	}
}

void DBPServerMgr::CheckConnections()
{
	for (auto pSession : m_DBPServerList) {
		pSession->CheckConnection();
	}
}

void DBPServerMgr::AddDBPServer(const std::string& host, const std::string& port)
{
	m_DBPServerList.push_back(DBPSession::New(host, port));
}

DBPSession* DBPServerMgr::GetDBPSession(size_t index) const
{
	return index < m_DBPServerList.size() ? m_DBPServerList[index] : NULL;
}
