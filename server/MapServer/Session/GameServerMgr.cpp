#include "preHeader.h"
#include "GameServerMgr.h"

GameServerMgr::GameServerMgr()
{
	m_GameServerList.reserve(CROSS_SERVER_MAX);
}

GameServerMgr::~GameServerMgr()
{
	for (auto pSession : m_GameServerList) {
		delete pSession;
	}
}

void GameServerMgr::CheckConnections()
{
	for (auto pSession : m_GameServerList) {
		pSession->CheckConnection();
	}
}

void GameServerMgr::AddGameServer(
	const std::string& host, const std::string& port, uint32 service)
{
	size_t index = m_GameServerList.size();
	m_GameServerList.push_back(
		new GameServerSession(host, port, service, index));
}

GameServerSession* GameServerMgr::GetGameServerSession(size_t index) const
{
	return index < m_GameServerList.size() ? m_GameServerList[index] : NULL;
}

void GameServerMgr::BroadcastPacket2AllGameServer(const INetPacket& pck) const
{
	for (auto pSession : m_GameServerList) {
		pSession->PushSendPacket(pck);
	}
}

void GameServerMgr::BroadcastPacket2AllGameServer(
	const INetPacket& pck, const INetPacket& data) const
{
	for (auto pSession : m_GameServerList) {
		pSession->PushSendPacket(pck, data);
	}
}
