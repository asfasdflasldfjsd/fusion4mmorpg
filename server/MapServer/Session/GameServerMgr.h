#pragma once

#include "Singleton.h"
#include "GameServerSession.h"

class GameServerMgr : public Singleton<GameServerMgr>
{
public:
	GameServerMgr();
	virtual ~GameServerMgr();

	void CheckConnections();

	void AddGameServer(
		const std::string& host, const std::string& port, uint32 service);

	void BroadcastPacket2AllGameServer(const INetPacket& pck) const;
	void BroadcastPacket2AllGameServer(
		const INetPacket& pck, const INetPacket& data) const;

	GameServerSession* GetGameServerSession(size_t index) const;

private:
	std::vector<GameServerSession*> m_GameServerList;
};

#define sGameServerMgr (*GameServerMgr::instance())
#define GSSession(i) (*sGameServerMgr.GetGameServerSession(i))
