#pragma once

#include "Singleton.h"
#include "GateServerSession.h"

class GateServerMgr : public Singleton<GateServerMgr>
{
public:
	GateServerMgr();
	virtual ~GateServerMgr();

	void RegisterGateServer(GateServerSession* pGateSession);
	void RemoveGateServer(GateServerSession* pGateSession);
	GateServerSession* GetGateServer(const GateServerGUID& guid) const;
	GateServerSession* GetGateServer(uint32 serverId, uint32 gateSN, uint32 msSN) const;

	void BroadcastPacket2AllClient(const INetPacket& data) const;

	void BroadcastPacket2AllGateServer(const INetPacket& pck) const;
	void BroadcastPacket2AllGateServer(
		const INetPacket& pck, const INetPacket& data) const;

private:
	std::unordered_map<GateServerGUID, GateServerSession*> m_GateServerMap;
};

#define sGateServerMgr (*GateServerMgr::instance())
