#include "preHeader.h"
#include "GateServerMgr.h"
#include "Map/InstanceMgr.h"

GateServerMgr::GateServerMgr()
{
}

GateServerMgr::~GateServerMgr()
{
}

void GateServerMgr::RegisterGateServer(GateServerSession* pGateSession)
{
	m_GateServerMap.emplace(pGateSession->guid(), pGateSession);
}

void GateServerMgr::RemoveGateServer(GateServerSession* pGateSession)
{
	m_GateServerMap.erase(pGateSession->guid());
	sInstanceMgr.ForeachAllInstance(
		[=, connPtr = pGateSession->GetConnection()](MapInstance* pMapInstance) {
		pMapInstance->EventOfflineGatePlayer(pGateSession);
	});
}

GateServerSession* GateServerMgr::GetGateServer(const GateServerGUID& guid) const
{
	auto itr = m_GateServerMap.find(guid);
	return itr != m_GateServerMap.end() ? itr->second : NULL;
}

GateServerSession* GateServerMgr::GetGateServer(uint32 serverId, uint32 gateSN, uint32 msSN) const
{
	GateServerGUID guid;
	guid.serverId = serverId, guid.gateSN = gateSN, guid.msSN = msSN;
	return GetGateServer(guid);
}

void GateServerMgr::BroadcastPacket2AllClient(const INetPacket& data) const
{
	NetPacket pck(SGM_PUSH_PACKET_TO_ALL_CLIENT);
	BroadcastPacket2AllGateServer(pck, data);
}

void GateServerMgr::BroadcastPacket2AllGateServer(const INetPacket& pck) const
{
	for (auto& pair : m_GateServerMap) {
		pair.second->PushSendPacket(pck);
	}
}

void GateServerMgr::BroadcastPacket2AllGateServer(
	const INetPacket& pck, const INetPacket& data) const
{
	for (auto& pair : m_GateServerMap) {
		pair.second->PushSendPacket(pck, data);
	}
}
