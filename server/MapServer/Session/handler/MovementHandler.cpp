#include "preHeader.h"
#include "Session/PlayerPacketHandler.h"
#include "Map/MapInstance.h"

GErrorCode Player::HandleMovement(INetPacket& pck)
{
	if (!IsInWorld() || IsTeleporting()) {
		return CommonSuccess;
	}
	if (IsDead() || IsOutOfMove()) {
		return CommonSuccess;
	}

	vector3f pos, dir;
	pck >> pos >> dir;
	if (GetDistance2DSq(pos) > 100.f) {
		return InvalidRequest;
	}

	SetPosition(pos);
	SetDirection(dir);

	NetPacket pack(pck.GetOpcode());
	pack.Append(pck.GetBuffer(), pck.GetTotalSize());
	SendMessageToSet(pack, false);

	return CommonSuccess;
}

int PlayerPacketHandler::HandleMoveStart(Player *pPlayer, INetPacket &pck)
{
	auto errCode = pPlayer->HandleMovement(pck);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleMoveStop(Player *pPlayer, INetPacket &pck)
{
	auto errCode = pPlayer->HandleMovement(pck);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleMoveSync(Player *pPlayer, INetPacket &pck)
{
	auto errCode = pPlayer->HandleMovement(pck);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}

int PlayerPacketHandler::HandleMoveTurnDir(Player *pPlayer, INetPacket &pck)
{
	auto errCode = pPlayer->HandleMovement(pck);
	if (errCode != CommonSuccess) {
		pPlayer->SendError(errCode);
	}
	return SessionHandleSuccess;
}
