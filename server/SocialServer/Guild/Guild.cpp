#include "preHeader.h"
#include "Guild.h"
#include "GuildMgr.h"

Guild::Guild()
: m_guildMaster(0)
{
}

Guild::~Guild()
{
}

bool Guild::OnLoadDataFromDBFinished()
{
	for (auto&[playerId, guildMember] : m_guildMembers) {
		if (guildMember.guildTitle == (s8)GUILD_TITLE::MASTER) {
			m_guildMaster = guildMember.playerId;
			break;
		}
	}
	return true;
}

void Guild::SetGuildInfo(GuildInformation&& guildInfo)
{
	m_guildInfo = std::move(guildInfo);
}

void Guild::AddGuildMember(GuildMember&& guildMember)
{
	m_guildMembers.emplace(guildMember.playerId, std::move(guildMember));
}

void Guild::AddGuildApply(GuildApply&& guildApply)
{
	m_guildApplys.emplace(guildApply.playerId, std::move(guildApply));
}

void Guild::AddGuildInvite(GuildInvite&& guildInvite)
{
	m_guildInvites.emplace(guildInvite.targetId, std::move(guildInvite));
}

void Guild::RemoveGuildMember(uint32 playerId)
{
	m_guildMembers.erase(playerId);
}

void Guild::RemoveGuildApply(uint32 playerId)
{
	m_guildApplys.erase(playerId);
}

void Guild::RemoveGuildInvite(uint32 playerId, bool isExpire)
{
	auto itr = m_guildInvites.find(playerId);
	if (itr != m_guildInvites.end()) {
		if (!isExpire) {
			sGuildMgr.RemoveTimers(itr->second.timer);
		}
		m_guildInvites.erase(itr);
	}
}

GuildMember* Guild::GetGuildMember(uint32 playerId)
{
	auto itr = m_guildMembers.find(playerId);
	return itr != m_guildMembers.end() ? &itr->second : NULL;
}

const GuildApply* Guild::GetGuildApply(uint32 playerId) const
{
	auto itr = m_guildApplys.find(playerId);
	return itr != m_guildApplys.end() ? &itr->second : NULL;
}

const GuildInvite* Guild::GetGuildInvite(uint32 playerId) const
{
	auto itr = m_guildInvites.find(playerId);
	return itr != m_guildInvites.end() ? &itr->second : NULL;
}

bool Guild::IsGuildMemberFull() const
{
	return m_guildMembers.size() >= 30;
}
