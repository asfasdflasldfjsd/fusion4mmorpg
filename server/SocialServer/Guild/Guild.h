#pragma once

class Guild
{
public:
	Guild();
	~Guild();

	bool OnLoadDataFromDBFinished();
	void SetGuildInfo(GuildInformation&& guildInfo);
	void AddGuildMember(GuildMember&& guildMember);
	void AddGuildApply(GuildApply&& guildApply);
	void AddGuildInvite(GuildInvite&& guildInvite);

	void RemoveGuildMember(uint32 playerId);
	void RemoveGuildApply(uint32 playerId);
	void RemoveGuildInvite(uint32 playerId, bool isExpire = false);

	GuildMember* GetGuildMember(uint32 playerId);
	const GuildApply* GetGuildApply(uint32 playerId) const;
	const GuildInvite* GetGuildInvite(uint32 playerId) const;

	bool IsGuildMemberFull() const;

	uint32 GetGuildMaster() const { return m_guildMaster; }
	uint32 GetGuildId() const { return m_guildInfo.Id; }
	const std::string& GetGuildName() const { return m_guildInfo.name; }
	const GuildInformation& GetGuildInformation() const { return m_guildInfo; }
	const std::unordered_map<uint32, GuildMember>& GetGuildMembers() const { return m_guildMembers; }
	const std::unordered_map<uint32, GuildApply>& GetGuildApplys() const { return m_guildApplys; }
	const std::unordered_map<uint32, GuildInvite>& GetGuildInvites() const { return m_guildInvites; }

private:
	uint32 m_guildMaster;
	GuildInformation m_guildInfo;
	std::unordered_map<uint32, GuildMember> m_guildMembers;
	std::unordered_map<uint32, GuildApply> m_guildApplys;
	std::unordered_map<uint32, GuildInvite> m_guildInvites;
};
