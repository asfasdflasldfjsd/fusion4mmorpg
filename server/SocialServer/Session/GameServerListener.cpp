#include "preHeader.h"
#include "GameServerListener.h"
#include "GameServerSession.h"
#include "ServerMaster.h"

GameServerListener::GameServerListener()
{
}

GameServerListener::~GameServerListener()
{
}

std::string GameServerListener::GetBindAddress()
{
	return IServerMaster::GetInstance().GetConfig().
		GetString("GAME_SERVER", "HOST", "0.0.0.0");
}

std::string GameServerListener::GetBindPort()
{
	return IServerMaster::GetInstance().GetConfig().
		GetString("GAME_SERVER", "PORT", "9996");
}

Session *GameServerListener::NewSessionObject()
{
	return new GameServerSession();
}
