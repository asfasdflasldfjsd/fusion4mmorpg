#include "preHeader.h"
#include "GateServerMgr.h"

GateServerMgr::GateServerMgr()
{
}

GateServerMgr::~GateServerMgr()
{
}

void GateServerMgr::AddGateServer(GateServerSession* pSession)
{
	m_GateServerMap.emplace(pSession->sn(), pSession);
}

void GateServerMgr::RemoveGateServer(GateServerSession* pSession)
{
	m_GateServerMap.erase(pSession->sn());
}

void GateServerMgr::BroadcastPacket2AllClient(const INetPacket& data) const
{
	NetPacket pck(SGT_PUSH_PACKET_TO_ALL_CLIENT);
	BroadcastPacket2AllGateServer(pck, data);
}

void GateServerMgr::BroadcastPacket2AllGateServer(const INetPacket& pck) const
{
	for (const auto& pair : m_GateServerMap) {
		pair.second->PushSendPacket(pck);
	}
}

void GateServerMgr::BroadcastPacket2AllGateServer(const INetPacket& pck, const INetPacket& data) const
{
	for (const auto& pair : m_GateServerMap) {
		pair.second->PushSendPacket(pck, data);
	}
}
