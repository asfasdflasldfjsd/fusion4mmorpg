#include "preHeader.h"
#include "GateServerSession.h"
#include "GateServerSessionHandler.h"
#include "GateServerMgr.h"
#include "SocialServer.h"
#include "C2GuildPacketHandler.h"
#include "Guild/GuildMgr.h"
#include "C2RankPacketHandler.h"
#include "Rank/RankMgr.h"
#include "Game/TransMgr.h"

GateServerSession::GateServerSession(uint32 sn)
: m_sn(sn)
, m_gsGateSN(0)
{
}

GateServerSession::~GateServerSession()
{
}

int GateServerSession::HandlePacket(INetPacket *pck)
{
	if (IsReady() || pck->GetOpcode() < FLAG_GATE2SOCIAL_MSG_NEED_REGISTER_BEGIN) {
		return sGateServerSessionHandler.HandlePacket(this, *pck);
	} else {
		return SessionHandleUnhandle;
	}
}

void GateServerSession::OnShutdownSession()
{
	sTransMgr.RemoveGateServer(this);
	sGateServerMgr.RemoveGateServer(this);
	WLOG("Close GateServerSession [sn:%u gsGateSN:%u].", m_sn, m_gsGateSN);
	Session::OnShutdownSession();
}

void GateServerSession::OnRecvPacket(INetPacket *pck)
{
	switch (pck->GetOpcode()) {
	case CGT_TRANS_PLAYER_PACKET:
		TransPlayerPacket(pck);
		break;
	default:
		Session::OnRecvPacket(pck);
		break;
	}
}

void GateServerSession::TransPlayerPacket(INetPacket *pck) const
{
	uint32 uid;
	*pck >> uid;
	pck->UnpackPacket();
	auto opcode = pck->GetOpcode();
	if (sC2GuildPacketHandler.CanHandle(opcode)) {
		sGuildMgr.PushRecvPacket(GuildMgr::NewClientPacket(uid, pck));
		return;
	}
	if (sC2RankPacketHandler.CanHandle(opcode)) {
		sRankMgr.PushRecvPacket(RankMgr::NewClientPacket(uid, pck));
		return;
	}
	WLOG("Trans player packet[%u] failed, can't find recipient.", opcode);
	delete pck;
}

int GateServerSessionHandler::HandleRegister(GateServerSession *pSession, INetPacket &pck)
{
	sGateServerMgr.AddGateServer(pSession);

	pck >> pSession->m_gsGateSN;
	sTransMgr.AddGateServer(pSession);

	bool flagHandlers[GAME_OPCODE::CSMSG_COUNT];
	sC2GuildPacketHandler.InitHandlerFlags(flagHandlers, ARRAY_SIZE(flagHandlers));

	NetPacket resp(SGT_REGISTER_RESP);
	resp.Append(flagHandlers, sizeof(flagHandlers));
	pSession->PushSendPacket(resp);

	NLOG("GateServer Register Success As [sn:%u gsGateSN:%u].",
		pSession->m_sn, pSession->m_gsGateSN);

	return SessionHandleSuccess;
}

int GateServerSessionHandler::HandleUpdateGsGateSn(GateServerSession *pSession, INetPacket &pck)
{
	auto oldGsGateSN = pSession->m_gsGateSN;
	pck >> pSession->m_gsGateSN;
	sTransMgr.UpdateGateServerGSSN(oldGsGateSN, pSession);
	return SessionHandleSuccess;
}
