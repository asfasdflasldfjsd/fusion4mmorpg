#pragma once

#include "Singleton.h"
#include "network/Listener.h"

class GameServerListener : public Listener, public Singleton<GameServerListener>
{
public:
	THREAD_RUNTIME(GameServerListener)

	GameServerListener();
	virtual ~GameServerListener();

private:
	virtual std::string GetBindAddress();
	virtual std::string GetBindPort();

	virtual Session *NewSessionObject();
};

#define sGameServerListener (*GameServerListener::instance())
