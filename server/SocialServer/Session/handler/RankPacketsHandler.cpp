#include "preHeader.h"
#include "Rank/RankMgr.h"
#include "Session/C2RankPacketHandler.h"
#include "Session/S2RankPacketHandler.h"
#include "Session/GameServerMgr.h"
#include "Game/TransMgr.h"
#include "Game/PacketDispatcher.h"

int C2RankPacketHandler::HandleRankGetPlayerList(RankMgr *mgr, INetPacket &pck, uint32 uid)
{
	uint8 subType;
	uint32 startIndex, getCountMax;
	pck >> subType >> startIndex >> getCountMax;
	Coroutine::Spawn([=](Coroutine::YieldContext& ctx) {
		auto errCode = mgr->HandleGetPlayerRankList(ctx, uid, subType, startIndex, getCountMax);
		if (errCode != CommonSuccess) {
			sTransMgr.SendError2ClientSafe(uid, errCode);
		}
	});
	return SessionHandleSuccess;
}

int C2RankPacketHandler::HandleRankGetGuildList(RankMgr *mgr, INetPacket &pck, uint32 uid)
{
	uint8 subType;
	uint32 guildId, startIndex, getCountMax;
	pck >> guildId >> subType >> startIndex >> getCountMax;
	Coroutine::Spawn([=](Coroutine::YieldContext& ctx) {
		auto errCode = mgr->HandleGetGuildRankList(ctx, uid, guildId, subType, startIndex, getCountMax);
		if (errCode != CommonSuccess) {
			sTransMgr.SendError2ClientSafe(uid, errCode);
		}
	});
	return SessionHandleSuccess;
}

int S2RankPacketHandler::HandleRankNewPlayer(RankMgr *mgr, INetPacket &pck)
{
	while (!pck.IsReadableEmpty()) {
		mgr->HandleNewPlayer(pck);
	}
	return SessionHandleSuccess;
}

int S2RankPacketHandler::HandleRankUpdatePlayer(RankMgr *mgr, INetPacket &pck)
{
	while (!pck.IsReadableEmpty()) {
		mgr->HandleUpdatePlayer(pck);
	}
	return SessionHandleSuccess;
}

int S2RankPacketHandler::HandleRankRemovePlayer(RankMgr *mgr, INetPacket &pck)
{
	while (!pck.IsReadableEmpty()) {
		mgr->HandleRemovePlayer(pck);
	}
	return SessionHandleSuccess;
}

int S2RankPacketHandler::HandleRankNewGuild(RankMgr *mgr, INetPacket &pck)
{
	while (!pck.IsReadableEmpty()) {
		mgr->HandleNewGuild(pck);
	}
	return SessionHandleSuccess;
}

int S2RankPacketHandler::HandleRankUpdateGuild(RankMgr *mgr, INetPacket &pck)
{
	while (!pck.IsReadableEmpty()) {
		mgr->HandleUpdateGuild(pck);
	}
	return SessionHandleSuccess;
}

int S2RankPacketHandler::HandleRankRemoveGuild(RankMgr *mgr, INetPacket &pck)
{
	while (!pck.IsReadableEmpty()) {
		mgr->HandleRemoveGuild(pck);
	}
	return SessionHandleSuccess;
}

int S2RankPacketHandler::HandleRankGetGuildIdList(RankMgr *mgr, INetPacket &pck, const RPCActor::RequestMetaInfo &info)
{
	uint8 replyType, subType;
	uint32 startIndex, getCountMax;
	pck >> replyType >> subType >> startIndex >> getCountMax;
	NetPacket rpcRespPck(SGX_RPC_INVOKE_RESP);
	auto errCode = mgr->HandlePackGuildRankIdList(rpcRespPck, subType, startIndex, getCountMax);
	sPacketDispatcher.RunS2SRPCReply((S2SRPCReply)replyType,
		rpcRespPck, info.sn, errCode == CommonSuccess ? RPCErrorNone : RPCErrorFailed);
	return SessionHandleSuccess;
}
