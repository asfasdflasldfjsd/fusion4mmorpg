#pragma once

#include "Singleton.h"
#include "ServerMaster.h"

class SocialServerMaster : public IServerMaster, public Singleton<SocialServerMaster>
{
public:
	SocialServerMaster();
	virtual ~SocialServerMaster();

	virtual bool InitSingleton();
	virtual void FinishSingleton();

private:
	virtual bool InitDBPool();
	virtual bool LoadDBData();

	virtual bool StartServices();
	virtual void StopServices();

	virtual void Tick();

	virtual std::string GetConfigFile();
	virtual size_t GetDeferAsyncServiceCount();
	virtual size_t GetAsyncServiceCount();
	virtual size_t GetIOServiceCount();
};

#define sSocialServerMaster (*SocialServerMaster::instance())
